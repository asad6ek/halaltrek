<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 18:13
 */

namespace App\DDD\Packages\Action;


use App\Models\City;
use App\DDD\Packages\Models\Package;
use App\DDD\Packages\VO\PackageVO;
use App\Models\PackageAirline;
use App\Models\PackageHotel;
use App\Models\PackageOption;
use App\Models\PackagePrice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PackageAction
{
    /**
     * @var array
     */
    private $packageVO;

    /**
     * @var  int
     */
    private $agent_id;
    /**
     * @var null
     */
    private $package_id;

    protected $package;

    public function __construct($packageVO, $user_id, $package_id = null)
    {
        $this->packageVO = $packageVO;
        $this->agent_id = $user_id;
        $this->package_id = $package_id;
    }


    public function execute()
    {
        \DB::beginTransaction();
        $package = $this->savePakcage();

        $package->payments()->delete();
        $package->hotels()->delete();
        $package->option()->delete();
        $package->flights()->delete();

        $this->savePrices($package->id);
        $this->saveHotels($package->id);
        $this->saveFlights($package->id);

        $this->saveOptions($package->id);

        $this->saveCities($package);

        \DB::commit();
        return $package;
    }

    /**
     * @return Package
     */
    protected function savePakcage()
    {
        $package = $this->getPackage();
        $package->fill($this->packageVO);
        $package->agent_id = $this->agent_id;
        $package->date_from = $this->dateConverter($this->packageVO['date_from']);
        $package->date_to = $this->dateConverter($this->packageVO['date_to']);
        $package->package_class = $this->packageVO["package_class"];
        $package->country_id = $this->packageVO["country_id"] ?? null;
        $package->description = $this->packageVO["description"] ?? null;
        $package->save();

        return $package;
    }

    /**
     * @return Package;
     * @throws ModelNotFoundException
     */
    protected function getPackage()
    {
        if (!$this->package) {
            if ($this->package_id) {
                $this->package = Package::query()
                    ->where(['id' => $this->package_id,])
                    ->where(['agent_id' => $this->agent_id])
                    ->firstOrFail();
                return $this->package;
            }

            $this->package = new Package();
        }

        return $this->package;
    }

    protected function savePrices($package_id)
    {
        $payments = data_get($this->packageVO, 'payment', []);

        foreach ($payments as $payment) {
            $model = new PackagePrice();
            $model->package_id = $package_id;
            $price = data_get($payment, 'price', 0);
            if (!$price) {
                continue;
            }

            $type = data_get($payment, 'type', 1);
            $model->name = 'Price for ' . $type;
            $model->price = $price;
            $model->currency = data_get($payment, 'currency', 'usd');
            $model->type = $type;
            $model->save();
        }
    }


    protected function saveHotels($package_id)
    {
        $hotels = data_get($this->packageVO, 'hotels', []);

        foreach ($hotels as $type => $hotel) {
            $model = new PackageHotel();
            $model->package_id = $package_id;
            $model->hotel_id = $hotel;
            $model->type = PackageHotel::convertTypeText($type);
            $model->save();
        }
    }

    protected function saveFlights($package_id)
    {
        $flights = data_get($this->packageVO, 'flight', []);

        foreach ($flights as $type => $flight) {
            $model = new PackageAirline();
            $model->package_id = $package_id;
            $model->airline_id = data_get($flight, 'airline');
            $model->from = data_get($flight, 'from');
            $model->to = data_get($flight, 'to');
            $model->from_date = $this->dateConverter(data_get($flight, 'from_date'));
            $model->return_date = $this->dateConverter(data_get($flight, 'return_date'));
            $model->description = data_get($flight, 'description');
            $model->type = data_get($flight, 'type');
//            $model->to_date = data_get($flight, 'to_date');
            $model->save();
        }
    }

    protected function saveOptions($package_id)
    {
        $model = new PackageOption();
        $model->package_id = $package_id;
        $model->seminar = data_get($this->packageVO, 'options.seminar', 0);
        $model->goody_bag = data_get($this->packageVO, 'options.goody_bag', 0);
        $model->breakfast = data_get($this->packageVO, 'options.breakfast', 0);
        $model->ground_transport = data_get($this->packageVO, 'options.ground_transport', 0);
        $model->hadiy = data_get($this->packageVO, 'options.hadiy', 0);
        $model->visit = data_get($this->packageVO, 'options.visit', 0);
        $model->save();
    }

    public function dateConverter($date)
    {
        return Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
    }

    public function saveCities(Package $package)
    {
        $city_ids = (array)$this->packageVO['city_id'] ?? [];
        $package->cities()->delete();
        $cities = City::query()->whereIn('id', (array)$city_ids)->get();

        $package->cities()->saveMany($cities);
    }


}
