<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 18:24
 */

namespace App\DDD\Packages\Exceptions;


use Illuminate\Database\Eloquent\ModelNotFoundException;

class PackageNotFound extends ModelNotFoundException
{

}
