<?php

namespace App\DDD\Packages\Models;

use App\Models\Agent;
use App\Models\City;
use App\Models\Hotel;
use App\Models\PackageAirline;
use App\Models\PackageHotel;
use App\Models\PackageOption;
use App\Models\PackagePrice;
use App\QueryFilters\PackageQueryFilter;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DDD\Packages\Models\Package
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package query()
 * @method static \Illuminate\Database\Eloquent\Builder filter($request)
 * @mixin \Eloquent
 * @property int                                                                        $id
 * @property string                                                                     $ziyarat_type
 * @property string                                                                     $name
 * @property string                                                                     $date_from
 * @property string                                                                     $date_to
 * @property int                                                                        $num_days
 * @property string                                                                     $package_class
 * @property int                                                                        $city_id
 * @property int                                                                        $country_id
 * @property int                                                                        $agent_id
 * @property string                                                                     $hotel_basis_makkah
 * @property string                                                                     $description
 * @property string                                                                     $hotel_basis_madinah
 * @property string                                                                     $hotel_basis
 * @property string                                                                     $first_visit
 * @property int                                                                        $ziyorah_included
 * @property int                                                                        $transport_support
 * @property int                                                                        $is_shifting
 * @property int                                                                        $num_days_aziziya
 * @property \Illuminate\Support\Carbon|null                                            $created_at
 * @property \Illuminate\Support\Carbon|null                                            $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereFirstVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereHotelBasis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereIsShifting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereNumDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereNumDaysAziziya($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereTransportSupport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereZiyaratType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereZiyorahIncluded($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackagePrice[]   $payments
 * @property-read int|null                                                              $payments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackageHotel[]   $hotels
 * @property-read int|null                                                              $hotels_count
 * @property-read \App\Models\PackageOption                                             $option
 * @property int                                                                        $count_review
 * @property int                                                                        $average_review
 * @property int                                                                        $status
 * @property City|null                                                                  $city
 * @property-read \App\Models\Agent                                                     $agent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PackageAirline[] $flights
 * @property-read int|null                                                              $flights_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereAverageReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereCountReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereStatus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\City[] $cities
 * @property-read int|null $cities_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereHotelBasisMadinah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package whereHotelBasisMakkah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DDD\Packages\Models\Package wherePackageClass($value)
 */
class Package extends Model
{
    const TYPE_HAJJ = 'hajj';
    const TYPE_UMRAH = 'umrah';
    const TYPE_TOURS = 'tours';

    const MAKKAH = 'Makkah';
    const MADINAH = 'Madinah';
    const TYPE_INDIRECT = 'Indirect';

    protected $fillable = [
        'ziyarat_type',
        'name',
        'date_from',
        'date_to',
        'num_days',
//        'city_id',
        'country_id',
        'hotel_basis_makkah',
        'hotel_basis_madinah',
        'first_visit',
        'ziyorah_included',
        'transport_support',
        'is_shifting',
        'num_days_aziziya',
        'description',
    ];

    protected $guarded = [
        'id', 'agent_id',
    ];

    protected $casts = [
        'date_from' => 'datetime:d-m-Y',
        'date_to'   => 'datetime:d-m-Y',
    ];


    public static function packageTypes()
    {
        return [
            self::TYPE_HAJJ,
            self::TYPE_UMRAH,
            self::TYPE_TOURS,
        ];
    }

    public function scopeFilter($query, $request)
    {
        return (new PackageQueryFilter($query))->filter($request);
    }

    public function isForHajj()
    {
        return $this->ziyarat_type === self::TYPE_HAJJ;
    }

    public function isForUmrah()
    {
        return $this->ziyarat_type === self::TYPE_UMRAH;
    }

    public function isForTours()
    {
        return $this->ziyarat_type === self::TYPE_TOURS;
    }

    public static function hotelBasesList()
    {
        return [
            'Room', 'B&B', 'FullBoard', 'Half board', 'To be confirmed',
        ];
    }

    public static function firstVisitDesire()
    {
        return [
            'Makkah',
            'Madinah',
        ];
    }

    public function payments()
    {
        return $this->hasMany(PackagePrice::class);
    }

    public function getPriceFor($numberOfPerson)
    {
        if (!$this->payments) {
            return '';
        }

        if (!isset($this->payments[$numberOfPerson - 1])) {
            return '';
        }

        return $this->payments[$numberOfPerson - 1]['price'];
    }

    public function hotels()
    {
        return $this->hasMany(PackageHotel::class);
    }

    public function getHotelsFor($type)
    {
        $hotels = $this->hotels->keyBy('type');

        return data_get($hotels, $type);
    }

    public function option()
    {
        return $this->hasOne(PackageOption::class);
    }

    public function flights()
    {
        return $this->hasMany(PackageAirline::class);
    }

    public function getReviewCount()
    {
        return optional($this->agent)->count_review;
    }

    public function getReviewAvarage()
    {
        return optional($this->agent)->average_review;
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function priceShow()
    {
        return '£' . number_format($this->getMinPrice());
    }

    public function getType()
    {
        return strtoupper($this->ziyarat_type);
    }

    public function getHotelForMakka()
    {
        return $this->getHotelsFor(PackageHotel::TYPE_MAKKA);
    }

    public function getHotelForMadina()
    {
        return $this->getHotelsFor(PackageHotel::TYPE_MADINA);
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'country_id');
    }

    public function getMinPrice()
    {
        return $this->payments->filter(function ($model) {
            return $model->price > 0;
        })->min('price');
    }

    public static function packageClasses()
    {
        return ['Premium' => 'Premium', 'Economy' => 'Economy', 'Budget' => 'Budget'];
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    public function getCitiesIds()
    {
        return $this->cities->pluck('id');
    }
}
