<?php

namespace App\DDD\Packages\Repos;

use App\DDD\Packages\Interfaces\IPackageRepo;
use App\DDD\Packages\Models\Package;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
class BookingRepo implements IPackageRepo
{

    /**
     * @param $agent_id
     * @return LengthAwarePaginator
     */
    public function getUserPackages($agent_id)
    {
        return Package::query()
            ->with(['payments', 'hotels.hotel.images', 'flights.airline', 'agent'])
            ->where('agent_id', $agent_id)
            ->orderByDesc('id')
            ->paginate();
    }
}
