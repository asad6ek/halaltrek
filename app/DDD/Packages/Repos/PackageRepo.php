<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 21:16
 */

namespace App\DDD\Packages\Repos;


use App\DDD\Packages\Interfaces\IPackageRepo;
use App\DDD\Packages\Models\Package;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PackageRepo implements IPackageRepo
{
    /**
     * @param $id
     * @param $agent_id
     * @return Package
     * @throws ModelNotFoundException
     */
    public function getByIDAndAgent($id, $agent_id)
    {
        return Package::query()
            ->where('agent_id', $agent_id)
            ->where('id', $id)
            ->firstOrFail();
    }

    /**
     * @param $agent_id
     * @return Package[]
     */
    public function getUserPackages($agent_id)
    {
        return Package::query()
            ->with(['payments', 'hotels.hotel.images', 'flights.airline', 'agent'])
            ->where('agent_id', $agent_id)
            ->orderByDesc('id')
            ->paginate();
    }

    /**
     * @param $type
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getGetMainPackage($type)
    {
        return Package::query()
            ->with(['payments', 'flights.airline', 'hotels.hotel.images', 'agent'])
            ->where('ziyarat_type', $type)
            ->orderByDesc('id')
            ->paginate();
    }

    public function getPackages()
    {
        return Package::query()
            ->with(['payments', 'flights.airline', 'hotels.hotel.images', 'agent'])
            ->whereHas('agent')
            ->orderByDesc('id')
            ->paginate();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Package[]
     */
    public function filter($request)
    {
        return Package::filter($request)
            ->selectRaw('packages.*, min(package_prices.price) as min_price')
            ->join('package_hotels', 'package_hotels.package_id', '=', 'packages.id')
            ->join('hotels', 'hotels.id', '=', 'package_hotels.hotel_id')
            ->join('package_prices','package_prices.package_id','=','packages.id')
            ->groupBy('packages.id')
            ->paginate();
//        ->toSql();
    }
}
