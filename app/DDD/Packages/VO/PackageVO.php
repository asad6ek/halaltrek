<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 18:15
 */

namespace App\DDD\Packages\VO;


use App\Helpers\ArrayableTrait;
use App\Helpers\Loadable;

class PackageVO
{
    use ArrayableTrait, Loadable;

    public $public;
    public $otherPublic;
    protected $protected;
    protected $otherProtected;
    private $private;
    private $otherPrivate;
}
