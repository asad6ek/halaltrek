<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 18:31
 */

namespace App\Helpers;


trait ArrayableTrait
{
    public function toArray()
    {
        $ref = new \ReflectionClass($this);

        $properties = $ref->getProperties();

        $returnArray = [];
        foreach ($properties as $property) {
            if (!$property->isPublic()) {
                continue;
            }

            $returnArray[$property->getName()] = $property->getValue($this);
        }

        return $returnArray;
    }
}
