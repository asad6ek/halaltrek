<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 11:19
 */

namespace App\Helpers;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\AbstractPaginator;

class CollectionToSelect2Api
{
    /**
     * @var Collection
     */
    private $collection;
    private $fields;
    /**
     * @var string
     */
    private $primaryKey;

    public function __construct(AbstractPaginator $collection, $fields, $primaryKey = 'id')
    {
        $this->collection = $collection;
        if (is_string($fields)) {
            $arr['text'] = $fields;
            $fields = $arr;
        }
        $this->fields = $fields;
        $this->primaryKey = $primaryKey;
    }

    public function send()
    {
        if ($this->collection->count() ===0) {
            return [];
        }


        return [
            'results' => $this->getData(),
            'pagination' => $this->getPagination(),
        ];
    }

    protected function getData()
    {
        $result = [];
        foreach ($this->collection as $item) {
            $data = [
                'id' => $item->{$this->primaryKey},
            ];

            foreach ($this->fields as $field => $attribute) {
                $data[$field] = $item->{$attribute};
            }

            $result[] = $data;
        }

        return $result;
    }

    protected function getPagination()
    {
        return [
            'more' => $this->collection->hasMorePages(),
        ];
    }
}
