<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 22:10
 */

namespace App\Helpers;


class DetailedViewFilter
{
    private $value;
    private $filters;

    public function __construct($value, $filters)
    {

        $this->value = $value;
        $this->filters = $filters;
    }

    public function execute()
    {
        if (!$this->filters) {
            return $this->value;
        }

        foreach ($this->filters as $filter) {
            $this->value = $this->executeFilter($this->value, $filter);
        }
        return $this->value;
    }

    protected function executeFilter($value, $filter)
    {
        if (method_exists($this, $filter)) {
            return $this->$filter($value);
        }

        throw new \Exception('Filter not found');
        return $value;
    }

    public function bool($value)
    {
        return $value ? 'Yes' : 'no';
    }
}
