<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 18:39
 */

namespace App\Helpers;


trait Loadable
{
    protected $protectedItems;
    private $ref;
    private $isChanged = false;

    public function load($items, $key = null)
    {
        $data = $this->getFormData($items, $key);

        foreach ($data as $property => $value) {
            if ($this->isProtectedProperty($property)) {
                continue;
            }
            $this->{$property} = $value;
            $this->isChanged = true;
        }

        return $this->isChanged;

    }

    public function getFormData($items, $key = null)
    {
        if (!$key) {
            return $items;
        }

        return data_get($items, $key);
    }

    protected function isProtectedProperty($property)
    {
        if (in_array($property, $this->protectedItems)) {
            return true;
        }

        $property = $this->getRef()->getProperty($property);

        if (!$property) {
            return false;
        }

        return !$property->isPublic();
    }

    /**
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    private function getRef()
    {
        if (!$this->ref) {
            $this->ref = new \ReflectionClass($this);
        }

        return $this->ref;
    }
}
