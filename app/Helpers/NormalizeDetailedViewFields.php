<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 21:27
 */

namespace App\Helpers;


class NormalizeDetailedViewFields
{
    private $fields;
    private $model;

    public function __construct($fields, $model)
    {
        $this->fields = $fields;
        $this->model = $model;
    }


    public function execute()
    {
        $newFields = [];
        foreach ($this->fields as $key => $field) {

            if (is_callable($field)) {
                $newFields[] = [
                    'label' => $this->makeTitle($key),
                    'value' => $field(),
                    'filters'=>'',
                ];
                continue;
            }

            $data = explode('|', $field);

            $field = array_shift($data);

          if (is_string($field) && is_numeric($key)) {
                $newFields[] = [
                    'label' => $this->makeTitle($field),
                    'value' => data_get($this->model, $field),
                    'filters' => $data,

                ];
            } else {
                $newFields[] = [
                    'label' => ucfirst($key),
                    'value' => data_get($this->model, $field),
                    'filters' => $data,
                ];
            }
        }

        return $newFields;
    }

    public function makeTitle($title)
    {
        return implode(' ', explode('_', $title));
    }

}
