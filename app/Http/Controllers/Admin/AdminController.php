<?php

namespace App\Http\Controllers\Admin;

use App\actions\AdminAdminModifyAction;
use App\Http\Requests\AdminRequest;
use App\Models\Admin;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Admin::orderByDesc('id')->paginate();

        return view('admin.admin.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new Admin();

        return view('admin.admin.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        $adminSaver = new AdminAdminModifyAction($request->all());
        $adminSaver->execute();
        alertMessage('info', 'Admin Created');
        return redirect()->route('admin.admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = Admin::getAgentById($id);

        return view('admin.admin.view', compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::findById($id);

        return view('admin.admin.update', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRequest $request, $id)
    {
        $adminSaver = new AdminAdminModifyAction($request->all(), $id);
        $adminSaver->execute();
        alertMessage('info', 'Admin Updated');
        return redirect()->route('admin.admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {

        $user_id = Auth::id();
        if ((int)$user_id === (int)$admin->id) {
            alertMessage('warning', 'You cannot delete yourself');
            return redirect()->route('admin.admin.index');
        }
        $admin->delete();

        alertMessage('info', 'Admin deleted');

        return redirect()->route('admin.admin.index');
    }
}
