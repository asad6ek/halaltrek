<?php

namespace App\Http\Controllers\Admin;

use App\actions\AdminAgentModifyAction;
use App\Http\Requests\AgentRequest;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Traits\Pond\PondUploadTrait;
use App\ViewModels\AgentSaveViewModel;
use Illuminate\Http\Request;

class AgentsController extends Controller
{
    use PondUploadTrait;

    public function __construct()
    {
        $this->pondInputName = 'file';
        $this->pondUrlPath = 'agents';
        $this->pondShouldDeleteOnAjax = true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Agent::orderByDesc('id')->paginate();

        return view('admin::agent.agent', compact('agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent = new Agent();

        return view('admin::agent.create', new AgentSaveViewModel($agent));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgentRequest $request)
    {
        $adminSaver = new AdminAgentModifyAction($request->validated());
        $adminSaver->execute();
        alertMessage('info', 'Agent Created');
        return redirect()->route('admin.agent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = Agent::getAgentById($id);

        return view('admin::agent.view', new AgentSaveViewModel($agent));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::getAgentById($id);

        return view('admin::agent.update', new AgentSaveViewModel($agent));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgentRequest $request, $id)
    {
        $adminSaver = new AdminAgentModifyAction($request->validated(), $id);
        $adminSaver->execute();
        alertMessage('info', 'Agent Updated');
        return redirect()->route('admin.agent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {

        $agent->delete();

        alertMessage('info', 'Agent deleted');

        return redirect()->route('admin.agent.index');
    }
}
