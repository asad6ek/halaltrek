<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AirlineRequest;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Models\Airline;
use App\Traits\Pond\PondUploadTrait;

class AirlineController extends Controller
{
    use PondUploadTrait;


    public function __construct()
    {
        $this->pondInputName = 'logo';
        $this->pondUrlPath = 'airline';
        $this->pondShouldDeleteOnAjax = true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Airline::query()->orderByDesc('id')->paginate();

        return view('admin::airlines.list', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Airline();

        return view('admin::airlines.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AirlineRequest $request)
    {

        $model = new Airline();
        $model->fill($request->validated());
        $model->logo = $this->decryptPondPath($request->input('logo'))['path'] ?? '';
        $model->save();
        alertMessage('info', 'Airline Created');
        return redirect()->route('admin.airlines.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Airline::findById($id);

        return view('admin::airlines.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Airline::findById($id);

        return view('admin::airlines.update', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AirlineRequest $request, $id)
    {
        $model = Airline::findById($id);
        $model->fill($request->validated());
        $model->logo = $this->decryptPondPath($request->input('logo'))['path'] ?? '';
        $model->save();

        alertMessage('info', 'Airline Updated');
        return redirect()->route('admin.airlines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Airline $country)
    {

        $country->delete();

        alertMessage('info', 'Airline deleted');

        return redirect()->route('admin.airlines.index');
    }
}
