<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AmenityRequest;
use App\Http\Controllers\Controller;
use App\Models\Amenity;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AmenityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $models = Amenity::query()->orderByDesc('id')->paginate();

        return view('admin::amenities.list', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $model = new Amenity();

        return view('admin::amenities.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AmenityRequest $request
     * @return RedirectResponse
     */
    public function store(AmenityRequest $request)
    {
        $data = $request->validated();
        Amenity::query()->create($data);
        alertMessage('info', 'Amenity Created');
        return redirect()->route('admin.amenities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Amenity $amenity
     * @return Factory|View
     */
    public function show(Amenity $amenity)
    {
        $model = $amenity;

        return view('admin::amenities.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Amenity $amenity
     * @return Factory|View
     */
    public function edit(Amenity $amenity)
    {
        $model = $amenity;
        return view('admin::amenities.update', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AmenityRequest $request
     * @param Amenity $amenity
     * @return RedirectResponse
     */
    public function update(AmenityRequest $request, Amenity $amenity)
    {
        $amenity->fill($request->validated());
        $amenity->save();

        alertMessage('info', 'Amenity Updated');
        return redirect()->route('admin.amenities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Amenity $amenity
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Amenity $amenity)
    {
        $amenity->delete();

        alertMessage('info', 'Amenity deleted');

        return redirect()->route('admin.amenities.index');
    }


}
