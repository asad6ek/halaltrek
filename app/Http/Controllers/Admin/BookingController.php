<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AirlineRequest;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Models\Airline;
use App\Models\Booking;
use App\Models\BookingRead;

class BookingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $new = Booking::query()->with(['mainContract'])->where('status',0)->orderByDesc('id') ->paginate();
        $complete = Booking::query()->with(['mainContract'])->where('status',1)->orderByDesc('id') ->paginate();
        BookingRead::where(['status_agent' => 1])->delete();
        BookingRead::where(['status_admin' => 0])->update(['status_admin' => 1]);


        return view('admin::booking.index', compact('new' , 'complete'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $model = Booking::findById($id);
        return view('admin::booking.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Booking::findById($id);

        return view('admin::airlines.update', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AirlineRequest $request, $id)
    {
        $model = Booking::findById($id);
        $model->fill($request->validated());
        $model->save();

        alertMessage('info', 'Airline Updated');
        return redirect()->route('admin.airlines.index');
    }
   /**
     * Update the specified resource in storage.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update_status($id)
    {
       Booking::where('id', $id)
            ->update(array('status'=>1));

        alertMessage('info', 'Booking Updated');
        return redirect()->route('admin.booking.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Airline $country)
    {

        $country->delete();

        alertMessage('info', 'Airline deleted');

        return redirect()->route('admin.airlines.index');
    }
}
