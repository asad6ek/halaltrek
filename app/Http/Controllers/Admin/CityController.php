<?php

namespace App\Http\Controllers\Admin;

use App\actions\AdminAdminModifyAction;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\CityRequest;
use App\Models\Admin;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\ViewModels\CityFormViewModel;
use Illuminate\Support\Facades\Auth;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = City::query()->orderByDesc('id')->paginate();

        return view('admin::cities.list', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new City();

        return view('admin::cities.create', new CityFormViewModel($model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $model = City::create($request->validated());
        alertMessage('info', 'City Created');
        return redirect()->route('admin.cities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = City::findById($id);

        return view('admin::cities.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = City::findById($id);

        return view('admin::cities.update', new CityFormViewModel($model));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        $model = City::findById($id);
        $model->fill($request->validated());
        $model->save();

        alertMessage('info', 'City Updated');
        return redirect()->route('admin.cities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {

        $city->delete();

        alertMessage('info', 'City deleted');

        return redirect()->route('admin.cities.index');
    }
}
