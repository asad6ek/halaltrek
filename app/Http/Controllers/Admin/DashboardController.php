<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 20:59
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\ViewModels\AdminDashboardViewModel;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index', new AdminDashboardViewModel());
    }
}
