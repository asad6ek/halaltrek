<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HotelRequest;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Models\Amenity;
use App\Models\Hotel;
use App\Models\HotelImage;
use Illuminate\Http\Request;
use Storage;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Hotel::query()->orderByDesc('id')->paginate();

        return view('admin::hotels.list', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Hotel();
        $amenities = Amenity::all();
        return view('admin::hotels.create', compact('model', 'amenities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HotelRequest $request)
    {

        $attribute = $request->get('attribute');
        $hotel = Hotel::create($request->validated());
        $this->saveImages($hotel, $request->input('images'));

        if (!is_null($attribute)) {
            foreach ($attribute as $value) {
                $hotel->amenities()->attach($value);
            }
        }

        alertMessage('info', 'Hotel Created');
        return redirect()->route('admin.hotels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Hotel::findById($id);

        return view('admin::hotels.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Hotel::findById($id);
        $amenities = Amenity::all();
        return view('admin::hotels.update', compact('model', 'amenities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HotelRequest $request, $id)
    {

        $model = Hotel::findById($id);
        $model->fill($request->validated());
        $this->saveImages($model, $request->input('images', []));
        $model->amenities()->detach();
        $model->save();
        $attribute = $request->get('attribute');
        if (!is_null($attribute)) {
            foreach ($attribute as $value) {
                $model->amenities()->attach($value);
            }
        }

        alertMessage('info', 'Hotel Updated');
        return redirect()->route('admin.hotels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {

        $hotel->delete();

        alertMessage('info', 'Hotel deleted');

        return redirect()->route('admin.hotels.index');
    }

    protected function saveImages(Hotel $hotel, $images)
    {

        $imagesObj = collect($images);
        $ids = $imagesObj->map(function ($item) {
            $pathArray = json_decode(base64_decode($item), 1);
            return $pathArray['id'] ?? null;
        })->filter(function ($item) {
            return $item;
        });


        if ($imagesObj->count()) {
            $hotelImages = $hotel->images()->whereNotIn('id', $ids->all())
                ->get();
            foreach ($hotelImages as $hotelImage) {
                $hotelImage->delete();
            }
        }

        if (!$images) {
            return null;
        }

        foreach ($images as $key => $image) {

            $model = null;

            try {
                $pathArray = json_decode(base64_decode($image), 1);
            } catch (\Exception $exception) {
                return $exception->getMessage();
            }

            if (empty($pathArray['path'])) {
                continue;
            }
            if (isset($pathArray['id'])) {
                $model = HotelImage::find($pathArray['id']);
            }

            if (empty($model)) {
                $model = new HotelImage();
            }

            $model->hotel_id = $hotel->id;
            $model->path = $pathArray['path'];
            $model->is_main = $key === 0;
            $model->type = 0;
            $model->save();
        }


    }


    public function processImage(Request $request)
    {
        \Debugbar::disable();
        $file = $request->file('images');
        if (is_array($file)) {
            $file = current($file);
        }

        $path_dir = 'hotels';
        $path = Storage::putFile($path_dir, $file, 'public');

        return base64_encode(json_encode(['path' => $path]));
    }

    public function revertImage(Request $request)
    {
        \Debugbar::disable();
        $path = $request->getContent();
        Storage::delete($path);
        return 'ok';
    }


    public function loadImage($id, $path)
    {
        \Debugbar::disable();
        try {
            $pathArray = json_decode(base64_decode($path), 1);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        throw_if(!isset($pathArray['path']), 'path not found');
        return Storage::download($pathArray['path']);
    }
}
