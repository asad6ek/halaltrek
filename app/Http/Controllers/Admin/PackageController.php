<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 21:13
 */

namespace App\Http\Controllers\Admin;


use App\DDD\Packages\Action\PackageAction;
use App\DDD\Packages\Models\Package;
use App\DDD\Packages\Repos\PackageRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Models\Agent;
use App\ViewModels\AdminPackageSaveViewModel as PackageSaveViewModel;

class PackageController extends Controller
{

    /**
     * @var PackageRepo
     */
    private $repo;

    public function __construct(PackageRepo $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($agent_id)
    {
        $models = $this->repo->getUserPackages($agent_id);
        return view('admin::package.index', compact('models','agent_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($agent_id)
    {
        $models = [
            Package::TYPE_HAJJ => new Package(),
            Package::TYPE_UMRAH => new Package(),
            Package::TYPE_TOURS => new Package(),
        ];

        return view('admin::package.create', new PackageSaveViewModel($models, $agent_id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageRequest $request, $agent)
    {
        $action = new PackageAction($request->validated(), $agent);
        $package = $action->execute();
        alertMessage('success', 'Package successfully saved');
        return redirect()->route('admin.packages.all',['agent'=>$agent, 'package'=>$package->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($agent, $id)
    {
        $model = $this->repo->getByIDAndAgent($id, $agent);
        return view('admin::package.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($agent, $id)
    {
        $model = $this->repo->getByIDAndAgent($id, $agent);
        $models = [
            Package::TYPE_HAJJ => $model->isForHajj() ? $model : new Package(),
            Package::TYPE_UMRAH => $model->isForUmrah() ? $model : new Package(),
            Package::TYPE_TOURS => $model->isForTours() ? $model : new Package(),
        ];
        return view('admin::package.update', new PackageSaveViewModel($models, $agent, $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageRequest $request, $agent, $id)
    {
        $action = new PackageAction($request->toArray(), $agent, $id);
        $package = $action->execute();
        alertMessage('success', 'Package successfully saved');
        return redirect()->route('admin.packages.all',['agent'=>$agent, 'package'=>$package->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($agent, $id)
    {
        $model = $this->repo->getByIDAndAgent($id, $agent);
        $model->delete();
        $model->payments()->delete();
        $model->hotels()->delete();

        alertMessage('error', 'Package successfully saved');
        return redirect()->route('admin.agent.packages.index', $agent);
    }

    public function list()
    {
        $models = $this->repo->getPackages();
        return view('admin::package.list', compact('models'));
    }

    public function agents()
    {
       $models =  Agent::all();

       return view('admin.package.agent-list',compact('models'));
    }
}
