<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-23
 * Time: 12:53
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\RecipientEmail;

class RecipientEmailsController extends Controller
{
    public function index()
    {
      $models =  RecipientEmail::latest()->paginate();

      return view('admin.recipient-emails.list',compact('models'));
    }
}
