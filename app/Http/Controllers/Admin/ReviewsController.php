<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-24
 * Time: 21:24
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewsRequest;
use App\Models\Review;
use Illuminate\Support\Facades\DB;

class ReviewsController extends Controller
{
    public function index()
    {
        //update
        $data = array('status_read' => 1);
        DB::table('reviews')
            ->where('is_mail_verified', true)
            ->where('status_read', 0)
            ->update($data);


        $models = Review::where('is_mail_verified', true)->latest()->paginate();
        return view('admin::reviews.list', compact('models'));
    }

    public function edit( Review $review)
    {
        $model = $review;
        return view('admin.reviews.update',compact('model'));
    }

    public function update(ReviewsRequest $request, Review $review)
    {
        $review->fill($request->validated());
        $review->save();
        alertMessage('success', 'Review updated');
        return redirect()->route('admin.review.index');
    }

    public function block(Review $review)
    {
        $review->status = Review::BLOCK;
        $review->save();
        $review->reviewable->updateReviewsAndAvarageRate();
        alertMessage('success', 'Review blocked');
        return redirect()->route('admin.review.index');
    }

    public function active(Review $review)
    {
        $review->status = Review::ACTIVE;
        $review->save();
        $review->reviewable->updateReviewsAndAvarageRate();
        alertMessage('success', 'Review activated');
        return redirect()->route('admin.review.index');
    }

    public function destroy(Review $review)
    {
        $review->delete();
        $review->reviewable->updateReviewsAndAvarageRate();
        alertMessage('success', 'Review deleted');
        return redirect()->route('admin.review.index');
    }
}
