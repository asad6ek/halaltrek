<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Carusel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = Carusel::query()
            ->orderByDesc('id')
            ->paginate();
        return view('admin.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Carusel $carusel)
    {    $request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'comment'=>'required',
    ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();

            $filename = time().'.'.$extension;
            $carusel->img = $filename;
            $carusel->comment = $request->input('comment');
            $carusel->save();
            $file->move('frontend/images/', $filename);
            alertMessage('info', 'Successfully deleted');
            return redirect()->route('admin.settings.index')
                ->with('success','Greate! Product created successfully.');
        }
        else{
            return $request;
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Carusel[]|\Illuminate\Database\Eloquent\Collection
     */
    public function show($id)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {

        $item = Carusel::findOrFail($id);
        $path = public_path('frontend/images/' . $item->img);
        if ($item && is_file($path)) {
            unlink($path);
        }

        $item->delete();

        alertMessage('info', 'Successfully deleted');
        return redirect()->route('admin.settings.index');
    }
}
