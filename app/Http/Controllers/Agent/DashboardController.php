<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-18
 * Time: 22:29
 */

namespace App\Http\Controllers\Agent;


use App\Http\Controllers\Controller;
use App\ViewModels\AdminDashboardViewModel;

class DashboardController extends Controller
{
    public function index()
    {
        return view('agent.dashboard');
    }
}
