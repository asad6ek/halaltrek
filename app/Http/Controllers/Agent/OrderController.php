<?php

namespace App\Http\Controllers\Agent;

use App\DDD\Packages\Models\Package;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\BookingRead;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $models = Booking::query()
            ->with(['mainContract'])
            ->whereHas('package', function ($q) {
                return $q->where('agent_id', $this->getUserId());
            })
            ->where('booking_type', Package::class)
            ->orderByDesc('id')
            ->paginate();

        $data_read = BookingRead::query()->where('status_admin','=',1)->get();

        foreach ($models as $model)
        {
             BookingRead::query()
                 ->where(['status_admin'=>1],['bookings_id'=>$model->id])
                 ->delete();
             BookingRead::query()
                 ->where('bookings_id','=',$model->id)
                 ->update(['status_agent'=>1]);
        }
        return view('agent.order.index', compact('models'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Booking::query()
            ->whereHas('package', function ($q) {
                return $q->where('agent_id', $this->getUserId());
            })
            ->where('id', $id)
            ->firstOrFail()->update();
        return view('admin.booking.view', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update_status($id)
    {
        Booking::where('id', $id)
            ->update(array('status_admin'=>1));
        alertMessage('info', 'Order Updated');
        return redirect()->route('agent.orders.index');
    }




    public function getUserId()
    {
        return \Auth::guard('agent')->id();
    }
}
