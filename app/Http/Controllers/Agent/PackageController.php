<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 21:13
 */

namespace App\Http\Controllers\Agent;


use App\DDD\Packages\Action\PackageAction;
use App\DDD\Packages\Models\Package;
use App\DDD\Packages\Repos\PackageRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\ViewModels\PackageSaveViewModel;

class PackageController extends Controller
{

    /**
     * @var PackageRepo
     */
    private $repo;

    public function __construct(PackageRepo $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->repo->getUserPackages($this->getAgentId());
        return view('agent::package.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $models = [
            Package::TYPE_HAJJ => new Package(),
            Package::TYPE_UMRAH => new Package(),
            Package::TYPE_TOURS => new Package(),
        ];

        return view('agent::package.create', new PackageSaveViewModel($models));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackageRequest $request)
    {
        $action = new PackageAction($request->toArray(), $this->getAgentId());
        $package = $action->execute();
        alertMessage('success', 'Package successfully saved');
        return redirect()->route('agent.packages.edit', $package->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->repo->getByIDAndAgent($id, $this->getAgentId());
        return view('agent::package.view', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->repo->getByIDAndAgent($id, $this->getAgentId());

        $models = [
            Package::TYPE_HAJJ => $model->isForHajj() ? $model : new Package(),
            Package::TYPE_UMRAH => $model->isForUmrah() ? $model : new Package(),
            Package::TYPE_TOURS => $model->isForTours() ? $model : new Package(),
        ];

        return view('agent::package.update', new PackageSaveViewModel($models, $id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PackageRequest $request, $id)
    {
        $action = new PackageAction($request->toArray(), $this->getAgentId(), $id);
        $package = $action->execute();
        alertMessage('success', 'Package successfully saved');
        return redirect()->route('agent.packages.edit', $package->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->repo->getByIDAndAgent($id, $this->getAgentId());
        $model->delete();
        $model->payments()->delete();
        $model->hotels()->delete();

        alertMessage('error', 'Package successfully saved');
        return redirect()->route('agent.packages.index');
    }

    protected function getAgentId()
    {
        return \Auth::guard('agent')->id();
    }
}
