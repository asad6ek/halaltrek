<?php

namespace App\Http\Controllers\Agent;

use App\actions\AdminAgentModifyAction;
use App\Http\Requests\AgentProfileRequest;
use App\Http\Requests\AgentRequest;
use App\Models\Agent;
use App\Http\Controllers\Controller;
use App\Traits\Pond\PondUploadTrait;
use App\ViewModels\AgentSaveViewModel;
use Illuminate\Http\Request;
use Storage;

class ProfileController extends Controller
{

    use PondUploadTrait;

    public function __construct()
    {
        $this->pondInputName = 'file';
        $this->pondUrlPath = 'agents';
        $this->pondShouldDeleteOnAjax = true;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $agent = Agent::getAgentById($this->getAgentId());

        return view('agent::profile.create', new AgentSaveViewModel($agent));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgentProfileRequest $request)
    {
        $adminSaver = new AdminAgentModifyAction($request->validated(), $this->getAgentId());
        $adminSaver->execute();
        alertMessage('info', 'Agent Updated');
        return redirect()->route('agent.profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Agent $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {

        $agent->delete();

        alertMessage('info', 'Agent deleted');

        return redirect()->route('admin.agent.index');
    }

    public function getAgentId()
    {
        return \Auth::guard('agent')->id();
    }

    public function loadImage($path)
    {
        \Debugbar::disable();
        $pathArray = $this->decryptPondPath($path);

        throw_if(!isset($pathArray['path']), 'path not found');
        return Storage::download($pathArray['path']);
    }
}
