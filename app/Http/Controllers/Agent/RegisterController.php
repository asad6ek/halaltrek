<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Agent;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function showForm1(Request $request)
    {
        $agent = $request->session()->get('agent');

        return view('agent::auth.register1', compact('agent'));
    }

    public function showForm2(Request $request)
    {
        $agent = $request->session()->get('agent');

        return view('agent::auth.register2', compact('agent'));
    }

    public function postForm1(Request $request)
    {

        $validate = $request->validate([
            'name' => 'required',
            'manager_name' => 'required',
            'address' => 'required',
            'postcode' => 'required|integer:max,6',
            'telephone' => 'required',
            'atol_number' => 'required',
            'website' => 'nullable|url',
            'town' => 'required',
            'mobile' => 'required',
            'registered_with_ministry' => 'required|bool'
        ]);

        if (empty($request->session()->get('agent'))) {
            $agent = new Agent();
            $agent->fill($validate);
            $request->session()->put('agent', $agent);
        } else {
            $agent = $request->session()->get('agent');
            $agent->fill($validate);
            $request->session()->put('agent', $agent);
        }
        return redirect('/register2');
    }

    public function postForm2(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|email|unique:agents,email',
            'password' => 'required|confirmed|min:6',
        ]);

        if (empty($request->session()->get('agent'))) {
            return redirect()->route('agent.register1');
        } else {
            $agent = $request->session()->get('agent');
            $agent->fill($validate);
            $agent->password = bcrypt($request->password);
            $agent->info = '';
            $request->session()->put('agent', $agent);
            $request->session()->remove('agent');
            $agent->save();
            Auth::guard('agent')->login($agent);
            return redirect('/');
        }
    }
}
