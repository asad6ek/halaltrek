<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 10:02
 */

namespace App\Http\Controllers\Api;


use App\Helpers\CollectionToSelect2Api;
use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\Request;

class AgentsController extends Controller
{
    public function select(Request $request)
    {
        $models = Agent::where('name','like', '%'.$request->input('term').'%')
            ->paginate(10);
        $transformer = new CollectionToSelect2Api($models, 'name');
        return $transformer->send();
    }
}
