<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 09:58
 */

namespace App\Http\Controllers\Api;


use App\Helpers\CollectionToSelect2Api;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use Illuminate\Http\Request;

class HotelsController extends Controller
{
    public function select(Request $request)
    {
        $models = Hotel::where('name','like', '%'.$request->input('term').'%')
            ->paginate(10);
        $transformer = new CollectionToSelect2Api($models, 'name');
        return $transformer->send();
    }
}
