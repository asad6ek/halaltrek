<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 12:05
 */

namespace App\Http\Controllers\Main;


use App\DDD\Packages\Models\Package;
use App\Events\NewBookingEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookPackageRequest;
use App\Http\Requests\CustomBookingRequest;
use App\Models\Booking;
use App\Models\BookingGuest;
use App\Models\BookingRead;
use App\Models\CustomQuote;
use App\Models\MainContract;
use App\ViewModels\BookingCustomViewModel;
use Carbon\Carbon;

class BookingController extends Controller
{
    public function bookPackageView(Package $package)
    {
        $roomSizes = Booking::roomSizes();
        return view('main.book-package', compact('package', 'roomSizes'));
    }

    public function bookPackage(BookPackageRequest $request, Package $package)
    {

        \DB::beginTransaction();

        $data = $request->validated();

        $booking = new Booking();
        $booking->fill($data);
        $booking->booking_id = $package->id;
        $booking->booking_type = get_class($package);
        $booking->save();

        BookingRead::query()->insert([
            'package_id' => $package->id,
            'bookings_id' => $booking->id,
            'agent_id' => $package->agent_id
        ]);

        $contract = new MainContract();
        $contract->fill(data_get($data, 'contract'));
        $contract->booking_id = $booking->id;
        $contract->password = bcrypt(data_get($data, 'password'));
        $contract->save();

        $booking_read = new BookingRead();
        $booking_read->bookings_id = $booking->id;
        $booking_read->package_id=$package->id;
        $booking_read->agent_id = $package->agent_id;
        $booking_read->save();
        foreach (data_get($data, 'visitor', []) as $visitor) {
            $bookingVisitor = new BookingGuest();
            $bookingVisitor->fill($visitor);
            $bookingVisitor->booking_id = $booking->id;
            $bookingVisitor->birth_date = $this->dateConverter(data_get($visitor, 'birth_date'));
//            $bookingVisitor->last_travel = $this->dateConverter(data_get($visitor, 'last_travel'));
            $bookingVisitor->passport_expire = $this->dateConverter(data_get($visitor, 'passport_expire'));
            $bookingVisitor->save();
        }

        event(new NewBookingEvent($booking));

        \DB::commit();

        alertMessage('success', 'Successfully booked');

        return redirect()->route('booking.success');
    }


    public function bookCustomView()
    {
        return view('main.book-custom', new BookingCustomViewModel());
    }

    public function bookCustom(CustomBookingRequest $request)
    {

        $data = $request->validated();

        \DB::beginTransaction();

        $customQuote = new CustomQuote();
        $customQuote->fill(data_get($data, 'custom'));
        $customQuote->flights = $request->input('flight');
        $customQuote->hotels = $request->input('hotel');
        $customQuote->depart = $this->dateConverter($request->input('custom.depart'));
        $customQuote->return = $this->dateConverter($request->input('custom.return'));
        $customQuote->save();

        $booking = new Booking();
        $booking->fill($data);
        $booking->booker_name = $request->input('contract.name');
        $booking->booking_id = $customQuote->id;
        $booking->booking_type = get_class($customQuote);
        $booking->save();
        $contract = new MainContract();
        $contract->fill(data_get($data, 'contract'));
        $contract->booking_id = $booking->id;
        $contract->password = bcrypt(data_get($data, 'password'));
        $contract->save();

        foreach (data_get($data, 'visitor', []) as $visitor) {
            $bookingVisitor = new BookingGuest();
            $bookingVisitor->fill($visitor);
            $bookingVisitor->birth_date = $this->dateConverter(data_get($visitor,'birth_date'));
//            $bookingVisitor->last_travel = $this->dateConverter(data_get($visitor,'last_travel'));
            $bookingVisitor->passport_expire = $this->dateConverter(data_get($visitor,'passport_expire'));
            $bookingVisitor->booking_id = $booking->id;
            $bookingVisitor->save();
        }

        event(new NewBookingEvent($booking));

        \DB::commit();
        alertMessage('success', 'Successfully booked');

        return redirect()->route('booking.success');
    }

    public function success()
    {
        return view('main.success');
    }

    public function dateConverter($date)
    {
        if (!$date) {
            return null;
        }
        return Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
    }

}
