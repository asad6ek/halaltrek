<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-14
 * Time: 22:28
 */

namespace App\Http\Controllers\Main;


use App\DDD\Packages\Repos\PackageRepo;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Controller;
use App\Models\Carusel;
use App\ViewModels\MainPageViewModel;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * @var PackageRepo
     */
    private $packageRepo;

    public function __construct(PackageRepo $packageRepo)
    {
        $this->packageRepo = $packageRepo;
    }

    public function index(Request $request)
    {
        $ass = Carusel::all();
        return view('welcome', new MainPageViewModel($this->packageRepo, $request),compact('ass'));
    }

    public function search(Request $request){
//
    }

}
