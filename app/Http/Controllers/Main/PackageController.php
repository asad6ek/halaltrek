<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-21
 * Time: 17:35
 */

namespace App\Http\Controllers\Main;


use App\DDD\Packages\Models\Package;
use App\Http\Controllers\Controller;

class PackageController extends Controller
{
    public function view(Package $package)
    {

        return view('main.package', compact('package'));
    }
}
