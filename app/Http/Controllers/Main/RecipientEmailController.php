<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-14
 * Time: 15:24
 */

namespace App\Http\Controllers\Main;


use App\Http\Controllers\Controller;
use App\Http\Requests\RecipientRequest;
use App\Models\RecipientEmail;

class RecipientEmailController extends Controller
{
    public function store(RecipientRequest $request)
    {

       $model = RecipientEmail::firstOrCreate(
            [
                'email' => $request->input('email')
            ],
            [
                'email' => $request->input('email'),
                'status' => RecipientEmail::ACTIVE
            ]
        );

        if ($request->ajax()) {
            return [
                'success' => true
            ];
        }


        alertMessage('success', 'Your email has been added to news letter');
        return redirect()->route('main');
    }
}
