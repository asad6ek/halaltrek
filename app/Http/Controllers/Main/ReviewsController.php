<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-24
 * Time: 19:39
 */

namespace App\Http\Controllers\Main;


use App\DDD\Packages\Models\Package;
use App\Events\ReviewAddedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReviewsRequest;
use App\Models\Agent;
use App\Models\Review;
use App\ReviewEmail;

class ReviewsController extends Controller
{
    public function save(ReviewsRequest $request, Package $package)
    {

        $validatedItems = $request->validated();

        $model = new Review();
        $model->fill($validatedItems);
        $model->status = Review::PENDING;
        $model->reviewable_type = Agent::class;
        $model->reviewable_id = $package->agent->id;
        $model->save();

        alertMessage('success', 'Your review received. Please verify your email');
        event(new ReviewAddedEvent($model));
        return redirect()->route('package.info', $package->id);
    }

    public function verify($token)
    {
        $model = ReviewEmail::checkToken($token);
        throw_if(!$model, 'wrong token');

        $review = Review::findOrFail($model->review_id);
        $review->is_mail_verified = true;
        $review->save();
        alertMessage('success', 'Your email verified and review has sent to admin');
        return redirect()->route('main');
    }
}
