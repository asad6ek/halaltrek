<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 12:42
 */

namespace App\Http\Controllers\Main;


use App\DDD\Packages\Repos\PackageRepo;
use App\Http\Controllers\Controller;
use App\ViewModels\SearchViewModel;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        return view('main.search', new SearchViewModel($request->all()));
    }

    public function ajax(Request $request, PackageRepo $repo)
    {
        $models = $repo->filter($request->all());

        return [
            'success' => true,
            'content' => view('partials.search-ajax', compact('models'))->toHtml(),
            'page' => $request->input('page', 1),
            'has-next' => $models->hasMorePages(),
            'next-page' => $models->hasMorePages() ? $models->currentPage() + 1 : null,
            'total-count' => $models->total(),
        ];
    }
}
