<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AgentProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => [
                'email',
                'required',
                Rule::unique('agents','email')->ignore(\Auth::guard('agent')->id()),
            ],
            'password' => [
                'nullable',
                'string',
            ],
            'address' => 'string|required',
            'manager_name' => 'string|required',
            'postcode' => 'string|required',
            'telephone' => 'string|required',
            'atol_numbe' => 'string|nullable',
//            'city_id'=>'required|exists:cities,id',
            'info'=>'nullable|string',
            'website' => 'nullable|url',
            'town' => 'required',
            'mobile' => 'required',
            'registered_with_ministry' => 'required|bool'
        ];
    }
}
