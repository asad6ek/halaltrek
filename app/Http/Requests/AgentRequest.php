<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => [
                'email',
                'required',
                Rule::unique('agents','email')->ignore($this->route('agent')),
            ],
            'password' => [
                Rule::requiredIf(!$this->route('agent'))
            ],
            'address' => 'string|required',
            'manager_name' => 'string|nullable',
            'postcode' => 'string|required',
            'telephone' => 'string|required',
            'atol_number' => 'string|nullable',
            'atol_expire' => 'string|nullable',
            'file'=>'nullable|string',
            'info'=>'required',
//            'city_id'=>'required|exists:cities,id',
            'approved'=>'nullable|boolean',
            'ministry_approved'=>'nullable|boolean',
            'website' => 'nullable|url',
            'town' => 'required',
            'mobile' => 'required',
            'registered_with_ministry' => 'required|bool'

        ];
    }
}
