<?php

namespace App\Http\Requests;

use App\Models\Booking;
use App\Rules\RequestRoomValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'booker_name' => 'required|string',
//            'room_size' => [
//                'required',
//                Rule::in(Booking::roomSizes()),
//            ],
            'special_req' => 'nullable',

            'rooms' => [
                'required',
                'array',
                new RequestRoomValidator(),
            ],
            'rooms.*.number' => 'nullable|numeric|max:100',
            'rooms.*.size' => ['required', Rule::in(Booking::roomSizes())],

            'contract.name' => 'required|string:min,2:max:3',
            'contract.address' => 'required',
            'contract.contact' => 'required',
            'contract.email' => 'required|email',
            'contract.password' => 'required|string:min,6',
            'contract.group_contact' => 'required',

            'visitor.*.name' => 'required',
//            'visitor.*.relation' => 'required',
            'visitor.*.birth_date' => 'required|date_format:d-m-Y|before:today',
            'visitor.*.last_travel_input' => 'nullable|boolean',
//            'visitor.*.last_travel' => [
//                'required_if:visitor.*.last_travel_input,==,1',
//                'nullable',
//                'date_format:d-m-Y',
//                'before:today',
//            ],
            'visitor.*.passport_expire' => 'required|date_format:d-m-Y',

        ];
    }
}
