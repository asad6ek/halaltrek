<?php

namespace App\Http\Requests;

use App\Models\Booking;
use App\Models\CustomQuote;
use App\Rules\RequestRoomValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomBookingRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'contract.name' => 'required|string:min,2',
            'contract.address' => 'required',
            'contract.contact' => 'required',
            'contract.group_contact' => 'required',
            'contract.email' => 'required|email',
            'contract.password' => 'required|string:min,6',
            'special_req' => 'nullable',

            'flight.has_flight' => [
                'boolean',
            ],
            'flight.type' => [
                'required_if:flight.has_flight,==,0',
                'boolean',
            ],
            'flight.detail' => [
                'required_if:flight.has_flight,==,1',
                'nullable',
                'string',

            ],

            'hotel.has_hotels' => [
                'boolean',
            ],
            'hotel.type' => [
                'required_if:hotel.has_hotels,==,0',
                'boolean',
            ],
            'hotel.hotel_makkah' => [
                'required_if:hotel.has_hotels,==,1',
                'exists:hotels,id'
            ],
            'hotel.hotel_madinah' => [
                'required_if:hotel.has_hotels,==,1',
                'exists:hotels,id'
            ],


//            'custom.hotel_madinah' => 'required|exists:hotels,id',
            'custom.nights_madinah' => 'required|numeric',
//            'custom.hotel_makkah' => 'required|exists:hotels,id',
            'custom.nights_makkah' => 'required|numeric',
            'custom.depart' => 'required|date_format:d-m-Y|after:today',
            'custom.return' => 'required|date_format:d-m-Y|after:custom.depart',

            'custom.transfers' => 'nullable|boolean',

            'custom.comment' => 'nullable|string',

            'rooms' => [
                'required',
                'array',
                new RequestRoomValidator(),
            ],
            'rooms.*.number' => 'nullable|numeric|max:100',
            'rooms.*.size' => ['required', Rule::in(Booking::roomSizes())],

            'visitor.*.name' => 'required',
            'visitor.*.relation' => 'required',
            'visitor.*.birth_date' => 'required|date_format:d-m-Y|before:today',
            'visitor.*.last_travel_input' => 'nullable|boolean',
            'visitor.*.last_travel' => [
                'required_if:visitor.*.last_travel_input,==,1',
                'nullable',
                'date_format:d-m-Y',
                'before:today',
            ],
            'visitor.*.passport_expire' => 'required|date_format:d-m-Y|after:custom.return',
        ];
    }
}
