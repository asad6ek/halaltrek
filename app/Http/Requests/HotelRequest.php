<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>[
                'required',
                Rule::unique('hotels','name')->ignore($this->route('hotel')),
            ],
            'address'=>'required|string',
            'description'=>'required',
            'excerpt'=>'required',
            'star'=>'required|int',
//            'images'=>'required'
        ];
    }
}
