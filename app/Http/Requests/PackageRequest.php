<?php

namespace App\Http\Requests;

use App\DDD\Packages\Models\Package;
use App\Models\Airline;
use App\Models\PackageAirline;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ziyarat_type' => [
                'required',
                Rule::in(Package::packageTypes()),
            ],
            'name'         => 'required|string',
            'country_id'   => '',

            'date_from'            => 'required|date_format:d-m-Y',
            'date_to'              => 'required|date_format:d-m-Y|after:date_from',
            'num_days'             => 'required|numeric|max:100',
            'package_class'        => 'required|string',
            'description'          => ['nullable',],
            'hotel_basis_makkah'   => [
                'required',
                Rule::in(Package::hotelBasesList()),
            ],
            'hotel_basis_madinah'  => [
                'required',
                Rule::in(Package::hotelBasesList()),
            ],
            'first_visit'          => [
                'required',
                Rule::in(Package::firstVisitDesire()),
            ],
            'hotels.*'             => 'required|exists:hotels,id',
            'ziyorah_included'     => 'required|boolean',
            'flight.*.airline'     => [
                'required',
                'exists:airlines,id',
            ],
            'flight.*.from'        => 'required',
            'flight.*.to'          => 'required',
            'flight.*.from_date'   => 'required|date_format:d-m-Y',
            'flight.*.return_date' => 'required|date_format:d-m-Y',
            'flight.*.type'        => 'required',
            'flight.*.description' => [
                'required_if:flight.*.type,==,' . PackageAirline::TYPE_INDIRECT,
            ],
            'payment.*.price'      => 'nullable|numeric',
            'payment.*.currency'   => 'nullable|string',

            'options.*' => 'nullable|numeric',
            'city_id.*' => Rule::exists('cities', 'id'),

//            'is_shifting',
//            'num_days_aziziya',
        ];
    }
}
