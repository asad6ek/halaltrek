<?php

namespace App\Listeners;

use App\Events\ReviewAddedEvent;
use App\Mail\ReviewUserEmailVerify;
use App\ReviewEmail;
use Faker\Provider\Text;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class ReviewAdderMailSender
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle(ReviewAddedEvent $event)
    {
        $review = $event->review;
        $model = new ReviewEmail();
        $model->review_id = $review->id;
        $model->token = Str::random(64);
        $model->save();

        \Mail::to($review->email)->send(new ReviewUserEmailVerify($review, $model->token));
    }
}
