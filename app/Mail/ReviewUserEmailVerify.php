<?php

namespace App\Mail;

use App\Models\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReviewUserEmailVerify extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Review
     */
    private $review;
    private $token;

    /**
     * Create a new message instance.
     *
     * @param Review $review
     */
    public function __construct(Review $review, $token)
    {
        $this->review = $review;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
            ->from(config('mail.from.address'))
            ->to($this->review->email)
            ->view('mails.review-email')
        ->with('review', $this->review)
        ->with('token', $this->token);
    }
}
