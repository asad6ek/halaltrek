<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\Agent
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $telephone
 * @property string $postcode
 * @property string $manager_name
 * @property string $atol_number
 * @property string|null $atol_expire
 * @property string $password
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property int $ministry_approved
 * @property int $approved
 * @property string $town
 * @property string $info
 * @property string $mobile
 * @property string $website
 * @property int $registered_with_ministry
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereAtolExpire($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereAtolNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereManagerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereMinistryApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @property string|null $logo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereStatus($value)
 * @property int $count_review
 * @property float $average_review
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereAverageReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereCountReview($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereInfo($value)
 * @property-write mixed $file
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property int|null $city_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereRegisteredWithMinistry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Agent whereWebsite($value)
 */
class Agent extends Authenticatable
{
    use Notifiable;

    protected $guard = 'agent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'address', 'manager_name', 'postcode', 'telephone', 'atol_number', 'atol_expire',
        'file', 'info','city_id','town','mobile','website','registered_with_ministry'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param $id
     * @return Agent
     * @throws ModelNotFoundException
     */
    public static function getAgentById($id)
    {
        return Agent::query()->findOrFail($id);
    }

    public function setFileAttribute($file)
    {
        $item = json_decode(base64_decode($file), 1);

        if (isset($item['path'])) {
            $this->logo = $item['path'];
        }
    }

    public function getLogo()
    {
        if ($this->logo) {
            return 'storage/' . $this->logo;
        }
        return '/frontend/images/image.svg';
    }

    public function getLogoPond()
    {
        if (!$this->logo) {
            return null;
        }

        return base64_encode(json_encode([
            'id' => $this->id,
            'path' => $this->logo
        ]));
    }

    protected function deleteFile()
    {
        if (\Storage::exists($this->logo)) {
            \Storage::delete($this->logo);
        }
    }

    public function delete()
    {
        $this->deleteFile();
        return parent::delete();
    }

    public function getReviewCount()
    {
        return $this->count_review;
    }

    public function getReviewAvarage()
    {
        return $this->average_review;
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    public function activeReviews()
    {
        return $this->reviews()
            ->where('status', Review::ACTIVE);
    }

    public function updateReviewsAndAvarageRate()
    {
        $data = $this->activeReviews()
            ->select([\DB::raw('count(*) as count'), \DB::raw('avg(rate) as avg')])
            ->first()
            ->toArray();

        $this->count_review = $data['count'];
        $this->average_review = round($data['avg'], 2);
        $this->save();
    }

}
