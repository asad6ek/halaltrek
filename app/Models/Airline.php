<?php

namespace App\Models;

use App\Traits\ListTrait;
use App\Traits\ModelRepoTraits;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Airline
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $info
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline whereUpdatedAt($value)
 */
class Airline extends Model
{
    use ListTrait, ModelRepoTraits;

    protected $guarded = [
        'id'
    ];

    public function getLogoPond()
    {
        if (!$this->logo) {
            return null;
        }

        return base64_encode(json_encode([
            'id' => $this->id,
            'path' => $this->logo
        ]));
    }

    protected function deleteFile()
    {
        if (\Storage::exists($this->logo)) {
            \Storage::delete($this->logo);
        }
    }

    public function delete()
    {
        $this->deleteFile();
        return parent::delete();
    }

    public function getUrl()
    {
        return  '/storage/'.($this->logo);
    }
}
