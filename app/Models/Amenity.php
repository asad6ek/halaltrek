<?php

namespace App\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;


/**
 * App\Models\Amenity
 *
 * @property int $id
 * @property string $label
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Amenity newModelQuery()
 * @method static Builder|Amenity newQuery()
 * @method static Builder|Amenity query()
 * @method static Builder|Amenity whereAddress($value)
 * @method static Builder|Amenity whereCreatedAt($value)
 * @method static Builder|Amenity whereId($value)
 * @method static Builder|Amenity whereName($value)
 * @method static Builder|Amenity whereTitle($value)
 * @method static Builder|Amenity whereUpdatedAt($value)
 * @mixin Eloquent
 * @property Hotel|null $hotels
 * @property-read int|null $hotels_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Amenity whereLabel($value)
 */
class Amenity extends Model
{
    protected $fillable = ['label', 'title'];

    public function hotels(): BelongsToMany
    {
        return $this->belongsToMany(Hotel::class, 'amenity_hotel', 'amenity_id', 'hotel_id');
    }

    public static function findById($id)
    {
        return self::query()->where('id', $id)->firstOrFail();
    }
}
