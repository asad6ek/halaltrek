<?php

namespace App\Models;

use App\DDD\Packages\Models\Package;
use App\Traits\ModelRepoTraits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Booking
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $booker_name
 * @property string $booking_type
 * @property int $booking_id
 * @property string $special_req
 * @property array $rooms
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereBookerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereBookingType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereRoomSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereSpecialReq($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BookingGuest[] $guests
 * @property-read int|null $guests_count
 * @property-read \App\Models\MainContract $mainContract
 * @property int $status_admin
 * @property string|null $options
 * @property int $status
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $order
 * @property-read \App\DDD\Packages\Models\Package $package
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereRooms($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Booking whereStatusAdmin($value)
 */
class Booking extends Model
{
    use ModelRepoTraits;

    protected $fillable = [
        'booker_name',
//        'room_size',
        'special_req',
        'rooms',
    ];

    protected $casts = [
        'rooms' => 'array',
    ];

    public function guests()
    {
        return $this->hasMany(BookingGuest::class);
    }

    public function mainContract()
    {
        return $this->hasOne(MainContract::class);
    }

    public static function roomSizes()
    {
        return [
            "Single" => "Single",
            "Double" => "Double",
            "Triple" => "Triple",
            "Quad" => "Quad",
        ];
    }

    public function getType()
    {
        return Str::title(str_replace('_', ' ', Str::snake(class_basename($this->booking_type))));
    }

    public function order()
    {
        return $this->morphTo('booking');
    }

    public function isPackage()
    {
        return $this->order instanceof Package;
    }

    public function isCustomQuote()
    {
        return $this->order instanceof CustomQuote;
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'booking_id');
    }
}
