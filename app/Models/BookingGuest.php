<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookingGuest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $booking_id
 * @property string $birth_date
 * @property string $name
 * @property string $host
 * @property string $relation
 * @property string $last_travel
 * @property string $passport_expire
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereHost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereLastTravel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereRelation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereUpdatedAt($value)
 * @property-read \App\Models\Booking $booking
 * @property string|null $options
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingGuest wherePassportExpire($value)
 */
class BookingGuest extends Model
{
    protected $fillable = [
        'name',
        'birth_date',
        'host',
//        'relation',
//        'last_travel',
        'passport_expire',
    ];

//    protected $casts = [
//        'last_travel'=>'date',
//        'passport_expire'=>'date',
//        'birth_date'=>'date',
//    ];


    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
