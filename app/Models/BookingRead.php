<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\BookingRead
 *
 * @property int bookings_id
 * @property int package_id
 * @property int $bookings_id
 * @property int $package_id
 * @property int $status_admin
 * @property int $status_agent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $agent_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereAgentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereBookingsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereStatusAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereStatusAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookingRead whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookingRead extends Model
{
    public $table='bookings_read';



}
