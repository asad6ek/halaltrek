<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Carusel
 *
 * @property string img
 * @property mixed comment
 * @property int $id
 * @property string $img
 * @property string|null $comment
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Carusel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Carusel extends Model
{
    public   $table = 'carusel';
    protected $fillable=[
        'id',
        'img',
        'comment',
    ];
    public $timestamps = false;

}
