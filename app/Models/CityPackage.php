<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CityPackage
 *
 * @package App\Models
 * @property $city_id
 * @property $package_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CityPackage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CityPackage extends Model
{

}
