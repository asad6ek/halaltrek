<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomQuote
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $hotel_madinah
 * @property string $depart
 * @property string $return
 * @property string $first_visit
 * @property int $transfers
 * @property string $ziyorah
 * @property string $comment
 * @property array $flights
 * @property array $hotels
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereDepart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereFirstVisit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereHotelMadinah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereHotelMakka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereNightsMadina($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereNightsMakka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereReturn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereTransfers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereZiyorah($value)
 * @property int $nights_makkah
 * @property int $nights_madinah
 * @property int $hotel_makkah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereHotelMakkah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereNightsMadinah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereNightsMakkah($value)
 * @property string|null $options
 * @property-read \App\Models\Hotel $hotelMadinah
 * @property-read \App\Models\Hotel $hotelMakkah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereFlights($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereHotels($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CustomQuote whereOptions($value)
 */
class CustomQuote extends Model
{
    protected $fillable = [
        'nights_madinah',
        'nights_makkah',
        'depart',
        'return',
        'first_visit',
        'transfer',
        'ziyorah',
        'comment',
        'transfers',
        'hotels',
        'flights',
    ];

    protected $casts = [
        'hotels' => 'array',
        'flights' => 'array',
    ];


    public static function firstVisitOptions()
    {
        return [
            'Makkah',
            'Madinah'
        ];
    }

    public static function ziyarahOptions()
    {
        return ['Madinah', 'Makkah', 'both'];
    }

    public function hotelMadinah()
    {
        return $this->belongsTo(Hotel::class, 'hotel_madinah');
    }

    public function hotelMakkah()
    {
        return $this->belongsTo(Hotel::class, 'hotel_makkah');
    }

    public static function hotelTypes()
    {
        return [
            5=>'5 Star',
            4=>'4 Star',
            3=>'3 Star',
            0=>'I don`t mind',
        ];
    }

    public static function flightTypes()
    {
        return [
            'Direct',
            'Indirect',
            'I Don’t Mind',
        ];
    }
}
