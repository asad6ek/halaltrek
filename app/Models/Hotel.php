<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use phpDocumentor\Reflection\Types\This;

/**
 * App\Models\Hotel
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $lat
 * @property string $long
 * @property int $star
 * @property int $rating
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereStar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\HotelImage[] $images
 * @property-read int|null $images_count
 * @property string $description
 * @property string $excerpt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Hotel whereExcerpt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Amenity[] $amenities
 * @property-read int|null $amenities_count
 */
class Hotel extends Model
{
    protected $guarded = [
        'id'
    ];

    public static function list()
    {
        return self::query()->pluck('name', 'id');
    }

    public static function findById($id)
    {
        return self::query()->where('id', $id)->firstOrFail();
    }

    public function images()
    {
        return $this->hasMany(HotelImage::class);
    }

    public function getFirstImgUrl()
    {
        if (!$this->images->count()) {
            return '';
        }
        return '/storage/' . $this->images[0]->getPath();
    }

    public static function getHotelName($id)
    {
        if (!$id) {
            return '';
        }

        $model = self::find($id);

        return optional($model)->name;
    }

    public function amenities(): BelongsToMany
    {
        return $this->belongsToMany(Amenity::class, 'amenity_hotel', 'hotel_id', 'amenity_id');
    }

    public function hotelAmenity($id)
    {
        $amenity = $this->amenities()->where('amenity_id', $id)->first();
        return $amenity ?? null;
    }
}
