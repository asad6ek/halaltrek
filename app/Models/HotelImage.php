<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\HotelImage
 *
 * @property int $id
 * @property int|null $hotel_id
 * @property string $path
 * @property int $is_main
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereHotelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereIsMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HotelImage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HotelImage extends Model
{
    public function getPath()
    {
        return $this->path;
    }

    public function getFullPath()
    {
        return '/storage/' . $this->getPath();
    }

    public function filePondPath()
    {
        return base64_encode(json_encode(['path' => $this->getPath(), 'id' => $this->id]));
    }

    public function delete()
    {
        $this->deleteFile();
        return parent::delete();
    }

    public function deleteFile()
    {
        if (\Storage::exists($this->path)) {
            \Storage::delete($this->path);
        }
    }
}
