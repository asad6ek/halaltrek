<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MainContract
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $booking_id
 * @property string $address
 * @property string $contact
 * @property string $email
 * @property string $password
 * @property string $group_contact
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereUpdatedAt($value)
 * @property-read \App\Models\Booking $booking
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MainContract whereGroupContact($value)
 */
class MainContract extends Model
{
    protected $fillable = [
        'name',
        'address',
        'contact',
        'email',
        'group_contact'
    ];

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
