<?php

namespace App\Models;

use App\Traits\ListTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PackageAirline
 *
 * @property int                             $id
 * @property int                             $package_id
 * @property string                          $from
 * @property string                          $to
 * @property int                             $airline_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereAirlineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Airline        $airline
 * @property mixed $from_date
 * @property string|null $type
 * @property string|null $description
 * @property mixed|null $return_date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereFromDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereReturnDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageAirline whereType($value)
 */
class PackageAirline extends Model
{
    use ListTrait;

    const TYPE_DIRECT = 'Direct';
    const TYPE_INDIRECT = 'Indirect';
    const TYPE_TO_BE_CONFIRMED = 'To be confirmed';

    protected $casts = [
        'from_date'   => 'datetime:d-m-Y',
        'return_date' => 'datetime:d-m-Y',
    ];


    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }

    public function dateFrom()
    {
        return optional($this->from_date)->format('jS M Y');
    }

    public function dateTo()
    {
        return optional($this->return_date)->format('jS M Y');
    }

    public static function types()
    {
        return [
            self::TYPE_DIRECT          => self::TYPE_DIRECT,
            self::TYPE_INDIRECT        => self::TYPE_INDIRECT,
            self::TYPE_TO_BE_CONFIRMED => self::TYPE_TO_BE_CONFIRMED,
        ];
    }
}
