<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PackageHotel
 *
 * @property int $id
 * @property int $package_id
 * @property int $hotel_id
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel whereHotelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageHotel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Hotel $hotel
 * @property-read mixed $type_text
 */
class PackageHotel extends Model
{
    const TYPE_MAKKA = 1;
    const TYPE_MADINA = 2;

    public static function types()
    {
        return [
            self::TYPE_MAKKA => 'Makka',
            self::TYPE_MADINA => 'Madina'
        ];
    }

    public static function convertTypeText($typeText)
    {
        $types = array_flip(self::types());

        return data_get($types, ucfirst(strtolower($typeText)));
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function getTypeTextAttribute()
    {
        return data_get(self::types(), $this->type);
    }


}
