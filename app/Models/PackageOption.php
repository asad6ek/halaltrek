<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PackageOption
 *
 * @property int $id
 * @property int $package_id
 * @property int $seminar
 * @property int $goody_bag
 * @property int $breakfast
 * @property int $ground_transport
 * @property int $hadiy
 * @property int $visit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereGoodyBag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereSeminar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereBreakfast($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereGroundTransport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereHadiy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOption whereVisit($value)
 */
class PackageOption extends Model
{
    //
}
