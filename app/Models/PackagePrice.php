<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PackagePrice
 *
 * @property int $id
 * @property int $package_id
 * @property int $type
 * @property string $name
 * @property float $price
 * @property string $currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackagePrice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PackagePrice extends Model
{

}
