<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecipientEmail
 *
 * @property int $id
 * @property string $email
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipientEmail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RecipientEmail extends Model
{
    const ACTIVE= 1;
    const NOTACTIVE= 2;
    protected $fillable = [
        'email','status',
    ];
}
