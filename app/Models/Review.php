<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property int $reviewable_id
 * @property string $reviewable_type
 * @property string $name
 * @property string $email
 * @property string $status
 * @property string $status_read
 * @property string $comment
 * @property int $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review verified()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereReviewableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereReviewableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $is_mail_verified
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $reviewable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereIsMailVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereStatusRead($value)
 */
class Review extends Model
{
    const ACTIVE = 1;
    const PENDING = 0;
    const BLOCK = 2;

    protected $fillable = [
        'name', 'email', 'comment', 'rate',
    ];

    public function scopeVerified($query)
    {
        $query->where('is_mail_verified', true);
    }

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function timeToNow()
    {
        return $this->created_at->diffForHumans();
    }

    public function isActive()
    {
        return (int)$this->status === self::ACTIVE;
    }

    public function isPending()
    {
        return (int)$this->status === self::PENDING;
    }

    public function isMailVerified()
    {
        return (int)$this->is_mail_verified;
    }

    public static function statusList()
    {
        return [
            self::ACTIVE => 'Active',
            self::PENDING => 'Pending',
            self::BLOCK => 'Blocked',
        ];
    }

    public function status()
    {
        return data_get(self::statusList(), $this->status);
    }
}
