<?php

namespace App\Providers;

use App\Events\NewBookingEvent;
use App\Events\ReviewAddedEvent;
use App\Listeners\NewBookingListener;
use App\Listeners\ReviewAdderMailSender;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ReviewAddedEvent::class => [
            ReviewAdderMailSender::class,
        ],
        NewBookingEvent::class=>[
            NewBookingListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
