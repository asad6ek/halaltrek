<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 12:30
 */

namespace App\QueryFilters;


use App\Traits\DateRangeParser;
use Illuminate\Database\Eloquent\Builder;
use JascoB\QueryFilter\QueryFilter;

class PackageQueryFilter extends QueryFilter
{
    use DateRangeParser;

    protected $filterableFields = ['type', 'datarange', 'hotel', 'airline', 'agent', 'package', 'duration', 'package_class'];

    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function airline($query, $value)
    {
        return $query->whereHas('flights', function ($query) use ($value) {
            $query->where('airline_id', $value);
        });
    }


    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function hotel($query, $value)
    {
        return $query->whereHas('hotels', function ($query) use ($value) {
            $query->where('hotel_id', $value);
        });
    }


    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function daterange($query, $value)
    {
        $dates = $this->parseDateRange($value);

        if (!$dates) {
            return $query;
        }

        return $query->where('date_from', '>=', $dates[0])
            ->where('date_to', '<=', $dates[1]);
    }


    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function agent($query, $value)
    {
        return $query->where('agent_id', $value);
    }



    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function type($query, $value)
    {
        return $query->where('ziyarat_type', $value);
    }



    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function duration($query, $value)
    {
        if (!trim($value)) {
            return $query;
        }

        return $query->where('num_days', $value);
    }



    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function package($query, $value)
    {
        if (!trim($value)) {
            return $query;
        }

        if ($value === '5') {
            return $query->where('hotels.star', '=', 5)
                ->havingRaw('count(hotels.id) = 2');
        }

        if ($value === 'premium-economy') {
            return $query->where('hotels.star', '=', 5)
                ->havingRaw('count(hotels.id) = 1');
        }

        if ($value === 'economy') {
            return $query->where('hotels.star', '<', 5)
                ->havingRaw('count(hotels.id) = 2');
        }

        return $query->whereHas('hotels.hotel', function ($query) use ($value) {
            return $query->where('star', $value);
        });


    }

    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */


    public function package_class($query, $value)
    {
        if (!trim($value)) {
            return $query;
        }

        $query->where('package_class', $value);

    }


    /**
     * @param $query Builder
     * @param $value
     * @return mixed
     */
    public function sort($query, $value)
    {
        if (!trim($value)) {
            return $query;
        }

        if ($value === '-price') {
            $query->orderBy('min_price', 'desc');
        } elseif ($value === 'price') {
            $query->orderBy('min_price', 'asc');
        }

    }

}
