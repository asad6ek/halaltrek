<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReviewEmail
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $review_id
 * @property string $token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail whereReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReviewEmail whereUpdatedAt($value)
 */
class ReviewEmail extends Model
{
    public static function checkToken($token)
    {
        return self::where('token', $token)->first();
    }
}
