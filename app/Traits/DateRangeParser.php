<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-08
 * Time: 14:33
 */

namespace App\Traits;


trait DateRangeParser
{
    public function parseDateRange($dateRange, $deliminator = ' - ', $fromFormat = 'd-m-Y', $toFormat = 'Y-m-d')
    {
        $dateRangeArr = $this->dateRangeDateParser($dateRange, $deliminator);

        if (count($dateRangeArr) !== 2){
            return [];
        }

        $fromFormat = $this->dateRangeFormatChecker('dateRangeFromFormat', $fromFormat);
        $toFormat = $this->dateRangeFormatChecker('dateRangeToFormat', $toFormat);

        return [
            $this->dateRangeConvert($dateRangeArr[0], $fromFormat, $toFormat),
            $this->dateRangeConvert($dateRangeArr[1], $fromFormat, $toFormat),
        ];
    }

    /**
     * Parse date date format  12/12/2012 - 20/12/2020
     * @param $date_range
     * @param $deliminator
     * @return array
     */
    protected function dateRangeDateParser($date_range, $deliminator)
    {
        return explode($deliminator, $date_range, 2);
    }

    /**
     * check weather we have other format for field it maybe a field or method
     * @param $field
     * @param $default
     * @return mixed
     */
    protected function dateRangeFormatChecker($field, $default)
    {
        if (method_exists($this, $field)) {
            return $this->$field();
        }

        if (property_exists($this, $field)) {
            return $this->{$field};
        }

        return $default;
    }

    /**
     * convert date from format to format
     * @param $date
     * @param $from_format
     * @param $to_format
     * @return mixed|void
     */
    protected function dateRangeConvert($date, $from_format, $to_format)
    {
        if (!$date) {
            return;
        }

        $dateObj = $this->dateRangeFormatReader($date, $from_format);
        if (!$dateObj) {
            return;
        }

        return $this->dateRangeFormatConverter($dateObj, $to_format);

    }


    /**
     * read date from Format
     * @param $date
     * @param $format
     * @return bool|\DateTime
     */
    protected function dateRangeFormatReader($date, $format)
    {
        return \DateTime::createFromFormat($format, $date);
    }

    /**
     * convert date to format
     * @param $dateObj
     * @param $format
     * @return mixed
     */
    protected function dateRangeFormatConverter($dateObj, $format)
    {
        return $dateObj->format($format);
    }


}
