<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 16:15
 */

namespace App\Traits;


trait ListTrait
{
    public static function list()
    {
        return self::query()->pluck('name', 'id');
    }
}
