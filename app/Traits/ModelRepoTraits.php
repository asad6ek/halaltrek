<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 16:16
 */

namespace App\Traits;


trait ModelRepoTraits
{
    public static function findById($id)
    {
        return self::query()->where('id', $id)->firstOrFail();
    }
}
