<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 19:10
 */

namespace App\Traits\Pond;


trait PondEncryptDecryptTrait
{
    protected function encryptPondPath($path)
    {
        return base64_encode(json_encode(['path' => $path]));
    }

    protected function decryptPondPath($path)
    {
        return json_decode(base64_decode($path), 1);
    }
}
