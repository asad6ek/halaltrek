<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 17:57
 */

namespace App\Traits\Pond;


use Illuminate\Support\Facades\Storage;

trait PondLoadImageTrait
{
    public function loadImage($id, $path)
    {
        \Debugbar::disable();
       $pathArray = $this->decryptPondPath($path);

        throw_if(!isset($pathArray['path']), 'path not found');
        return Storage::download($pathArray['path']);
    }
}
