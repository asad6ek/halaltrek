<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 17:53
 */

namespace App\Traits\Pond;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait PondProcessImageTrait
{

    protected $pondShouldUploadToPublic = true;
    protected $pondUrlPath = '';
    protected $pondInputName = 'image';

    public function processImage(Request $request)
    {
        \Debugbar::disable();
        $file = $request->file($this->pondInputName);
        if (is_array($file)) {
            $file = current($file);
        }

        $path_dir = $this->pondUrlPath;
        $path = Storage::putFile($path_dir, $file, $this->pondShouldUploadToPublic ? 'public' : null);

        return $this->encryptPondPath($path);
    }
}
