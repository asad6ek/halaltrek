<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 17:55
 */

namespace App\Traits\Pond;


use Illuminate\Http\Request;
use Storage;

trait PondRevertImageTrait
{

    protected $pondShouldDeleteOnAjax = false;

    public function revertImage(Request $request)
    {
        \Debugbar::disable();
        if (!$this->pondShouldDeleteOnAjax) {
            return 'not deleted';
        }
        $path = $request->getContent();
        $pathArray = $this->decryptPondPath($path);
        if (!isset($pathArray['id'])) {
            Storage::delete($pathArray['path']);

        }
        return 'ok';
    }
}
