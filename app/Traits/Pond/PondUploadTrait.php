<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-15
 * Time: 17:49
 */

namespace App\Traits\Pond;


trait PondUploadTrait
{
    use PondProcessImageTrait, PondRevertImageTrait, PondLoadImageTrait, PondEncryptDecryptTrait;
}
