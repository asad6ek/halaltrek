<?php

namespace App\ViewModels;

use App\DDD\Packages\Models\Package;
use App\Models\Agent;
use App\Models\Booking;
use App\Models\Review;
use Spatie\ViewModels\ViewModel;

class AdminDashboardViewModel extends ViewModel
{
    public function __construct()
    {

    }

    public function orderCount()
    {
        return Booking::query()
            ->count();
    }

    public function agentCount()
    {
        return Agent::count();
    }

    public function reviewCount()
    {
        return Review::query()->verified()->count();
    }

    public function makkahCount(){
        return Package::query()
            ->where('first_visit', '=', 'Makkah')->count();
    }

    public function madinahCount(){
        return Package::query()
            ->where('first_visit', '=', 'Madinah')->count();
    }
}
