<?php

namespace App\ViewModels;

use App\DDD\Packages\Models\Package;
use App\Models\City;
use App\Models\Hotel;
use Spatie\ViewModels\ViewModel;

class AdminPackageSaveViewModel extends PackageSaveViewModel
{

    public $agent_id;
    public function __construct($models, $agent_id, $id = null)
    {
        parent::__construct($models, $id);
        $this->agent_id = $agent_id;
    }

}
