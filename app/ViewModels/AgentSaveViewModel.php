<?php

namespace App\ViewModels;

use App\Models\City;
use Spatie\ViewModels\ViewModel;

class AgentSaveViewModel extends ViewModel
{
    public $agent;

    public function __construct($agent)
    {
        //
        $this->agent = $agent;
    }

    public function cities()
    {
        return City::list();
    }
}
