<?php

namespace App\ViewModels;

use App\Models\Booking;
use App\Models\CustomQuote;
use App\Models\Hotel;
use Spatie\ViewModels\ViewModel;

class BookingCustomViewModel extends ViewModel
{

    public function ziyarahOptions()
    {
        return CustomQuote::ziyarahOptions();
    }

    public function firstVisitOptions()
    {
        return CustomQuote::firstVisitOptions();
    }

    public function roomSizes()
    {
        return Booking::roomSizes();
    }

    public function hotelsInMakkah()
    {
        return Hotel::list();
    }

    public function hotelsInMadinah()
    {
        return Hotel::list();
    }
}
