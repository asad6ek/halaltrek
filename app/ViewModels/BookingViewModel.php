<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ViewModels\BookingViewModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ViewModels\BookingViewModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ViewModels\BookingViewModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ViewModels\BookingViewModel query()
 * @mixin \Eloquent
 */
class BookingViewModel extends Model
{
    //
}
