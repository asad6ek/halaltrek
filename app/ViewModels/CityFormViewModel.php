<?php

namespace App\ViewModels;

use App\Models\Country;
use Spatie\ViewModels\ViewModel;

class CityFormViewModel extends ViewModel
{
    public $model;

    public function __construct($model)
    {
        //
        $this->model = $model;
    }

    public function countries()
    {
        return Country::list();
    }
}
