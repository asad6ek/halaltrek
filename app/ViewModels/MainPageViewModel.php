<?php

namespace App\ViewModels;

use App\DDD\Packages\Repos\PackageRepo;
use Illuminate\Http\Request;
use Spatie\ViewModels\ViewModel;

class MainPageViewModel extends ViewModel
{
    /**
     * @var PackageRepo
     */
    private $repo;
    /**
     * @var Request
     */
    private $request;

    public function __construct(PackageRepo $repo, Request $request)
    {
        $this->repo = $repo;
        $this->request = $request;
    }

    public function hajPackages()
    {
        return $this->repo->getGetMainPackage('hajj');
    }

    public function umrahPackages()
    {
        return $this->repo->getGetMainPackage('umrah');
    }
    public function toursPackages()
    {
        return $this->repo->getGetMainPackage('tours');
    }

    public function searchForm()
    {
        return (new SearchViewModel([]))->toArray();
    }

    public function searchType()
    {
        return $this->request->input('type', 'hajj');
    }

    public function isSearchedUmrah()
    {
        return strtolower($this->searchType()) === 'umrah';
    }
    public function isSearchedTours()
    {
        return strtolower($this->searchType()) === 'tours';
    }

    public function packageTypes()
    {
        return [
            ''=>'Please choose',
            '5' => '5 star',
            'premium-economy' => 'Premium Economy',
            'economy' => 'Economy',
            ' ' => 'I don’t mind',
        ];
    }
}
