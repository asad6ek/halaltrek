<?php

namespace App\ViewModels;

use App\DDD\Packages\Models\Package;
use App\Models\Airline;
use App\Models\City;
use App\Models\Hotel;
use Spatie\ViewModels\ViewModel;

class PackageSaveViewModel extends ViewModel
{
    public $models;
    public $id;

    public function __construct($models, $id = null)
    {
        $this->models = $models;
        $this->id = $id;
    }

    public function cities()
    {
        return City::list();
    }

    public function hotelBasisList()
    {
        $items = Package::hotelBasesList();

        return $this->keyGenerator($items);
    }

    public function visitList()
    {
        $items = Package::firstVisitDesire();

        return $this->keyGenerator($items);
    }


    protected function keyGenerator($items)
    {
        $array = [];
        foreach ($items as $item) {
            $array[$item] = $item;
        }

        return $array;
    }

    public function hotels()
    {
        return Hotel::list();
    }

    public function airlines()
    {
        return Airline::list();
    }
}
