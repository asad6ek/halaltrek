<?php

namespace App\ViewModels;

use App\DDD\Packages\Models\Package;
use App\Models\Agent;
use App\Models\Airline;
use App\Models\Hotel;
use Spatie\ViewModels\ViewModel;

class SearchViewModel extends ViewModel
{
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function requestData()
    {
        return $this->request;
    }

    public function models()
    {
        return Package::query()->filter($this->request)->paginate();
    }

    public function hotel()
    {
        $id = data_get($this->request, 'hotel');
        if (!$id) {
            return null;
        }

        return Hotel::findOrFail($id);
    }

    public function type()
    {
        return data_get($this->request, 'type');
    }

    public function airline()
    {
        $id = data_get($this->request, 'airline');
        if (!$id) {
            return null;
        }

        return Airline::findOrFail($id);
    }

    public function agent()
    {
        $id = data_get($this->request, 'agent');
        if (!$id) {
            return null;
        }

        return Agent::findOrFail($id);
    }

    public function daterange()
    {
        return data_get($this->request, 'daterange');
    }

}
