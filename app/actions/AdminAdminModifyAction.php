<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:24
 */

namespace App\actions;


use App\Models\Admin;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdminAdminModifyAction
{
    /**
     * @var null
     */
    private $id;
    protected $admin;
    private $data;

    public function __construct($data, $id = null)
    {

        $this->data = $data;
        $this->id = $id;
        if ($this->id) {
            $this->getModel();
        } else {
            $this->admin = new Admin();
        }
    }

    public function execute()
    {
        $agent = $this->getModel();
        $agent->fill($this->data);
        $this->changePassword($this->data['password'] ?? '');
        $agent->save();
        return $agent;
    }

    /**
     * @throws ModelNotFoundException
     * @return  Admin
     */
    protected function getModel()
    {
        if (!$this->admin) {
            $this->admin = Admin::query()
                ->findOrFail($this->id);
        }

        return $this->admin;
    }

    protected function changePassword($password)
    {
        if (trim($password)) {
            $this->getModel()->password = bcrypt($password);
        }
    }


}
