<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:24
 */

namespace App\actions;


use App\Models\Agent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AdminAgentModifyAction
{
    /**
     * @var null
     */
    private $id;
    protected $agent;
    private $data;

    public function __construct($data, $id = null)
    {

        $this->data = $data;
        $this->id = $id;
        if ($this->id) {
            $this->getModel();
        } else {
            $this->agent = new Agent();
        }
    }

    public function execute()
    {
        $agent = $this->getModel();
        $agent->fill($this->data);
        $agent->info = data_get($this->data, 'info', '');
        $agent->ministry_approved = data_get($this->data, 'ministry_approved', 0);
        $agent->approved = data_get($this->data, 'approved', 0);
        $this->changePassword($this->data['password'] ?? '');
        $agent->save();
        return $agent;
    }

    /**
     * @throws ModelNotFoundException
     * @return  Agent
     */
    protected function getModel()
    {
        if (!$this->agent) {
            $this->agent = Agent::query()
                ->findOrFail($this->id);
        }

        return $this->agent;
    }

    protected function changePassword($password)
    {
        if (trim($password)) {
            $this->getModel()->password = bcrypt($password);
        }
    }


}
