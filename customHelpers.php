<?php

use Illuminate\Support\Facades\Route;

/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 3/11/18
 * Time: 9:55 PM
 */

function isActiveRoute($route, $strict = false, $className = 'active')
{
    $name = Route::currentRouteName();
    if (!$strict) {
        return \Illuminate\Support\Str::is($route . '*', $name) ? $className : '';
    }
    return $name === $route ? $className : '';
}

function alertMessage($type, $message)
{
    session()->flash('alert-message', $message);
    session()->flash('alert-type', $type);
}

function modalMessage($type, $message)
{
    session()->flash('modal-message', $message);
    session()->flash('modal-type', $type);
}

function resourceWithUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');
    Route::resource($url, $controller)->names([
        'index' => $url . '.index',
        'create' => $url . '.create',
        'store' => $url . '.store',
        'edit' => $url . '.edit',
        'update' => $url . '.update',
        'destroy' => $url . '.delete',
    ]);
}

function apiUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::get($url . '/load/{path}', $controller . '@loadImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');

}

function resource($url, $controller)
{
    Route::resource($url, $controller)->names([
        'index' => $url . '.index',
        'create' => $url . '.create',
        'store' => $url . '.store',
        'edit' => $url . '.edit',
        'update' => $url . '.update',
        'destroy' => $url . '.delete',
        'show' => $url . '.show',
    ]);
}


