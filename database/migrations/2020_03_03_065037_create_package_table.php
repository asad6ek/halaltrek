<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('ziyarat_type', ['hajj', 'umrah']);
            $table->string('name');
            $table->date('date_from');
            $table->date('date_to');
            $table->tinyInteger('num_days');
            $table->integer('city_id');
            $table->integer('country_id')->nullable();
            $table->integer('agent_id');
            $table->enum('hotel_basis',['Room','B&B','FullBoard']);
            $table->enum('first_visit',['Makkah','Madinah']);
            $table->boolean('ziyorah_included');
            $table->boolean('transport_support');
            $table->boolean('is_shifting')->nullable();
            $table->tinyInteger('num_days_aziziya')->nullable();
            $table->timestamps();

            $table->index('city_id');
            $table->index('ziyarat_type');
            $table->index('country_id');
            $table->index('agent_id');
            $table->index('date_from');
            $table->index('date_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
