<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageAirlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_airlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('package_id')->index();
            $table->string('from');
            $table->string('to');
            $table->bigInteger('airline_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_airlines');
    }
}
