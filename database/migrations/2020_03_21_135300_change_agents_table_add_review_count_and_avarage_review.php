<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAgentsTableAddReviewCountAndAvarageReview extends Migration
{
    const TABLE = 'agents';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->text('info')->nullable('');
            $table->integer('count_review')->default(0);
            $table->float('average_review')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE, function (Blueprint $table) {
            $table->dropColumn('info');
            $table->dropColumn('count_review');
            $table->dropColumn('average_review');
        });
    }
}
