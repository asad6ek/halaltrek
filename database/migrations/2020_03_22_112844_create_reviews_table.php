<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('reviewable_id')->index();
            $table->string('reviewable_type')->index();
            $table->string('name');
            $table->string('email');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_mail_verified')->default(0);
            $table->string('comment', 1000);
            $table->tinyInteger('rate');
            $table->timestamps();

            $table->index(['reviewable_id', 'reviewable_type']);
            $table->index(['email']);
            $table->index(['status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
