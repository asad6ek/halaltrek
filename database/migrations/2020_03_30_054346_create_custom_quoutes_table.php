<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomQuoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('nights_makkah');
            $table->integer('nights_madinah');
            $table->integer('hotel_makkah')->index();
            $table->integer('hotel_madinah')->index();
            $table->date('depart');
            $table->date('return');
            $table->enum('first_visit', ['Makkah', 'Madinah']);
            $table->tinyInteger('transfers');
            $table->enum('ziyorah', ['Madinah', 'Makkah', 'both']);
            $table->string('comment', 1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_quotes');
    }
}
