<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeHotelBasesAndZiyarateTypeAndTransportSupportToPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('packages', function (Blueprint $table) {
//            $table->enum('hotel_basis',['Room','B&B','FullBoard','Half board'])->change();
            $table->dropColumn('transport_support');
        });
        Schema::table('packages', function (Blueprint $table) {
//            $table->enum('hotel_basis',['Room','B&B','FullBoard','Half board'])->change();
            $table->boolean('transport_support')->nullable()->default(0);
        });

        DB::statement('

ALTER TABLE
    `packages`
MODIFY COLUMN
    `hotel_basis` enum(
        \'Room\',
        \'B&B\',
        \'FullBoard\',
        \'Half board\'
    )
NOT NULL AFTER `transport_support`;

');




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
//            $table->enum('hotel_basis',['Room','B&B','FullBoard'])->change();
            $table->dropColumn('transport_support');
        });

        Schema::table('packages', function (Blueprint $table) {
//            $table->enum('hotel_basis',['Room','B&B','FullBoard'])->change();
            $table->boolean('transport_support');
        });


        DB::statement('

ALTER TABLE
    `packages`
MODIFY COLUMN
    `hotel_basis` enum(
        \'Room\',
        \'B&B\',
        \'FullBoard\'
    )
NOT NULL AFTER `transport_support`;

');

    }
}
