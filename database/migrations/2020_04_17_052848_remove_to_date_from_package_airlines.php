<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveToDateFromPackageAirlines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_airlines', function (Blueprint $table) {
            $table->dropColumn('to_date');
            $table->enum('type', ['Direct', 'Indirect'])->nullable();
            $table->string('description', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_airlines', function (Blueprint $table) {
            $table->date('to_date')->nullable();
            $table->dropColumn('type');
            $table->dropColumn('description');
        });
    }
}
