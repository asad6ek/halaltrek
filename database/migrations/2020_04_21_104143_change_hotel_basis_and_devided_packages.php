<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeHotelBasisAndDevidedPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->enum('hotel_basis_madinah', ['Room', 'B&B', 'FullBoard','Half board'])->nullable();
            $table->enum('hotel_basis_makkah', ['Room', 'B&B', 'FullBoard','Half board'])->nullable();
            $table->dropColumn('hotel_basis');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('hotel_basis_madinah');
            $table->dropColumn('hotel_basis_makkah');
            $table->enum('hotel_basis', ['Room', 'B&B', 'FullBoard','Half board'])->nullable();
        });
    }
}
