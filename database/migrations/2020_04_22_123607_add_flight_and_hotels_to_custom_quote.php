<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlightAndHotelsToCustomQuote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('custom_quotes', function (Blueprint $table) {
            $table->dropColumn('first_visit');
            $table->dropColumn('ziyorah');
            $table->dropColumn('hotel_makkah');
            $table->dropColumn('hotel_madinah');
            $table->json('flights')->nullable();
            $table->json('hotels')->nullable();
            $table->string('comment')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('custom_quotes', function (Blueprint $table) {
            $table->dropColumn('flights');
            $table->dropColumn('hotels');
            $table->enum('first_visit', ['Makkah', 'Madinah']);
            $table->enum('ziyorah', ['Madinah', 'Makkah', 'both']);
            $table->integer('hotel_makkah')->index();
            $table->integer('hotel_madinah')->index();
            $table->string('comment')->change();
        });
    }
}
