<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobileTownWebsiteToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->string('mobile')->nullable();
            $table->string('website')->nullable();
            $table->string('town')->nullable();
            $table->boolean('registered_with_ministry')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents', function (Blueprint $table) {
            $table->dropColumn('mobile');
            $table->dropColumn('website');
            $table->dropColumn('town');
            $table->dropColumn('registered_with_ministry');
        });
    }
}
