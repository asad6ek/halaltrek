<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPackageClassToPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            DB::statement("ALTER TABLE packages MODIFY COLUMN ziyarat_type ENUM('hajj', 'umrah', 'tours')");
            $table->enum('package_class', ['Premium', 'Economy', 'Budget']);
            $table->string('description',1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->enum('ziyarat_type', ['hajj', 'umrah','tours']);
            $table->string('description',1000)->nullable();

        });
    }
}
