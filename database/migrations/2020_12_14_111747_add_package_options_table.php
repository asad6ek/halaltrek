<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPackageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_options', function (Blueprint $table) {
            $table->boolean('breakfast')->default(0);
            $table->boolean('ground_transport')->default(0);
            $table->boolean('hadiy')->default(0);
            $table->boolean('visit')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_options', function (Blueprint $table) {
            $table->dropColumn('breakfast');
            $table->dropColumn('ground_transport');
            $table->dropColumn('hadiy');
            $table->dropColumn('visit');
        });
    }
}
