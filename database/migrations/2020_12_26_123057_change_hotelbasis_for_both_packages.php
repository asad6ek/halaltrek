<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeHotelbasisForBothPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE packages MODIFY COLUMN hotel_basis_madinah ENUM('Room', 'B&B', 'FullBoard', 'Half board', 'To be confirmed')");
        DB::statement("ALTER TABLE packages MODIFY COLUMN hotel_basis_makkah ENUM('Room', 'B&B', 'FullBoard', 'Half board', 'To be confirmed')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE packages MODIFY COLUMN hotel_basis_madinah ENUM('Room', 'B&B', 'FullBoard', 'Half board')");
        DB::statement("ALTER TABLE packages MODIFY COLUMN hotel_basis_makkah ENUM('Room', 'B&B', 'FullBoard', 'Half board')");
    }
}
