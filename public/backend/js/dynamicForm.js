(function () {


    window.DynamicForm = function () {
        this.closeButton = null;
        this.template = null;
        this.addButton = null;


        var defaults = {
            count_block: 0,
            parent_block: ".rooms",
            add_button: '.addposition',
            copy_block: '.template',
            del_button: '.delblock',
            block: '.room',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: true,
            delConfirm: true,
            max: 999,
            min: 1,
            onAdd: function (index, item) {

            }
        };

        if (arguments[0] && typeof arguments[0] === "object") {
            this.options = extendDefaults(defaults, arguments[0]);
        }

    };

    window.DynamicForm.prototype.add = function (element) {
        console.info('add');
        if ($(this.options.parent_block).children().length >= this.options.max) {
            return null;
        }

        var template = $(this.options.copy_block).html();
        this.options.template = template;

        var replaced_template = template.replace(new RegExp(this.options.replaceItem, 'ig'), this.options.count_block);
        $(this.options.parent_block).append(replaced_template);
        this.options.count_block++;
        if (this.options.removeCopyButton) {
            console.info(this.options.removeCopyButton, 'sadf');
            $(element).fadeOut(500);
        }
        this.options.onAdd(this.options.count_block -1, replaced_template);
        return false;
    };


    window.DynamicForm.prototype.del = function (element) {
        var children = $(this.options.parent_block).children().length;
        if (children - 1 < this.options.min) {
            return false;
        }

        $(element).parents(this.options.block).remove();
        $(element).fadeOut(500);
            //count_block--;


    };


    function extendDefaults(defaults, arr) {
        for (var index in arr) {
            defaults[index] = arr[index];
        }
        return defaults;
    }

    window.DynamicForm.prototype.init = function () {
        var minThis = this;
        $(document).on('click', this.options.add_button, function (e) {
            e.preventDefault();
            minThis.add(this);
        });

        $(document).on('click', this.options.del_button, function (e) {
            e.preventDefault();
            minThis.del(this);
        });
    };


}());

