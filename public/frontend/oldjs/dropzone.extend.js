"use strict";

(function (window) {
    var DropzoneExtend = function (element, options = {}) {


        if (!options.hasOwnProperty('newInputName')) {
            console.info("new input name has not been specified");
            options.newInputName = 'file';
        }


        options.addRemoveLinks = true;
        options.transformFile = transformFile;

        this.dropzone = new Dropzone(element, options);

        this.init();

    };


    DropzoneExtend.prototype.init = function () {
        this.registerSuccess();
        this.registerRemoveFileEvent();

    };


    DropzoneExtend.prototype.registerSuccess = function () {
        var mainThis = this;

        this.dropzone.on('success', function (file, res) {

            var id = domHelperFuctions.getFileId(file);

            var input = domHelperFuctions.createInput({
                name: mainThis.dropzone.options.newInputName,
                id,
                value: res,
                isMultiple: mainThis.dropzone.options.maxFiles === null
                    || mainThis.dropzone.options.maxFiles > 1
            });


            domHelperFuctions.addElementToTemplate({
                el: mainThis.dropzone.element,
                input
            });

        });
    };


    DropzoneExtend.prototype.registerRemoveFileEvent = function () {
        this.dropzone.on('removedfile', function (file) {
            var id = domHelperFuctions.getFileId(file);
            var element = document.getElementById(id);
            if (element) {
                element.remove();
            }
        });
    };


    var domHelperFuctions = {

        addElementToTemplate: function ({el: dropzoneElement, input: InputToInsert}) {
            dropzoneElement.appendChild(InputToInsert);
        },

        createInput: function ({name, id, value, isMultiple = true}) {

            var input = document.createElement('input');

            if (isMultiple) {
                name += '[]';
            }

            input.type = 'hidden';
            input.name = name;
            input.id = id;

            input.value = value;

            return input;
        },

        getFileId: function (file) {
            return file.upload.uuid;
        }

    };

    var transformFile = function (file, done) {

        var myDropZone = this;

        // Create the image editor overlay
        var editor = document.createElement('div');
        editor.style.position = 'fixed';
        editor.style.left = 0;
        editor.style.right = 0;
        editor.style.top = 0;
        editor.style.bottom = 0;
        editor.style.zIndex = 9999;
        editor.style.backgroundColor = '#000';

        // Create the confirm button
        var confirm = document.createElement('button');
        confirm.style.position = 'absolute';
        confirm.style.left = '10px';
        confirm.style.top = '10px';
        confirm.style.zIndex = 9999;
        confirm.textContent = 'Confirm';
        confirm.addEventListener('click', function () {

            // Get the canvas with image data from Cropper.js
            var canvas = cropper.getCroppedCanvas({
                width: myDropZone.options.imageResizeTargetWidth || 300,
                height: myDropZone.options.imageResizeTargetHeight || 300
            });

            // Turn the canvas into a Blob (file object without a name)
            canvas.toBlob(function (blob) {

                // Update the image thumbnail with the new image data
                myDropZone.createThumbnail(
                    blob,
                    myDropZone.options.thumbnailWidth,
                    myDropZone.options.thumbnailHeight,
                    myDropZone.options.thumbnailMethod,
                    false,
                    function (dataURL) {

                        // Update the Dropzone file thumbnail
                        myDropZone.emit('thumbnail', file, dataURL);

                        // Return modified file to dropzone
                        done(blob);
                    }
                );

            });

            // Remove the editor from view
            editor.parentNode.removeChild(editor);

        });
        editor.appendChild(confirm);

        // Load the image
        var image = new Image();
        image.src = URL.createObjectURL(file);
        editor.appendChild(image);

        // Append the editor to the page
        document.body.appendChild(editor);

        // Create Cropper.js and pass image
        var cropper = new Cropper(image, {
            aspectRatio: 1
        });

    };



    DropzoneExtend.prototype.oneImageMode = function () {
       $('')

    };


    var templateForOneImage = `
        <div class="dz-preview dz-file-preview">
  <div class="dz-details">
    <div class="dz-filename"><span data-dz-name></span></div>
    <div class="dz-size" data-dz-size></div>
    <img data-dz-thumbnail />
  </div>
  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
  <div class="dz-success-mark"><span>✔</span></div>
  <div class="dz-error-mark"><span>✘</span></div>
  <div class="dz-error-message"><span data-dz-errormessage></span></div>
</div>
    `;


    window.DropzoneExtend = DropzoneExtend;


})(window);









