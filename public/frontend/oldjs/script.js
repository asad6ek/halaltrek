(function ($) {


    $('input[name="daterange"]').daterangepicker({
        opens: 'center',
        autoUpdateInput: false,
        locale: {
            format: 'DD-MM-YYYY'
        }
    });

    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    });

    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });


    $(".js-states").select2({
        allowClear: true
    });

    $(".js-states").each(function (index, item) {
        var value = $(item).attr('data-selectedValue');
        var text = $(item).attr('data-selectedText');
        if (!text || !value) {
            return;
        }
// Create a DOM Option and pre-select by default
        var newOption = new Option(text, value, true, true);
        // Append it to the select
        $(item).append(newOption).trigger('change');
    })

})($);


(function () {
    var materialInput = function (params = {}) {

        var defaults = {
            input: '.form-control',
            inputFilled: 'input-filled',
            inputError: 'input-error',
            inputDirty: 'input-dirty',
            inputActive: 'input-active',
            inputBlur: 'input-blur',
            keyUp: false,
        };

        if (typeof params === 'string') {
            defaults.input = params || defaults.input;
        } else {
            defaults = extendDefaults(defaults, params);
        }

        this.defaults = defaults;

        // this.init();
    };


    materialInput.prototype.listen = function (element) {

        element.onblur = () => {
            this.setClass(element, false)
        };

        element.onfocus = () => {
            this.setClass(element, true)
        };

        if (this.defaults.keyUp) {
            element.keyup = () => {
                this.setClass(element, true)
            }
        }

        this.setClass(element, false);
    };

    materialInput.prototype.setClass = function (el, active) {
        var defaults = this.defaults;

        if (active) {
            el.classList.add(defaults.inputActive);
            el.classList.remove(defaults.inputBlur);
        } else {
            el.classList.remove(defaults.inputActive);
            el.value === '' ?
                el.classList.remove(defaults.inputFilled) :
                el.classList.add(defaults.inputFilled);

            el.classList.add(defaults.inputBlur);
        }

        // error
        if (!el.validity.valid) {
            el.classList.add(defaults.inputError);
        } else {
            el.classList.remove(defaults.inputError);
        }

        // dirty
        if (active && [].indexOf.call(el.classList) === -1) {
            el.classList.add(defaults.inputDirty);
        }
    };

    materialInput.prototype.init = function () {
        [].forEach.call(
            document.querySelectorAll(this.defaults.input),
            (el) => {
                this.listen(el);
            });
    };


    function extendDefaults(defaults, arr) {
        for (var index in arr) {
            defaults[index] = arr[index];
        }
        return defaults;
    }

    window.materialInput = materialInput;

})(window);

