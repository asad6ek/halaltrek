-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 03 2020 г., 18:30
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `striksi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-12-01 11:55:26', '2020-12-01 11:55:26', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://striksi.loc', 'yes'),
(2, 'home', 'http://striksi.loc', 'yes'),
(3, 'blogname', 'striksi', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'zafar.abdullaev.00@bk.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:136:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:46:\"gorod/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?gorod=$matches[1]&feed=$matches[2]\";s:41:\"gorod/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?gorod=$matches[1]&feed=$matches[2]\";s:22:\"gorod/([^/]+)/embed/?$\";s:38:\"index.php?gorod=$matches[1]&embed=true\";s:34:\"gorod/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?gorod=$matches[1]&paged=$matches[2]\";s:16:\"gorod/([^/]+)/?$\";s:27:\"index.php?gorod=$matches[1]\";s:48:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:58:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:78:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:73:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:54:\"yaponskie_tehnologii/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"yaponskie_tehnologii/([^/]+)/embed/?$\";s:53:\"index.php?yaponskie_tehnologii=$matches[1]&embed=true\";s:41:\"yaponskie_tehnologii/([^/]+)/trackback/?$\";s:47:\"index.php?yaponskie_tehnologii=$matches[1]&tb=1\";s:49:\"yaponskie_tehnologii/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?yaponskie_tehnologii=$matches[1]&paged=$matches[2]\";s:56:\"yaponskie_tehnologii/([^/]+)/comment-page-([0-9]{1,})/?$\";s:60:\"index.php?yaponskie_tehnologii=$matches[1]&cpage=$matches[2]\";s:45:\"yaponskie_tehnologii/([^/]+)(?:/([0-9]+))?/?$\";s:59:\"index.php?yaponskie_tehnologii=$matches[1]&page=$matches[2]\";s:37:\"yaponskie_tehnologii/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"yaponskie_tehnologii/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"yaponskie_tehnologii/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"yaponskie_tehnologii/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"yaponskie_tehnologii/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"yaponskie_tehnologii/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"ulitsa/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"ulitsa/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"ulitsa/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"ulitsa/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"ulitsa/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"ulitsa/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"ulitsa/([^/]+)/embed/?$\";s:39:\"index.php?ulitsa=$matches[1]&embed=true\";s:27:\"ulitsa/([^/]+)/trackback/?$\";s:33:\"index.php?ulitsa=$matches[1]&tb=1\";s:35:\"ulitsa/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?ulitsa=$matches[1]&paged=$matches[2]\";s:42:\"ulitsa/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?ulitsa=$matches[1]&cpage=$matches[2]\";s:31:\"ulitsa/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?ulitsa=$matches[1]&page=$matches[2]\";s:23:\"ulitsa/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"ulitsa/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"ulitsa/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"ulitsa/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"ulitsa/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"ulitsa/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=12&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:3;s:16:\"yamaps/yamap.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:80:\"D:\\installs\\OSPanel\\domains\\striksi.loc/wp-content/themes/twentytwenty/style.css\";i:1;s:0:\"\";}', 'no'),
(40, 'template', 'striksi', 'yes'),
(41, 'stylesheet', 'striksi', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '48748', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:3:{i:2;a:4:{s:5:\"title\";s:13:\"О сайте\";s:4:\"text\";s:205:\"Здесь может быть отличное место для того, чтобы представить себя, свой сайт или выразить какие-то благодарности.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:21:\"Найдите нас\";s:4:\"text\";s:226:\"<strong>Адрес</strong>\n123 Мейн стрит\nНью Йорк, NY 10001\n\n<strong>Часы</strong>\nПонедельник&mdash;пятница: 9:00&ndash;17:00\nСуббота и воскресенье: 11:00&ndash;15:00\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '12', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1622375717', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'initial_db_version', '48748', 'yes'),
(96, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(97, 'fresh_site', '0', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:7:{i:1607010931;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1607039731;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1607082929;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1607082953;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1607082962;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1607514929;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'theme_mods_twentytwenty', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1606825268;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(116, 'recovery_keys', 'a:0:{}', 'yes'),
(125, '_site_transient_timeout_browser_8d3fec2581d3961f3037851d5cc0039c', '1607428560', 'no'),
(126, '_site_transient_browser_8d3fec2581d3961f3037851d5cc0039c', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"86.0.4240.198\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(127, '_site_transient_timeout_php_check_a5b4d2808570efd012607394df5c6fa9', '1607428561', 'no'),
(128, '_site_transient_php_check_a5b4d2808570efd012607394df5c6fa9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(129, 'can_compress_scripts', '0', 'no'),
(134, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:24:\"zafar.abdullaev.00@bk.ru\";s:7:\"version\";s:5:\"5.5.3\";s:9:\"timestamp\";i:1606823790;}', 'no'),
(137, 'finished_updating_comment_type', '1', 'yes'),
(140, 'WPLANG', 'ru_RU', 'yes'),
(141, 'new_admin_email', 'zafar.abdullaev.00@bk.ru', 'yes'),
(149, 'current_theme', 'striksi', 'yes'),
(150, 'theme_mods_strikisi', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1606825242;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:8:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:8:\"search-2\";i:3;s:14:\"recent-posts-2\";i:4;s:17:\"recent-comments-2\";i:5;s:10:\"archives-2\";i:6;s:12:\"categories-2\";i:7;s:6:\"meta-2\";}}}}', 'yes'),
(151, 'theme_switched', '', 'yes'),
(154, 'theme_mods_striksi', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";i:3;s:6:\"menu-2\";i:12;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(155, 'category_children', 'a:0:{}', 'yes'),
(177, 'recently_activated', 'a:0:{}', 'yes'),
(178, 'acf_version', '5.8.0', 'yes'),
(183, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(193, '_transient_health-check-site-status-result', '{\"good\":11,\"recommended\":9,\"critical\":0}', 'yes'),
(195, '_transient_timeout_acf_plugin_updates', '1607083898', 'no'),
(196, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";s:11:\"new_version\";s:5:\"5.9.3\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.5.3\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";s:5:\"5.8.0\";}}', 'no'),
(204, 'cptui_new_install', 'false', 'yes'),
(205, 'cptui_post_types', 'a:2:{s:20:\"yaponskie_tehnologii\";a:30:{s:4:\"name\";s:20:\"yaponskie_tehnologii\";s:5:\"label\";s:37:\"Японские технологии\";s:14:\"singular_label\";s:37:\"Японские технологии\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:29:{s:9:\"all_items\";s:44:\"Все Японские технологии\";s:9:\"menu_name\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";s:14:\"name_admin_bar\";s:0:\"\";s:14:\"item_published\";s:0:\"\";s:24:\"item_published_privately\";s:0:\"\";s:22:\"item_reverted_to_draft\";s:0:\"\";s:14:\"item_scheduled\";s:0:\"\";s:12:\"item_updated\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:6:\"ulitsa\";a:30:{s:4:\"name\";s:6:\"ulitsa\";s:5:\"label\";s:10:\"Улицы\";s:14:\"singular_label\";s:10:\"Улицы\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:16:\"delete_with_user\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:29:{s:9:\"all_items\";s:17:\"Все Улицы\";s:9:\"menu_name\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";s:14:\"name_admin_bar\";s:0:\"\";s:14:\"item_published\";s:0:\"\";s:24:\"item_published_privately\";s:0:\"\";s:22:\"item_reverted_to_draft\";s:0:\"\";s:14:\"item_scheduled\";s:0:\"\";s:12:\"item_updated\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(222, 'cptui_taxonomies', 'a:1:{s:5:\"gorod\";a:25:{s:4:\"name\";s:5:\"gorod\";s:5:\"label\";s:10:\"Город\";s:14:\"singular_label\";s:10:\"Город\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"show_ui\";s:4:\"true\";s:12:\"show_in_menu\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:1:\"1\";s:20:\"rewrite_hierarchical\";s:1:\"0\";s:17:\"show_admin_column\";s:5:\"false\";s:12:\"show_in_rest\";s:4:\"true\";s:18:\"show_in_quick_edit\";s:0:\"\";s:9:\"rest_base\";s:0:\"\";s:21:\"rest_controller_class\";s:0:\"\";s:6:\"labels\";a:18:{s:9:\"all_items\";s:19:\"Все Города\";s:9:\"menu_name\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:11:\"update_item\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:13:\"new_item_name\";s:0:\"\";s:11:\"parent_item\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:13:\"popular_items\";s:0:\"\";s:26:\"separate_items_with_commas\";s:0:\"\";s:19:\"add_or_remove_items\";s:0:\"\";s:21:\"choose_from_most_used\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:8:\"no_terms\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";}s:11:\"meta_box_cb\";s:0:\"\";s:12:\"default_term\";s:0:\"\";s:12:\"object_types\";a:1:{i:0;s:6:\"ulitsa\";}}}', 'yes'),
(226, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1606996840', 'no'),
(227, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4730;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:4540;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2697;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2570;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1984;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1838;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1813;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1498;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1495;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1494;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1468;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1460;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1452;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1306;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1230;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1211;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1183;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1139;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1114;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:1028;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:917;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:907;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:886;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:883;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:838;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:807;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:797;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:788;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:786;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:756;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:756;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:727;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:723;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:710;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:707;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:692;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:668;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:663;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:659;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:653;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:641;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:638;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:630;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:619;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:593;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:587;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:585;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:584;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:581;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:563;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:562;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:559;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:555;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:554;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:553;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:545;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:537;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:534;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:526;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:524;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:524;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:507;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:498;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:496;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:488;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:485;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:483;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:469;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:462;}s:5:\"block\";a:3:{s:4:\"name\";s:5:\"block\";s:4:\"slug\";s:5:\"block\";s:5:\"count\";i:453;}s:9:\"elementor\";a:3:{s:4:\"name\";s:9:\"elementor\";s:4:\"slug\";s:9:\"elementor\";s:5:\"count\";i:447;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:446;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:444;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:440;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:440;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:434;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:434;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:428;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:428;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:426;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:424;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:414;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:405;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:403;}s:8:\"shipping\";a:3:{s:4:\"name\";s:8:\"shipping\";s:4:\"slug\";s:8:\"shipping\";s:5:\"count\";i:399;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:398;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:397;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:392;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:388;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:383;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:382;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:378;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:376;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:366;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:362;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:359;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:353;}s:6:\"import\";a:3:{s:4:\"name\";s:6:\"import\";s:4:\"slug\";s:6:\"import\";s:5:\"count\";i:348;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:347;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:343;}}', 'no'),
(234, 'yamaps_options', 'a:13:{s:17:\"center_map_option\";s:15:\"55.7473,37.6247\";s:15:\"zoom_map_option\";s:2:\"12\";s:15:\"type_map_option\";s:10:\"yandex#map\";s:17:\"height_map_option\";s:5:\"22rem\";s:19:\"controls_map_option\";s:0:\"\";s:20:\"wheelzoom_map_option\";s:3:\"off\";s:21:\"mobiledrag_map_option\";s:3:\"off\";s:16:\"type_icon_option\";s:15:\"islands#dotIcon\";s:17:\"color_icon_option\";s:7:\"#1e98ff\";s:21:\"authorlink_map_option\";s:3:\"off\";s:15:\"open_map_option\";s:3:\"off\";s:17:\"apikey_map_option\";s:0:\"\";s:17:\"reset_maps_option\";s:3:\"off\";}', 'yes'),
(236, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.5.3.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.5.3.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.5.3\";s:7:\"version\";s:5:\"5.5.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1606996569;s:15:\"version_checked\";s:5:\"5.5.3\";s:12:\"translations\";a:0:{}}', 'no'),
(238, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1606996571;s:7:\"checked\";a:4:{s:7:\"striksi\";s:5:\"1.0.0\";s:14:\"twentynineteen\";s:3:\"1.7\";s:15:\"twentyseventeen\";s:3:\"2.4\";s:12:\"twentytwenty\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.7.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.4\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.4.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.5.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(260, '_site_transient_timeout_theme_roots', '1606998370', 'no'),
(261, '_site_transient_theme_roots', 'a:4:{s:7:\"striksi\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(262, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1606996572;s:7:\"checked\";a:6:{s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";s:5:\"5.8.0\";s:19:\"akismet/akismet.php\";s:5:\"4.1.6\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.6\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.8.1\";s:9:\"hello.php\";s:5:\"1.7.2\";s:16:\"yamaps/yamap.php\";s:6:\"0.6.20\";}s:8:\"response\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.7\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.5.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:40:\"advanced-custom-fields-pro-5.8.0/acf.php\";s:11:\"new_version\";s:5:\"5.9.3\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"5.5.3\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:16:\"yamaps/yamap.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:20:\"w.org/plugins/yamaps\";s:4:\"slug\";s:6:\"yamaps\";s:6:\"plugin\";s:16:\"yamaps/yamap.php\";s:11:\"new_version\";s:6:\"0.6.20\";s:3:\"url\";s:37:\"https://wordpress.org/plugins/yamaps/\";s:7:\"package\";s:49:\"https://downloads.wordpress.org/plugin/yamaps.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:59:\"https://ps.w.org/yamaps/assets/icon-256x256.png?rev=1821014\";s:2:\"1x\";s:51:\"https://ps.w.org/yamaps/assets/icon.svg?rev=1821014\";s:3:\"svg\";s:51:\"https://ps.w.org/yamaps/assets/icon.svg?rev=1821014\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/yamaps/assets/banner-1544x500.jpg?rev=1821014\";s:2:\"1x\";s:61:\"https://ps.w.org/yamaps/assets/banner-772x250.jpg?rev=1821014\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2020/12/2020-landscape-1.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:769;s:4:\"file\";s:28:\"2020/12/2020-landscape-1.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"2020-landscape-1-300x192.png\";s:5:\"width\";i:300;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"2020-landscape-1-1024x656.png\";s:5:\"width\";i:1024;s:6:\"height\";i:656;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"2020-landscape-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"2020-landscape-1-768x492.png\";s:5:\"width\";i:768;s:6:\"height\";i:492;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(5, 5, '_starter_content_theme', 'twentytwenty'),
(6, 5, '_customize_draft_post_name', '%d0%be%d0%b1%d0%bd%d0%be%d0%b2%d0%bb%d1%91%d0%bd%d0%bd%d0%b0%d1%8f-umoma-%d0%be%d1%82%d0%ba%d1%80%d1%8b%d0%b2%d0%b0%d0%b5%d1%82-%d0%b4%d0%b2%d0%b5%d1%80%d0%b8'),
(7, 6, '_thumbnail_id', '5'),
(8, 6, '_customize_draft_post_name', 'umoma-%d0%be%d1%82%d0%ba%d1%80%d1%8b%d0%b2%d0%b0%d0%b5%d1%82-%d0%b4%d0%b2%d0%b5%d1%80%d0%b8'),
(9, 6, '_customize_changeset_uuid', '079ef1f0-48a8-4e83-90a5-372690f7763e'),
(10, 7, '_customize_draft_post_name', '%d0%be-%d0%bd%d0%b0%d1%81'),
(11, 7, '_customize_changeset_uuid', '079ef1f0-48a8-4e83-90a5-372690f7763e'),
(12, 8, '_customize_draft_post_name', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d1%8b'),
(13, 8, '_customize_changeset_uuid', '079ef1f0-48a8-4e83-90a5-372690f7763e'),
(14, 9, '_customize_draft_post_name', '%d0%b1%d0%bb%d0%be%d0%b3'),
(15, 9, '_customize_changeset_uuid', '079ef1f0-48a8-4e83-90a5-372690f7763e'),
(18, 12, '_edit_lock', '1606905843:1'),
(19, 3, '_wp_trash_meta_status', 'draft'),
(20, 3, '_wp_trash_meta_time', '1606898026'),
(21, 3, '_wp_desired_post_slug', 'privacy-policy'),
(22, 2, '_wp_trash_meta_status', 'publish'),
(23, 2, '_wp_trash_meta_time', '1606898034'),
(24, 2, '_wp_desired_post_slug', 'sample-page'),
(25, 16, '_edit_lock', '1606991719:1'),
(26, 18, '_edit_lock', '1606991911:1'),
(27, 20, '_menu_item_type', 'post_type'),
(28, 20, '_menu_item_menu_item_parent', '0'),
(29, 20, '_menu_item_object_id', '12'),
(30, 20, '_menu_item_object', 'page'),
(31, 20, '_menu_item_target', ''),
(32, 20, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(33, 20, '_menu_item_xfn', ''),
(34, 20, '_menu_item_url', ''),
(35, 20, '_menu_item_orphaned', '1606898186'),
(54, 23, '_menu_item_type', 'post_type'),
(55, 23, '_menu_item_menu_item_parent', '0'),
(56, 23, '_menu_item_object_id', '12'),
(57, 23, '_menu_item_object', 'page'),
(58, 23, '_menu_item_target', ''),
(59, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(60, 23, '_menu_item_xfn', ''),
(61, 23, '_menu_item_url', ''),
(62, 23, '_menu_item_orphaned', '1606898494'),
(63, 24, '_menu_item_type', 'post_type'),
(64, 24, '_menu_item_menu_item_parent', '0'),
(65, 24, '_menu_item_object_id', '18'),
(66, 24, '_menu_item_object', 'page'),
(67, 24, '_menu_item_target', ''),
(68, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(69, 24, '_menu_item_xfn', ''),
(70, 24, '_menu_item_url', ''),
(72, 25, '_menu_item_type', 'post_type'),
(73, 25, '_menu_item_menu_item_parent', '0'),
(74, 25, '_menu_item_object_id', '16'),
(75, 25, '_menu_item_object', 'page'),
(76, 25, '_menu_item_target', ''),
(77, 25, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(78, 25, '_menu_item_xfn', ''),
(79, 25, '_menu_item_url', ''),
(81, 26, '_edit_last', '1'),
(82, 26, '_edit_lock', '1606902436:1'),
(83, 29, '_wp_attached_file', '2020/12/slider-bg.jpg'),
(84, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1440;s:6:\"height\";i:675;s:4:\"file\";s:21:\"2020/12/slider-bg.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"slider-bg-300x141.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:141;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"slider-bg-1024x480.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"slider-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"slider-bg-768x360.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:360;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(85, 30, '_wp_attached_file', '2020/12/1revq.jpg'),
(86, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1563;s:4:\"file\";s:17:\"2020/12/1revq.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"1revq-238x300.jpg\";s:5:\"width\";i:238;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"1revq-814x1024.jpg\";s:5:\"width\";i:814;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"1revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"1revq-768x966.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:966;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"1revq-1221x1536.jpg\";s:5:\"width\";i:1221;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(87, 31, '_wp_attached_file', '2020/12/2revq.jpg'),
(88, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1547;s:4:\"file\";s:17:\"2020/12/2revq.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"2revq-241x300.jpg\";s:5:\"width\";i:241;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"2revq-822x1024.jpg\";s:5:\"width\";i:822;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"2revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"2revq-768x957.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:957;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"2revq-1233x1536.jpg\";s:5:\"width\";i:1233;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(89, 32, '_wp_attached_file', '2020/12/3revq.jpg'),
(90, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1819;s:4:\"file\";s:17:\"2020/12/3revq.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"3revq-205x300.jpg\";s:5:\"width\";i:205;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"3revq-699x1024.jpg\";s:5:\"width\";i:699;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"3revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"3revq-768x1125.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1125;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"3revq-1049x1536.jpg\";s:5:\"width\";i:1049;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(91, 33, '_wp_attached_file', '2020/12/4revq.jpg'),
(92, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1283;s:4:\"file\";s:17:\"2020/12/4revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"4revq-290x300.jpg\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"4revq-991x1024.jpg\";s:5:\"width\";i:991;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"4revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"4revq-768x793.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:793;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(93, 34, '_wp_attached_file', '2020/12/5revq.jpg'),
(94, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1446;s:4:\"file\";s:17:\"2020/12/5revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"5revq-258x300.jpg\";s:5:\"width\";i:258;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"5revq-880x1024.jpg\";s:5:\"width\";i:880;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"5revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"5revq-768x894.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:894;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(95, 35, '_wp_attached_file', '2020/12/6revq.jpg'),
(96, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1123;s:4:\"file\";s:17:\"2020/12/6revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"6revq-300x271.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:271;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"6revq-1024x926.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:926;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"6revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"6revq-768x694.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:694;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(97, 36, '_wp_attached_file', '2020/12/7revq.jpg'),
(98, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1241;s:6:\"height\";i:1214;s:4:\"file\";s:17:\"2020/12/7revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"7revq-300x293.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:293;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"7revq-1024x1002.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1002;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"7revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"7revq-768x751.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:751;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(99, 37, '_wp_attached_file', '2020/12/8revq.jpg'),
(100, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1783;s:4:\"file\";s:17:\"2020/12/8revq.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"8revq-209x300.jpg\";s:5:\"width\";i:209;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"8revq-713x1024.jpg\";s:5:\"width\";i:713;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"8revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"8revq-768x1103.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1103;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:19:\"8revq-1070x1536.jpg\";s:5:\"width\";i:1070;s:6:\"height\";i:1536;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(101, 38, '_wp_attached_file', '2020/12/9revq.jpg'),
(102, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1433;s:4:\"file\";s:17:\"2020/12/9revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"9revq-260x300.jpg\";s:5:\"width\";i:260;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"9revq-888x1024.jpg\";s:5:\"width\";i:888;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"9revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"9revq-768x886.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:886;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(103, 39, '_wp_attached_file', '2020/12/10revq.jpg'),
(104, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1242;s:6:\"height\";i:1250;s:4:\"file\";s:18:\"2020/12/10revq.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"10revq-298x300.jpg\";s:5:\"width\";i:298;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"10revq-1017x1024.jpg\";s:5:\"width\";i:1017;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"10revq-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"10revq-768x773.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:773;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(105, 40, '_wp_attached_file', '2020/12/map.jpg'),
(106, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1440;s:6:\"height\";i:500;s:4:\"file\";s:15:\"2020/12/map.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"map-300x104.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:104;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"map-1024x356.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:356;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"map-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"map-768x267.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:267;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(107, 41, '_wp_attached_file', '2020/12/map2.jpg'),
(108, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:670;s:6:\"height\";i:350;s:4:\"file\";s:16:\"2020/12/map2.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"map2-300x157.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"map2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(109, 42, '_wp_attached_file', '2020/12/otzivview.png'),
(110, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:720;s:6:\"height\";i:1280;s:4:\"file\";s:21:\"2020/12/otzivview.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"otzivview-169x300.png\";s:5:\"width\";i:169;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"otzivview-576x1024.png\";s:5:\"width\";i:576;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"otzivview-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(111, 43, '_wp_attached_file', '2020/12/rev-1.jpg'),
(112, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:225;s:6:\"height\";i:90;s:4:\"file\";s:17:\"2020/12/rev-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"rev-1-150x90.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(113, 12, '_edit_last', '1'),
(114, 12, 'screen_image_container_0_screen_images', '30'),
(115, 12, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(116, 12, 'screen_image_container_1_screen_images', '31'),
(117, 12, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(118, 12, 'screen_image_container', '9'),
(119, 12, '_screen_image_container', 'field_5fc754d43546c'),
(120, 44, 'screen_image_container_0_screen_images', '30'),
(121, 44, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(122, 44, 'screen_image_container_1_screen_images', '31'),
(123, 44, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(124, 44, 'screen_image_container', '2'),
(125, 44, '_screen_image_container', 'field_5fc754d43546c'),
(126, 12, 'screen_image_container_2_screen_images', '32'),
(127, 12, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(128, 45, 'screen_image_container_0_screen_images', '30'),
(129, 45, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(130, 45, 'screen_image_container_1_screen_images', '31'),
(131, 45, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(132, 45, 'screen_image_container', '3'),
(133, 45, '_screen_image_container', 'field_5fc754d43546c'),
(134, 45, 'screen_image_container_2_screen_images', '32'),
(135, 45, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(136, 12, 'screen_image_container_3_screen_images', '38'),
(137, 12, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(138, 46, 'screen_image_container_0_screen_images', '30'),
(139, 46, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(140, 46, 'screen_image_container_1_screen_images', '31'),
(141, 46, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(142, 46, 'screen_image_container', '4'),
(143, 46, '_screen_image_container', 'field_5fc754d43546c'),
(144, 46, 'screen_image_container_2_screen_images', '32'),
(145, 46, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(146, 46, 'screen_image_container_3_screen_images', '33'),
(147, 46, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(148, 12, 'screen_image_container_4_screen_images', '37'),
(149, 12, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(150, 12, 'screen_image_container_5_screen_images', '33'),
(151, 12, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(152, 47, 'screen_image_container_0_screen_images', '30'),
(153, 47, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(154, 47, 'screen_image_container_1_screen_images', '31'),
(155, 47, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(156, 47, 'screen_image_container', '6'),
(157, 47, '_screen_image_container', 'field_5fc754d43546c'),
(158, 47, 'screen_image_container_2_screen_images', '32'),
(159, 47, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(160, 47, 'screen_image_container_3_screen_images', '33'),
(161, 47, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(162, 47, 'screen_image_container_4_screen_images', '34'),
(163, 47, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(164, 47, 'screen_image_container_5_screen_images', '35'),
(165, 47, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(166, 48, 'screen_image_container_0_screen_images', '30'),
(167, 48, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(168, 48, 'screen_image_container_1_screen_images', '31'),
(169, 48, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(170, 48, 'screen_image_container', '6'),
(171, 48, '_screen_image_container', 'field_5fc754d43546c'),
(172, 48, 'screen_image_container_2_screen_images', '32'),
(173, 48, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(174, 48, 'screen_image_container_3_screen_images', '35'),
(175, 48, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(176, 48, 'screen_image_container_4_screen_images', '34'),
(177, 48, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(178, 48, 'screen_image_container_5_screen_images', '33'),
(179, 48, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(180, 12, 'screen_image_container_6_screen_images', '35'),
(181, 12, '_screen_image_container_6_screen_images', 'field_5fc754ea3546d'),
(182, 12, 'screen_image_container_7_screen_images', '36'),
(183, 12, '_screen_image_container_7_screen_images', 'field_5fc754ea3546d'),
(184, 12, 'screen_image_container_8_screen_images', '39'),
(185, 12, '_screen_image_container_8_screen_images', 'field_5fc754ea3546d'),
(186, 49, 'screen_image_container_0_screen_images', '30'),
(187, 49, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(188, 49, 'screen_image_container_1_screen_images', '31'),
(189, 49, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(190, 49, 'screen_image_container', '9'),
(191, 49, '_screen_image_container', 'field_5fc754d43546c'),
(192, 49, 'screen_image_container_2_screen_images', '32'),
(193, 49, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(194, 49, 'screen_image_container_3_screen_images', '33'),
(195, 49, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(196, 49, 'screen_image_container_4_screen_images', '35'),
(197, 49, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(198, 49, 'screen_image_container_5_screen_images', '36'),
(199, 49, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(200, 49, 'screen_image_container_6_screen_images', '37'),
(201, 49, '_screen_image_container_6_screen_images', 'field_5fc754ea3546d'),
(202, 49, 'screen_image_container_7_screen_images', '38'),
(203, 49, '_screen_image_container_7_screen_images', 'field_5fc754ea3546d'),
(204, 49, 'screen_image_container_8_screen_images', '39'),
(205, 49, '_screen_image_container_8_screen_images', 'field_5fc754ea3546d'),
(206, 50, 'screen_image_container_0_screen_images', '30'),
(207, 50, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(208, 50, 'screen_image_container_1_screen_images', '31'),
(209, 50, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(210, 50, 'screen_image_container', '9'),
(211, 50, '_screen_image_container', 'field_5fc754d43546c'),
(212, 50, 'screen_image_container_2_screen_images', '32'),
(213, 50, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(214, 50, 'screen_image_container_3_screen_images', '33'),
(215, 50, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(216, 50, 'screen_image_container_4_screen_images', '37'),
(217, 50, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(218, 50, 'screen_image_container_5_screen_images', '38'),
(219, 50, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(220, 50, 'screen_image_container_6_screen_images', '35'),
(221, 50, '_screen_image_container_6_screen_images', 'field_5fc754ea3546d'),
(222, 50, 'screen_image_container_7_screen_images', '36'),
(223, 50, '_screen_image_container_7_screen_images', 'field_5fc754ea3546d'),
(224, 50, 'screen_image_container_8_screen_images', '39'),
(225, 50, '_screen_image_container_8_screen_images', 'field_5fc754ea3546d'),
(226, 51, 'screen_image_container_0_screen_images', '30'),
(227, 51, '_screen_image_container_0_screen_images', 'field_5fc754ea3546d'),
(228, 51, 'screen_image_container_1_screen_images', '31'),
(229, 51, '_screen_image_container_1_screen_images', 'field_5fc754ea3546d'),
(230, 51, 'screen_image_container', '9'),
(231, 51, '_screen_image_container', 'field_5fc754d43546c'),
(232, 51, 'screen_image_container_2_screen_images', '32'),
(233, 51, '_screen_image_container_2_screen_images', 'field_5fc754ea3546d'),
(234, 51, 'screen_image_container_3_screen_images', '38'),
(235, 51, '_screen_image_container_3_screen_images', 'field_5fc754ea3546d'),
(236, 51, 'screen_image_container_4_screen_images', '37'),
(237, 51, '_screen_image_container_4_screen_images', 'field_5fc754ea3546d'),
(238, 51, 'screen_image_container_5_screen_images', '33'),
(239, 51, '_screen_image_container_5_screen_images', 'field_5fc754ea3546d'),
(240, 51, 'screen_image_container_6_screen_images', '35'),
(241, 51, '_screen_image_container_6_screen_images', 'field_5fc754ea3546d'),
(242, 51, 'screen_image_container_7_screen_images', '36'),
(243, 51, '_screen_image_container_7_screen_images', 'field_5fc754ea3546d'),
(244, 51, 'screen_image_container_8_screen_images', '39'),
(245, 51, '_screen_image_container_8_screen_images', 'field_5fc754ea3546d'),
(246, 52, '_edit_last', '1'),
(247, 52, '_edit_lock', '1606987719:1'),
(248, 59, '_edit_lock', '1606914168:1'),
(249, 60, '_edit_lock', '1606984564:1'),
(250, 61, '_edit_lock', '1606984272:1'),
(251, 63, '_edit_lock', '1606991459:1'),
(252, 64, '_edit_last', '1'),
(253, 64, '_edit_lock', '1606999554:1'),
(256, 1, '_edit_lock', '1606986234:1'),
(257, 68, '_edit_last', '1'),
(258, 68, '_edit_lock', '1607007948:1'),
(259, 69, '_edit_last', '1'),
(260, 69, '_edit_lock', '1607007782:1'),
(261, 70, '_edit_last', '1'),
(262, 70, '_edit_lock', '1607007772:1'),
(263, 16, '_edit_last', '1'),
(264, 16, 'list', ''),
(265, 16, '_list', 'field_5fc784e0faab3'),
(266, 71, 'list', ''),
(267, 71, '_list', 'field_5fc784e0faab3'),
(268, 16, 'title', 'Наши главные отличия от других парикмахерских:'),
(269, 16, '_title', 'field_5fc8b00300cc5'),
(270, 16, 'content', '<p> Мы используем в работе одноразовые полотенца и воротнички;</p>\r\n<div class=\"info__text textb\">\r\n\r\n— У нас всегда чисто, потому что в парикмахерские шкафы встроена система мощных пылесосов, которые всасывают мусор с пола. Все, даже самые мелкие и незаметные глазу, остриженные волоски моментально затягиваются в специальный электросовок внизу шкафа. Мы не используем обычные совки для уборки волос и не складируем волосы в рабочем пространстве парикмахерской;\r\n\r\n— После каждого клиента инструменты отправляются на 3-хступенчатую обработку;\r\n\r\n— Мастера используют для работы перчатки. Это дополнительная защита для вас и парикмахера, которая защищает от порезов и заноз;\r\n\r\n— Мастер при вас откроет новую расческу и будет использовать ее во время стрижки. После вы получите расческу в подарок.\r\n\r\n</div>'),
(271, 16, '_content', 'field_5fc8b03000cc6'),
(272, 74, 'list', ''),
(273, 74, '_list', 'field_5fc784e0faab3'),
(274, 74, 'title', 'Наши главные отличия от других парикмахерских:'),
(275, 74, '_title', 'field_5fc8b00300cc5'),
(276, 74, 'content', '<div class=\"info__stitle stitle stitle_sm\">Наши главные отличия от других парикмахерских:</div>\r\n<div class=\"info__text textb\">\r\n\r\n— Мы используем в работе одноразовые полотенца и воротнички;\r\n\r\n— У нас всегда чисто, потому что в парикмахерские шкафы встроена система мощных пылесосов, которые всасывают мусор с пола. Все, даже самые мелкие и незаметные глазу, остриженные волоски моментально затягиваются в специальный электросовок внизу шкафа. Мы не используем обычные совки для уборки волос и не складируем волосы в рабочем пространстве парикмахерской;\r\n\r\n— После каждого клиента инструменты отправляются на 3-хступенчатую обработку;\r\n\r\n— Мастера используют для работы перчатки. Это дополнительная защита для вас и парикмахера, которая защищает от порезов и заноз;\r\n\r\n— Мастер при вас откроет новую расческу и будет использовать ее во время стрижки. После вы получите расческу в подарок.\r\n\r\n</div>'),
(277, 74, '_content', 'field_5fc8b03000cc6'),
(278, 75, 'list', ''),
(279, 75, '_list', 'field_5fc784e0faab3'),
(280, 75, 'title', 'Наши главные отличия от других парикмахерских:'),
(281, 75, '_title', 'field_5fc8b00300cc5'),
(282, 75, 'content', '<div class=\"info__stitle stitle stitle_sm\">— Мы используем в работе одноразовые полотенца и воротнички;</div>\r\n<div class=\"info__text textb\">\r\n\r\n— У нас всегда чисто, потому что в парикмахерские шкафы встроена система мощных пылесосов, которые всасывают мусор с пола. Все, даже самые мелкие и незаметные глазу, остриженные волоски моментально затягиваются в специальный электросовок внизу шкафа. Мы не используем обычные совки для уборки волос и не складируем волосы в рабочем пространстве парикмахерской;\r\n\r\n— После каждого клиента инструменты отправляются на 3-хступенчатую обработку;\r\n\r\n— Мастера используют для работы перчатки. Это дополнительная защита для вас и парикмахера, которая защищает от порезов и заноз;\r\n\r\n— Мастер при вас откроет новую расческу и будет использовать ее во время стрижки. После вы получите расческу в подарок.\r\n\r\n</div>'),
(283, 75, '_content', 'field_5fc8b03000cc6'),
(284, 76, 'list', ''),
(285, 76, '_list', 'field_5fc784e0faab3'),
(286, 76, 'title', 'Наши главные отличия от других парикмахерских:'),
(287, 76, '_title', 'field_5fc8b00300cc5'),
(288, 76, 'content', '<p> Мы используем в работе одноразовые полотенца и воротнички;</p>\r\n<div class=\"info__text textb\">\r\n\r\n— У нас всегда чисто, потому что в парикмахерские шкафы встроена система мощных пылесосов, которые всасывают мусор с пола. Все, даже самые мелкие и незаметные глазу, остриженные волоски моментально затягиваются в специальный электросовок внизу шкафа. Мы не используем обычные совки для уборки волос и не складируем волосы в рабочем пространстве парикмахерской;\r\n\r\n— После каждого клиента инструменты отправляются на 3-хступенчатую обработку;\r\n\r\n— Мастера используют для работы перчатки. Это дополнительная защита для вас и парикмахера, которая защищает от порезов и заноз;\r\n\r\n— Мастер при вас откроет новую расческу и будет использовать ее во время стрижки. После вы получите расческу в подарок.\r\n\r\n</div>'),
(289, 76, '_content', 'field_5fc8b03000cc6'),
(290, 84, '_edit_last', '1'),
(291, 84, '_edit_lock', '1606991948:1'),
(292, 18, '_edit_last', '1'),
(293, 18, 'slaon_title', 'Выберите ближайшую к вам парикмахерскую из списка слева, чтобы ознакомиться с прейскурантом.'),
(294, 18, '_slaon_title', 'field_5fc8c0245f98e'),
(295, 86, 'slaon_title', 'Выберите ближайшую к вам парикмахерскую из списка слева, чтобы ознакомиться с прейскурантом.'),
(296, 86, '_slaon_title', 'field_5fc8c0245f98e'),
(297, 68, 'time_work', 'c 10:00 до 22:00 (без выходных)'),
(298, 68, '_time_work', 'field_5fc8b7fe51510'),
(299, 68, 'number_telephone', '+7 (495) 559-44-22'),
(300, 68, '_number_telephone', 'field_5fc8b81951511'),
(301, 68, 'link-social_0_link', 'https://vk.com/supermanhair'),
(302, 68, '_link-social_0_link', 'field_5fc8b89051513'),
(303, 68, 'link-social_1_link', 'https://www.instagram.com/supermancut/'),
(304, 68, '_link-social_1_link', 'field_5fc8b89051513'),
(305, 68, 'link-social', '2'),
(306, 68, '_link-social', 'field_5fc8b83551512'),
(307, 68, 'price_list_0_title_price', 'Стрижки'),
(308, 68, '_price_list_0_title_price', 'field_5fc8b8eda389f'),
(309, 68, 'price_list_0_price_list', '<div class=\"body wrapper\">\r\n<div class=\"body__content\">\r\n<div class=\"salon\">\r\n<div class=\"salon__prices\">\r\n<ul>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Стрижка мужская</div>\r\n<div class=\"salon__prices-v\">250 Р</div></li>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Стрижка женская</div>\r\n<div class=\"salon__prices-v\">250 Р</div></li>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Стрижка детская</div>\r\n<div class=\"salon__prices-v\">250 Р</div></li>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Подравнивание кончиков</div>\r\n<div class=\"salon__prices-v\">150 Р</div></li>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Стрижка наголо</div>\r\n<div class=\"salon__prices-v\">150 Р</div></li>\r\n 	<li>\r\n<div class=\"salon__prices-l\">Стрижка ветеранам ВОВ</div>\r\n<div class=\"salon__prices-v\">100 Р</div></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div class=\"footerpr\"></div>\r\n<footer class=\"footer_foot\">\r\n<div class=\"footer__wrapper wrapper\">\r\n<div class=\"footer__logo logo\"></div>\r\n</div>\r\n</footer>'),
(310, 68, '_price_list_0_price_list', 'field_5fc8b8ffa38a0'),
(311, 68, 'price_list_1_title_price', ''),
(312, 68, '_price_list_1_title_price', 'field_5fc8b8eda389f'),
(313, 68, 'price_list_1_price_list', ''),
(314, 68, '_price_list_1_price_list', 'field_5fc8b8ffa38a0'),
(315, 68, 'price_list', '2'),
(316, 68, '_price_list', 'field_5fc8b8c9a389e'),
(317, 68, 'price_list_0_price', ''),
(318, 68, '_price_list_0_price', 'field_5fc8b8ffa38a0'),
(319, 68, 'price_list_1_price', ''),
(320, 68, '_price_list_1_price', 'field_5fc8b8ffa38a0'),
(321, 68, 'link_vk', 'https://vk.com/supermanhair'),
(322, 68, '_link_vk', 'field_5fc8c73893a25'),
(323, 68, 'link_insta', 'https://www.instagram.com/supermancut/'),
(324, 68, '_link_insta', 'field_5fc8c74493a26'),
(325, 68, 'price_list_0_list_0_price_name', 'Стрижка мужская'),
(326, 68, '_price_list_0_list_0_price_name', 'field_5fc8c83d0ee90'),
(327, 68, 'price_list_0_list_0_price', '250 Р'),
(328, 68, '_price_list_0_list_0_price', 'field_5fc8c8480ee91'),
(329, 68, 'price_list_0_list', '6'),
(330, 68, '_price_list_0_list', 'field_5fc8b8ffa38a0'),
(331, 68, 'price_list_1_list', ''),
(332, 68, '_price_list_1_list', 'field_5fc8b8ffa38a0'),
(333, 68, 'price_list_0_list_1_price_name', 'Стрижка женская'),
(334, 68, '_price_list_0_list_1_price_name', 'field_5fc8c83d0ee90'),
(335, 68, 'price_list_0_list_1_price', '250 Р'),
(336, 68, '_price_list_0_list_1_price', 'field_5fc8c8480ee91'),
(337, 68, 'price_list_0_list_2_price_name', 'Стрижка детская'),
(338, 68, '_price_list_0_list_2_price_name', 'field_5fc8c83d0ee90'),
(339, 68, 'price_list_0_list_2_price', '250 Р'),
(340, 68, '_price_list_0_list_2_price', 'field_5fc8c8480ee91'),
(341, 68, 'price_list_0_list_3_price_name', 'Подравнивание кончиков'),
(342, 68, '_price_list_0_list_3_price_name', 'field_5fc8c83d0ee90'),
(343, 68, 'price_list_0_list_3_price', '150 Р'),
(344, 68, '_price_list_0_list_3_price', 'field_5fc8c8480ee91'),
(345, 68, 'price_list_0_list_4_price_name', 'Стрижка наголо'),
(346, 68, '_price_list_0_list_4_price_name', 'field_5fc8c83d0ee90'),
(347, 68, 'price_list_0_list_4_price', '150 Р'),
(348, 68, '_price_list_0_list_4_price', 'field_5fc8c8480ee91'),
(349, 68, 'price_list_0_list_5_price_name', 'Стрижка ветеранам ВОВ'),
(350, 68, '_price_list_0_list_5_price_name', 'field_5fc8c83d0ee90'),
(351, 68, 'price_list_0_list_5_price', '100 Р'),
(352, 68, '_price_list_0_list_5_price', 'field_5fc8c8480ee91'),
(353, 92, '_edit_last', '1'),
(354, 92, '_edit_lock', '1606995420:1'),
(355, 93, '_edit_last', '1'),
(356, 93, '_edit_lock', '1606995447:1'),
(357, 94, '_edit_last', '1'),
(358, 94, '_edit_lock', '1606995471:1'),
(359, 95, '_edit_last', '1'),
(360, 95, '_edit_lock', '1606995506:1'),
(361, 69, 'time_work', 'c 09:00 до 21:00 (без выходных)'),
(362, 69, '_time_work', 'field_5fc8b7fe51510'),
(363, 69, 'number_telephone', '+7 (495) 564-46-33'),
(364, 69, '_number_telephone', 'field_5fc8b81951511'),
(365, 69, 'link_vk', 'https://vk.com/lazer_poyma'),
(366, 69, '_link_vk', 'field_5fc8c73893a25'),
(367, 69, 'link_insta', 'https://www.instagram.com/sfera.centr/'),
(368, 69, '_link_insta', 'field_5fc8c74493a26'),
(369, 69, 'price_list_0_title_price', 'Стрижки'),
(370, 69, '_price_list_0_title_price', 'field_5fc8b8eda389f'),
(371, 69, 'price_list_0_list_0_price_name', 'Стрижка мужская'),
(372, 69, '_price_list_0_list_0_price_name', 'field_5fc8c83d0ee90'),
(373, 69, 'price_list_0_list_0_price', '250 Р'),
(374, 69, '_price_list_0_list_0_price', 'field_5fc8c8480ee91'),
(375, 69, 'price_list_0_list_1_price_name', 'Стрижка женская'),
(376, 69, '_price_list_0_list_1_price_name', 'field_5fc8c83d0ee90'),
(377, 69, 'price_list_0_list_1_price', '250 Р'),
(378, 69, '_price_list_0_list_1_price', 'field_5fc8c8480ee91'),
(379, 69, 'price_list_0_list_2_price_name', 'Стрижка детская'),
(380, 69, '_price_list_0_list_2_price_name', 'field_5fc8c83d0ee90'),
(381, 69, 'price_list_0_list_2_price', '250 Р'),
(382, 69, '_price_list_0_list_2_price', 'field_5fc8c8480ee91'),
(383, 69, 'price_list_0_list_3_price_name', 'Подравнивание кончиков'),
(384, 69, '_price_list_0_list_3_price_name', 'field_5fc8c83d0ee90'),
(385, 69, 'price_list_0_list_3_price', '200 Р'),
(386, 69, '_price_list_0_list_3_price', 'field_5fc8c8480ee91'),
(387, 69, 'price_list_0_list_4_price_name', 'Стрижка наголо'),
(388, 69, '_price_list_0_list_4_price_name', 'field_5fc8c83d0ee90'),
(389, 69, 'price_list_0_list_4_price', '200 Р'),
(390, 69, '_price_list_0_list_4_price', 'field_5fc8c8480ee91'),
(391, 69, 'price_list_0_list_5_price_name', 'Стрижка ветеров ВОВ'),
(392, 69, '_price_list_0_list_5_price_name', 'field_5fc8c83d0ee90'),
(393, 69, 'price_list_0_list_5_price', 'Бесплатно'),
(394, 69, '_price_list_0_list_5_price', 'field_5fc8c8480ee91'),
(395, 69, 'price_list_0_list', '6'),
(396, 69, '_price_list_0_list', 'field_5fc8b8ffa38a0'),
(397, 69, 'price_list_1_title_price', 'Маникюр'),
(398, 69, '_price_list_1_title_price', 'field_5fc8b8eda389f'),
(399, 69, 'price_list_1_list_0_price_name', 'Маникюр'),
(400, 69, '_price_list_1_list_0_price_name', 'field_5fc8c83d0ee90'),
(401, 69, 'price_list_1_list_0_price', '500 Р'),
(402, 69, '_price_list_1_list_0_price', 'field_5fc8c8480ee91'),
(403, 69, 'price_list_1_list_1_price_name', 'Педикюр с полировкой стоп'),
(404, 69, '_price_list_1_list_1_price_name', 'field_5fc8c83d0ee90'),
(405, 69, 'price_list_1_list_1_price', '1 200 Р'),
(406, 69, '_price_list_1_list_1_price', 'field_5fc8c8480ee91'),
(407, 69, 'price_list_1_list_2_price_name', 'Экспресс-педикюр'),
(408, 69, '_price_list_1_list_2_price_name', 'field_5fc8c83d0ee90'),
(409, 69, 'price_list_1_list_2_price', '1 000 Р'),
(410, 69, '_price_list_1_list_2_price', 'field_5fc8c8480ee91'),
(411, 69, 'price_list_1_list_3_price_name', 'Коррекция ногтей гелем'),
(412, 69, '_price_list_1_list_3_price_name', 'field_5fc8c83d0ee90'),
(413, 69, 'price_list_1_list_3_price', '1 100 Р'),
(414, 69, '_price_list_1_list_3_price', 'field_5fc8c8480ee91'),
(415, 69, 'price_list_1_list_4_price_name', 'Детский маникюр'),
(416, 69, '_price_list_1_list_4_price_name', 'field_5fc8c83d0ee90'),
(417, 69, 'price_list_1_list_4_price', 'от 50 Р'),
(418, 69, '_price_list_1_list_4_price', 'field_5fc8c8480ee91'),
(419, 69, 'price_list_1_list', '5'),
(420, 69, '_price_list_1_list', 'field_5fc8b8ffa38a0'),
(421, 69, 'price_list', '2'),
(422, 69, '_price_list', 'field_5fc8b8c9a389e'),
(423, 70, 'time_work', 'c 10:00 до 18:00 (без выходных)'),
(424, 70, '_time_work', 'field_5fc8b7fe51510'),
(425, 70, 'number_telephone', '+7 (495) 789-57-54'),
(426, 70, '_number_telephone', 'field_5fc8b81951511'),
(427, 70, 'link_vk', 'https://vk.com/britvabarber'),
(428, 70, '_link_vk', 'field_5fc8c73893a25'),
(429, 70, 'link_insta', 'https://www.instagram.com/britvabarbershop_/'),
(430, 70, '_link_insta', 'field_5fc8c74493a26'),
(431, 70, 'price_list_0_title_price', 'Стрижки'),
(432, 70, '_price_list_0_title_price', 'field_5fc8b8eda389f'),
(433, 70, 'price_list_0_list_0_price_name', 'Стрижка мужская'),
(434, 70, '_price_list_0_list_0_price_name', 'field_5fc8c83d0ee90'),
(435, 70, 'price_list_0_list_0_price', '250 Р'),
(436, 70, '_price_list_0_list_0_price', 'field_5fc8c8480ee91'),
(437, 70, 'price_list_0_list_1_price_name', 'Стрижка женская'),
(438, 70, '_price_list_0_list_1_price_name', 'field_5fc8c83d0ee90'),
(439, 70, 'price_list_0_list_1_price', '250 Р'),
(440, 70, '_price_list_0_list_1_price', 'field_5fc8c8480ee91'),
(441, 70, 'price_list_0_list_2_price_name', 'Стрижка детская'),
(442, 70, '_price_list_0_list_2_price_name', 'field_5fc8c83d0ee90'),
(443, 70, 'price_list_0_list_2_price', '250 Р'),
(444, 70, '_price_list_0_list_2_price', 'field_5fc8c8480ee91'),
(445, 70, 'price_list_0_list_3_price_name', 'Окантовка'),
(446, 70, '_price_list_0_list_3_price_name', 'field_5fc8c83d0ee90'),
(447, 70, 'price_list_0_list_3_price', '100 Р'),
(448, 70, '_price_list_0_list_3_price', 'field_5fc8c8480ee91'),
(449, 70, 'price_list_0_list_4_price_name', 'Подравнивание кончиков'),
(450, 70, '_price_list_0_list_4_price_name', 'field_5fc8c83d0ee90'),
(451, 70, 'price_list_0_list_4_price', '300 Р'),
(452, 70, '_price_list_0_list_4_price', 'field_5fc8c8480ee91'),
(453, 70, 'price_list_0_list_5_price_name', 'Hair-tattoo'),
(454, 70, '_price_list_0_list_5_price_name', 'field_5fc8c83d0ee90'),
(455, 70, 'price_list_0_list_5_price', 'от 400 Р'),
(456, 70, '_price_list_0_list_5_price', 'field_5fc8c8480ee91'),
(457, 70, 'price_list_0_list_6_price_name', 'Стрижка ветеров ВОВ'),
(458, 70, '_price_list_0_list_6_price_name', 'field_5fc8c83d0ee90'),
(459, 70, 'price_list_0_list_6_price', '50 Р'),
(460, 70, '_price_list_0_list_6_price', 'field_5fc8c8480ee91'),
(461, 70, 'price_list_0_list', '7'),
(462, 70, '_price_list_0_list', 'field_5fc8b8ffa38a0'),
(463, 70, 'price_list_1_title_price', 'Укладка'),
(464, 70, '_price_list_1_title_price', 'field_5fc8b8eda389f'),
(465, 70, 'price_list_1_list_0_price_name', 'Био-химия'),
(466, 70, '_price_list_1_list_0_price_name', 'field_5fc8c83d0ee90'),
(467, 70, 'price_list_1_list_0_price', 'от 1 800 Р'),
(468, 70, '_price_list_1_list_0_price', 'field_5fc8c8480ee91'),
(469, 70, 'price_list_1_list_1_price_name', 'Экспресс-укладка'),
(470, 70, '_price_list_1_list_1_price_name', 'field_5fc8c83d0ee90'),
(471, 70, 'price_list_1_list_1_price', '500 Р'),
(472, 70, '_price_list_1_list_1_price', 'field_5fc8c8480ee91'),
(473, 70, 'price_list_1_list', '2'),
(474, 70, '_price_list_1_list', 'field_5fc8b8ffa38a0'),
(475, 70, 'price_list_2_title_price', 'Окрашивание'),
(476, 70, '_price_list_2_title_price', 'field_5fc8b8eda389f'),
(477, 70, 'price_list_2_list_0_price_name', 'Окрашивание'),
(478, 70, '_price_list_2_list_0_price_name', 'field_5fc8c83d0ee90'),
(479, 70, 'price_list_2_list_0_price', 'от 2 000 Р'),
(480, 70, '_price_list_2_list_0_price', 'field_5fc8c8480ee91'),
(481, 70, 'price_list_2_list_1_price_name', 'Омбре и балаяж'),
(482, 70, '_price_list_2_list_1_price_name', 'field_5fc8c83d0ee90'),
(483, 70, 'price_list_2_list_1_price', '3 000 Р'),
(484, 70, '_price_list_2_list_1_price', 'field_5fc8c8480ee91'),
(485, 70, 'price_list_2_list_2_price_name', 'Шатуш'),
(486, 70, '_price_list_2_list_2_price_name', 'field_5fc8c83d0ee90'),
(487, 70, 'price_list_2_list_2_price', '2 500 Р'),
(488, 70, '_price_list_2_list_2_price', 'field_5fc8c8480ee91'),
(489, 70, 'price_list_2_list_3_price_name', 'Осветление'),
(490, 70, '_price_list_2_list_3_price_name', 'field_5fc8c83d0ee90'),
(491, 70, 'price_list_2_list_3_price', '2 000 Р'),
(492, 70, '_price_list_2_list_3_price', 'field_5fc8c8480ee91'),
(493, 70, 'price_list_2_list', '4'),
(494, 70, '_price_list_2_list', 'field_5fc8b8ffa38a0'),
(495, 70, 'price_list', '3'),
(496, 70, '_price_list', 'field_5fc8b8c9a389e'),
(497, 97, '_edit_last', '1'),
(498, 97, '_edit_lock', '1607000137:1'),
(499, 97, 'time_work', ''),
(500, 97, '_time_work', 'field_5fc8b7fe51510'),
(501, 97, 'number_telephone', ''),
(502, 97, '_number_telephone', 'field_5fc8b81951511'),
(503, 97, 'link_vk', ''),
(504, 97, '_link_vk', 'field_5fc8c73893a25'),
(505, 97, 'link_insta', ''),
(506, 97, '_link_insta', 'field_5fc8c74493a26'),
(507, 97, 'price_list', ''),
(508, 97, '_price_list', 'field_5fc8b8c9a389e'),
(509, 98, '_edit_last', '1'),
(510, 98, '_edit_lock', '1607005394:1'),
(511, 100, '_menu_item_type', 'post_type'),
(512, 100, '_menu_item_menu_item_parent', '0'),
(513, 100, '_menu_item_object_id', '98'),
(514, 100, '_menu_item_object', 'page'),
(515, 100, '_menu_item_target', ''),
(516, 100, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(517, 100, '_menu_item_xfn', ''),
(518, 100, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-12-01 11:55:26', '2020-12-01 11:55:26', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-12-01 11:55:26', '2020-12-01 11:55:26', '', 0, 'http://striksi.loc/?p=1', 0, 'post', '', 1),
(2, 1, '2020-12-01 11:55:26', '2020-12-01 11:55:26', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://striksi.loc/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2020-12-02 08:33:54', '2020-12-02 08:33:54', '', 0, 'http://striksi.loc/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-12-01 11:55:26', '2020-12-01 11:55:26', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://striksi.loc.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2020-12-02 08:33:46', '2020-12-02 08:33:46', '', 0, 'http://striksi.loc/?page_id=3', 0, 'page', '', 0),
(4, 1, '2020-12-01 11:56:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-12-01 11:56:02', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?p=4', 0, 'post', '', 0),
(5, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '', 'Обновлённая UMoMA открывает двери', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2020-12-01 12:11:50', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/wp-content/uploads/2020/12/2020-landscape-1.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '<!-- wp:group {\"align\":\"wide\"} --><div class=\"wp-block-group alignwide\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"align\":\"center\"} --><h2 class=\"has-text-align-center\">Первоклассное место для современного искусства в северной Швеции. Открыто с 10:00 до 18:00, ежедневно в летние месяцы.</h2><!-- /wp:heading --></div></div><!-- /wp:group --><!-- wp:columns {\"align\":\"wide\"} --><div class=\"wp-block-columns alignwide\"><!-- wp:column --><div class=\"wp-block-column\"><!-- wp:group --><div class=\"wp-block-group\"><div class=\"wp-block-group__inner-container\"><!-- wp:image {\"align\":\"full\",\"id\":37,\"sizeSlug\":\"full\"} --><figure class=\"wp-block-image alignfull size-full\"><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-1.png\" alt=\"\" class=\"wp-image-37\"/></figure><!-- /wp:image --><!-- wp:heading {\"level\":3} --><h3>Дни работы</h3><!-- /wp:heading --><!-- wp:paragraph --><p>1 августа - 1 декабря</p><!-- /wp:paragraph --><!-- wp:button {\"className\":\"is-style-outline\"} --><div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link\" href=\"https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/\">Читать далее</a></div><!-- /wp:button --></div></div><!-- /wp:group --><!-- wp:group --><div class=\"wp-block-group\"><div class=\"wp-block-group__inner-container\"><!-- wp:image {\"align\":\"full\",\"id\":37,\"sizeSlug\":\"full\"} --><figure class=\"wp-block-image alignfull size-full\"><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-3.png\" alt=\"\" class=\"wp-image-37\"/></figure><!-- /wp:image --><!-- wp:heading {\"level\":3} --><h3>Театр действий</h3><!-- /wp:heading --><!-- wp:paragraph --><p>1 октября - 1 декабря</p><!-- /wp:paragraph --><!-- wp:button {\"className\":\"is-style-outline\"} --><div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link\" href=\"https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/\">Читать далее</a></div><!-- /wp:button --></div></div><!-- /wp:group --></div><!-- /wp:column --><!-- wp:column --><div class=\"wp-block-column\"><!-- wp:group --><div class=\"wp-block-group\"><div class=\"wp-block-group__inner-container\"><!-- wp:image {\"align\":\"full\",\"id\":37,\"sizeSlug\":\"full\"} --><figure class=\"wp-block-image alignfull size-full\"><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-2.png\" alt=\"\" class=\"wp-image-37\"/></figure><!-- /wp:image --><!-- wp:heading {\"level\":3} --><h3>Жизнь которую вы заслужили</h3><!-- /wp:heading --><!-- wp:paragraph --><p>1 августа - 1 декабря</p><!-- /wp:paragraph --><!-- wp:button {\"className\":\"is-style-outline\"} --><div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link\" href=\"https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/\">Читать далее</a></div><!-- /wp:button --></div></div><!-- /wp:group --><!-- wp:group --><div class=\"wp-block-group\"><div class=\"wp-block-group__inner-container\"><!-- wp:image {\"align\":\"full\",\"id\":37,\"sizeSlug\":\"full\"} --><figure class=\"wp-block-image alignfull size-full\"><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-three-quarters-4.png\" alt=\"\" class=\"wp-image-37\"/></figure><!-- /wp:image --><!-- wp:heading {\"level\":3} --><h3>От Синьяка до Матисса</h3><!-- /wp:heading --><!-- wp:paragraph --><p>1 октября - 1 декабря</p><!-- /wp:paragraph --><!-- wp:button {\"className\":\"is-style-outline\"} --><div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link\" href=\"https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/\">Читать далее</a></div><!-- /wp:button --></div></div><!-- /wp:group --></div><!-- /wp:column --></div><!-- /wp:columns --><!-- wp:image {\"align\":\"full\",\"id\":37,\"sizeSlug\":\"full\"} --><figure class=\"wp-block-image alignfull size-full\"><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-landscape-2.png\" alt=\"\" class=\"wp-image-37\"/></figure><!-- /wp:image --><!-- wp:group {\"align\":\"wide\"} --><div class=\"wp-block-group alignwide\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"align\":\"center\",\"textColor\":\"accent\"} --><h2 class=\"has-accent-color has-text-align-center\">\"Киборги, как установила философ Донна Харауэй, не почтительны. Они не помнят космос.\"</h2><!-- /wp:heading --></div></div><!-- /wp:group --><!-- wp:paragraph {\"dropCap\":true} --><p class=\"has-drop-cap\">На семи этажах потрясающей архитектуры UMoMA представляет выставку международного современного искусства, иногда наряду с историческими ретроспективами искусства. Экзистенциальные, политические и философские проблемы являются неотъемлемой частью нашей программы. В качестве посетителя вы приглашены на экскурсии, выступления артистов, лекции, показы фильмов и другие мероприятия с бесплатным входом.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Выставки проводятся UMoMA в сотрудничестве с художниками и музеями по всему миру, они часто привлекают международное внимание. UMoMA получили специальную награду от Европейского музея года и вошли в число лучших кандидатов на премию «Шведский музей года», а также на приз музея Совета Европы.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p></p><!-- /wp:paragraph --><!-- wp:group {\"customBackgroundColor\":\"#ffffff\",\"align\":\"wide\"} --><div class=\"wp-block-group alignwide has-background\" style=\"background-color:#ffffff\"><div class=\"wp-block-group__inner-container\"><!-- wp:group --><div class=\"wp-block-group\"><div class=\"wp-block-group__inner-container\"><!-- wp:heading {\"align\":\"center\"} --><h2 class=\"has-text-align-center\">Вступайте и получайте эксклюзивные предложения!</h2><!-- /wp:heading --><!-- wp:paragraph {\"align\":\"center\"} --><p class=\"has-text-align-center\">Члены клуба получают доступ к эксклюзивным выставкам и продажам. Оплата членства ежегодно, 99.99$</p><!-- /wp:paragraph --><!-- wp:button {\"align\":\"center\"} --><div class=\"wp-block-button aligncenter\"><a class=\"wp-block-button__link\" href=\"https://make.wordpress.org/core/2019/09/27/block-editor-theme-related-updates-in-wordpress-5-3/\">Вступайте в клуб</a></div><!-- /wp:button --></div></div><!-- /wp:group --></div></div><!-- /wp:group --><!-- wp:gallery {\"ids\":[39,38],\"align\":\"wide\"} --><figure class=\"wp-block-gallery alignwide columns-2 is-cropped\"><ul class=\"blocks-gallery-grid\"><li class=\"blocks-gallery-item\"><figure><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-square-2.png\" alt=\"\" data-id=\"39\" data-full-url=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-square-2.png\" data-link=\"assets/images/2020-square-2/\" class=\"wp-image-39\"/></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-square-1.png\" alt=\"\" data-id=\"38\" data-full-url=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-square-1.png\" data-link=\"http://striksi.loc/wp-content/themes/twentytwenty/assets/images/2020-square-1/\" class=\"wp-image-38\"/></figure></li></ul></figure><!-- /wp:gallery -->', 'UMoMA открывает двери', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-12-01 12:11:56', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?page_id=6', 0, 'page', '', 0),
(7, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '<!-- wp:paragraph -->\n<p>Вы можете быть художником, который желает здесь представить себя и свои работы или представителем бизнеса с описанием миссии.</p>\n<!-- /wp:paragraph -->', 'О нас', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-12-01 12:11:56', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?page_id=7', 0, 'page', '', 0),
(8, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '<!-- wp:paragraph -->\n<p>Это страница с основной контактной информацией, такой как адрес и номер телефона. Вы также можете попробовать добавить форму контактов с помощью плагина.</p>\n<!-- /wp:paragraph -->', 'Контакты', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-12-01 12:11:56', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?page_id=8', 0, 'page', '', 0),
(9, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '', 'Блог', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2020-12-01 12:11:57', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?page_id=9', 0, 'page', '', 0),
(10, 1, '2020-12-01 12:11:57', '0000-00-00 00:00:00', '{\n    \"widget_text[2]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjEzOiLQniDRgdCw0LnRgtC1IjtzOjQ6InRleHQiO3M6MjA1OiLQl9C00LXRgdGMINC80L7QttC10YIg0LHRi9GC0Ywg0L7RgtC70LjRh9C90L7QtSDQvNC10YHRgtC+INC00LvRjyDRgtC+0LPQviwg0YfRgtC+0LHRiyDQv9GA0LXQtNGB0YLQsNCy0LjRgtGMINGB0LXQsdGPLCDRgdCy0L7QuSDRgdCw0LnRgiDQuNC70Lgg0LLRi9GA0LDQt9C40YLRjCDQutCw0LrQuNC1LdGC0L4g0LHQu9Cw0LPQvtC00LDRgNC90L7RgdGC0LguIjtzOjY6ImZpbHRlciI7YjoxO3M6NjoidmlzdWFsIjtiOjE7fQ==\",\n            \"title\": \"\\u041e \\u0441\\u0430\\u0439\\u0442\\u0435\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"80e24a68b34f3b73d1ad418f225f81ce\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"sidebars_widgets[sidebar-1]\": {\n        \"starter_content\": true,\n        \"value\": [\n            \"text-2\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"widget_text[3]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"encoded_serialized_instance\": \"YTo0OntzOjU6InRpdGxlIjtzOjIxOiLQndCw0LnQtNC40YLQtSDQvdCw0YEiO3M6NDoidGV4dCI7czoyMjY6IjxzdHJvbmc+0JDQtNGA0LXRgTwvc3Ryb25nPgoxMjMg0JzQtdC50L0g0YHRgtGA0LjRggrQndGM0Y4g0JnQvtGA0LosIE5ZIDEwMDAxCgo8c3Ryb25nPtCn0LDRgdGLPC9zdHJvbmc+CtCf0L7QvdC10LTQtdC70YzQvdC40LombWRhc2g70L/Rj9GC0L3QuNGG0LA6IDk6MDAmbmRhc2g7MTc6MDAK0KHRg9Cx0LHQvtGC0LAg0Lgg0LLQvtGB0LrRgNC10YHQtdC90YzQtTogMTE6MDAmbmRhc2g7MTU6MDAiO3M6NjoiZmlsdGVyIjtiOjE7czo2OiJ2aXN1YWwiO2I6MTt9\",\n            \"title\": \"\\u041d\\u0430\\u0439\\u0434\\u0438\\u0442\\u0435 \\u043d\\u0430\\u0441\",\n            \"is_widget_customizer_js_value\": true,\n            \"instance_hash_key\": \"70320e9379aaeb4931205199f31a4200\"\n        },\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"sidebars_widgets[sidebar-2]\": {\n        \"starter_content\": true,\n        \"value\": [\n            \"text-3\"\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menus_created_posts\": {\n        \"starter_content\": true,\n        \"value\": [\n            5,\n            6,\n            7,\n            8,\n            9\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu[-1]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"name\": \"\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u043e\\u0435\"\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-1]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"custom\",\n            \"title\": \"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430\",\n            \"url\": \"http://striksi.loc/\",\n            \"position\": 0,\n            \"nav_menu_term_id\": -1,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-2]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 7,\n            \"position\": 1,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"\\u041e \\u043d\\u0430\\u0441\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-3]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 9,\n            \"position\": 2,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"\\u0411\\u043b\\u043e\\u0433\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-4]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 8,\n            \"position\": 3,\n            \"nav_menu_term_id\": -1,\n            \"title\": \"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"twentytwenty::nav_menu_locations[primary]\": {\n        \"starter_content\": true,\n        \"value\": -1,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu[-5]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"name\": \"\\u041e\\u0441\\u043d\\u043e\\u0432\\u043d\\u043e\\u0435\"\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-5]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"custom\",\n            \"title\": \"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430\",\n            \"url\": \"http://striksi.loc/\",\n            \"position\": 0,\n            \"nav_menu_term_id\": -5,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-6]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 7,\n            \"position\": 1,\n            \"nav_menu_term_id\": -5,\n            \"title\": \"\\u041e \\u043d\\u0430\\u0441\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-7]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 9,\n            \"position\": 2,\n            \"nav_menu_term_id\": -5,\n            \"title\": \"\\u0411\\u043b\\u043e\\u0433\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-8]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"type\": \"post_type\",\n            \"object\": \"page\",\n            \"object_id\": 8,\n            \"position\": 3,\n            \"nav_menu_term_id\": -5,\n            \"title\": \"\\u041a\\u043e\\u043d\\u0442\\u0430\\u043a\\u0442\\u044b\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"twentytwenty::nav_menu_locations[expanded]\": {\n        \"starter_content\": true,\n        \"value\": -5,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu[-9]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"name\": \"\\u041c\\u0435\\u043d\\u044e \\u0441\\u043e\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0441\\u0441\\u044b\\u043b\\u043e\\u043a\"\n        },\n        \"type\": \"nav_menu\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-9]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Yelp\",\n            \"url\": \"https://www.yelp.com\",\n            \"position\": 0,\n            \"nav_menu_term_id\": -9,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-10]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Facebook\",\n            \"url\": \"https://www.facebook.com/wordpress\",\n            \"position\": 1,\n            \"nav_menu_term_id\": -9,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-11]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Twitter\",\n            \"url\": \"https://twitter.com/wordpress\",\n            \"position\": 2,\n            \"nav_menu_term_id\": -9,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-12]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Instagram\",\n            \"url\": \"https://www.instagram.com/explore/tags/wordcamp/\",\n            \"position\": 3,\n            \"nav_menu_term_id\": -9,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"nav_menu_item[-13]\": {\n        \"starter_content\": true,\n        \"value\": {\n            \"title\": \"Email\",\n            \"url\": \"mailto:wordpress@example.com\",\n            \"position\": 4,\n            \"nav_menu_term_id\": -9,\n            \"object_id\": 0\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"twentytwenty::nav_menu_locations[social]\": {\n        \"starter_content\": true,\n        \"value\": -9,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"show_on_front\": {\n        \"starter_content\": true,\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"page_on_front\": {\n        \"starter_content\": true,\n        \"value\": 6,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    },\n    \"page_for_posts\": {\n        \"starter_content\": true,\n        \"value\": 9,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-12-01 12:11:57\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '079ef1f0-48a8-4e83-90a5-372690f7763e', '', '', '2020-12-01 12:11:57', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?p=10', 0, 'customize_changeset', '', 0),
(12, 1, '2020-12-02 08:32:26', '2020-12-02 08:32:26', '', 'HOME', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2020-12-02 10:45:36', '2020-12-02 10:45:36', '', 0, 'http://striksi.loc/?page_id=12', 0, 'page', '', 0),
(13, 1, '2020-12-02 08:32:26', '2020-12-02 08:32:26', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 08:32:26', '2020-12-02 08:32:26', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2020-12-02 08:33:46', '2020-12-02 08:33:46', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://striksi.loc.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2020-12-02 08:33:46', '2020-12-02 08:33:46', '', 3, 'http://striksi.loc/2020/12/02/3-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2020-12-02 08:33:54', '2020-12-02 08:33:54', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://striksi.loc/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2020-12-02 08:33:54', '2020-12-02 08:33:54', '', 2, 'http://striksi.loc/2020/12/02/2-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2020-12-02 08:34:31', '2020-12-02 08:34:31', '', 'Японские технологии', '', 'publish', 'closed', 'closed', '', 'japan', '', '', '2020-12-03 09:34:40', '2020-12-03 09:34:40', '', 0, 'http://striksi.loc/?page_id=16', 0, 'page', '', 0),
(17, 1, '2020-12-02 08:34:31', '2020-12-02 08:34:31', '', 'Японские технологии', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-12-02 08:34:31', '2020-12-02 08:34:31', '', 16, 'http://striksi.loc/2020/12/02/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2020-12-02 08:35:12', '2020-12-02 08:35:12', '', 'Салоны', '', 'publish', 'closed', 'closed', '', 'salon', '', '', '2020-12-03 10:40:11', '2020-12-03 10:40:11', '', 0, 'http://striksi.loc/?page_id=18', 0, 'page', '', 0),
(19, 1, '2020-12-02 08:35:12', '2020-12-02 08:35:12', '', 'Салоны', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2020-12-02 08:35:12', '2020-12-02 08:35:12', '', 18, 'http://striksi.loc/2020/12/02/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2020-12-02 08:36:26', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-12-02 08:36:26', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?p=20', 1, 'nav_menu_item', '', 0),
(23, 1, '2020-12-02 08:41:34', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-12-02 08:41:34', '0000-00-00 00:00:00', '', 0, 'http://striksi.loc/?p=23', 1, 'nav_menu_item', '', 0),
(24, 1, '2020-12-02 08:42:00', '2020-12-02 08:42:00', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2020-12-03 13:02:18', '2020-12-03 13:02:18', '', 0, 'http://striksi.loc/?p=24', 2, 'nav_menu_item', '', 0),
(25, 1, '2020-12-02 08:41:59', '2020-12-02 08:41:59', ' ', '', '', 'publish', 'closed', 'closed', '', '25', '', '', '2020-12-03 13:02:18', '2020-12-03 13:02:18', '', 0, 'http://striksi.loc/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2020-12-02 08:49:01', '2020-12-02 08:49:01', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"12\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'HOME', 'home', 'publish', 'closed', 'closed', '', 'group_5fc754ce964ea', '', '', '2020-12-02 09:49:21', '2020-12-02 09:49:21', '', 0, 'http://striksi.loc/?post_type=acf-field-group&#038;p=26', 0, 'acf-field-group', '', 0),
(27, 1, '2020-12-02 08:49:01', '2020-12-02 08:49:01', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Фото (скриншоты) отзывов', 'screen_image_container', 'publish', 'closed', 'closed', '', 'field_5fc754d43546c', '', '', '2020-12-02 09:34:31', '2020-12-02 09:34:31', '', 26, 'http://striksi.loc/?post_type=acf-field&#038;p=27', 0, 'acf-field', '', 0),
(28, 1, '2020-12-02 08:49:02', '2020-12-02 08:49:02', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Фото', 'screen_images', 'publish', 'closed', 'closed', '', 'field_5fc754ea3546d', '', '', '2020-12-02 09:49:21', '2020-12-02 09:49:21', '', 27, 'http://striksi.loc/?post_type=acf-field&#038;p=28', 0, 'acf-field', '', 0),
(29, 1, '2020-12-02 08:50:54', '2020-12-02 08:50:54', '', 'slider-bg', '', 'inherit', 'open', 'closed', '', 'slider-bg', '', '', '2020-12-02 08:50:54', '2020-12-02 08:50:54', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/slider-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2020-12-02 09:39:27', '2020-12-02 09:39:27', '', '1revq', '', 'inherit', 'open', 'closed', '', '1revq', '', '', '2020-12-02 09:39:27', '2020-12-02 09:39:27', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/1revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2020-12-02 09:39:32', '2020-12-02 09:39:32', '', '2revq', '', 'inherit', 'open', 'closed', '', '2revq', '', '', '2020-12-02 09:39:32', '2020-12-02 09:39:32', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/2revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(32, 1, '2020-12-02 09:39:36', '2020-12-02 09:39:36', '', '3revq', '', 'inherit', 'open', 'closed', '', '3revq', '', '', '2020-12-02 09:39:36', '2020-12-02 09:39:36', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/3revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2020-12-02 09:39:41', '2020-12-02 09:39:41', '', '4revq', '', 'inherit', 'open', 'closed', '', '4revq', '', '', '2020-12-02 09:39:41', '2020-12-02 09:39:41', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/4revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(34, 1, '2020-12-02 09:39:45', '2020-12-02 09:39:45', '', '5revq', '', 'inherit', 'open', 'closed', '', '5revq', '', '', '2020-12-02 09:39:45', '2020-12-02 09:39:45', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/5revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2020-12-02 09:39:49', '2020-12-02 09:39:49', '', '6revq', '', 'inherit', 'open', 'closed', '', '6revq', '', '', '2020-12-02 09:39:49', '2020-12-02 09:39:49', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/6revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2020-12-02 09:39:53', '2020-12-02 09:39:53', '', '7revq', '', 'inherit', 'open', 'closed', '', '7revq', '', '', '2020-12-02 09:39:53', '2020-12-02 09:39:53', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/7revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 1, '2020-12-02 09:39:58', '2020-12-02 09:39:58', '', '8revq', '', 'inherit', 'open', 'closed', '', '8revq', '', '', '2020-12-02 09:39:58', '2020-12-02 09:39:58', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/8revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 1, '2020-12-02 09:40:04', '2020-12-02 09:40:04', '', '9revq', '', 'inherit', 'open', 'closed', '', '9revq', '', '', '2020-12-02 09:40:04', '2020-12-02 09:40:04', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/9revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2020-12-02 09:40:08', '2020-12-02 09:40:08', '', '10revq', '', 'inherit', 'open', 'closed', '', '10revq', '', '', '2020-12-02 09:40:08', '2020-12-02 09:40:08', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/10revq.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2020-12-02 09:40:12', '2020-12-02 09:40:12', '', 'map', '', 'inherit', 'open', 'closed', '', 'map', '', '', '2020-12-02 09:40:12', '2020-12-02 09:40:12', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/map.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2020-12-02 09:40:16', '2020-12-02 09:40:16', '', 'map2', '', 'inherit', 'open', 'closed', '', 'map2', '', '', '2020-12-02 09:40:16', '2020-12-02 09:40:16', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/map2.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2020-12-02 09:40:19', '2020-12-02 09:40:19', '', 'otzivview', '', 'inherit', 'open', 'closed', '', 'otzivview', '', '', '2020-12-02 09:40:19', '2020-12-02 09:40:19', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/otzivview.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2020-12-02 09:40:23', '2020-12-02 09:40:23', '', 'rev-1', '', 'inherit', 'open', 'closed', '', 'rev-1', '', '', '2020-12-02 09:40:23', '2020-12-02 09:40:23', '', 12, 'http://striksi.loc/wp-content/uploads/2020/12/rev-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(44, 1, '2020-12-02 09:41:45', '2020-12-02 09:41:45', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 09:41:45', '2020-12-02 09:41:45', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2020-12-02 09:50:03', '2020-12-02 09:50:03', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 09:50:03', '2020-12-02 09:50:03', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2020-12-02 10:04:37', '2020-12-02 10:04:37', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:04:37', '2020-12-02 10:04:37', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-12-02 10:39:48', '2020-12-02 10:39:48', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:39:48', '2020-12-02 10:39:48', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2020-12-02 10:40:22', '2020-12-02 10:40:22', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:40:22', '2020-12-02 10:40:22', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-12-02 10:42:42', '2020-12-02 10:42:42', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:42:42', '2020-12-02 10:42:42', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2020-12-02 10:44:01', '2020-12-02 10:44:01', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:44:01', '2020-12-02 10:44:01', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2020-12-02 10:45:36', '2020-12-02 10:45:36', '', 'HOME', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2020-12-02 10:45:36', '2020-12-02 10:45:36', '', 12, 'http://striksi.loc/2020/12/02/12-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2020-12-02 12:19:41', '2020-12-02 12:19:41', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"16\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Японские технологии', '%d1%8f%d0%bf%d0%be%d0%bd%d1%81%d0%ba%d0%b8%d0%b5-%d1%82%d0%b5%d1%85%d0%bd%d0%be%d0%bb%d0%be%d0%b3%d0%b8%d0%b8', 'publish', 'closed', 'closed', '', 'group_5fc784c4e9dfe', '', '', '2020-12-03 09:30:52', '2020-12-03 09:30:52', '', 0, 'http://striksi.loc/?post_type=acf-field-group&#038;p=52', 0, 'acf-field-group', '', 0),
(59, 1, '2020-12-02 12:49:57', '2020-12-02 12:49:57', '<!-- wp:paragraph -->\n<p>Вы сэкономите еще больше, потому что каждая шестая <br>стрижка — вам в подарок.</p>\n<!-- /wp:paragraph -->', 'Шестая стрижка в подарок', '', 'publish', 'closed', 'closed', '', '%d1%88%d0%b5%d1%81%d1%82%d0%b0%d1%8f-%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b0-%d0%b2-%d0%bf%d0%be%d0%b4%d0%b0%d1%80%d0%be%d0%ba', '', '', '2020-12-02 13:05:07', '2020-12-02 13:05:07', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=59', 0, 'yaponskie_tehnologii', '', 0),
(60, 1, '2020-12-02 13:05:59', '2020-12-02 13:05:59', '<!-- wp:paragraph -->\n<p>Перед каждой стрижкой мастер распечатывает новую расческу и использует ее во время стрижки, а в конце запаковывает обратно и дарит клиенту. Двойная выгода для вас: вы знаете, что инструмент чистый, и получаете новую расческу для ежедневного использования.</p>\n<!-- /wp:paragraph -->', 'Каждому клиенту новая расческа', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%b0%d0%b6%d0%b4%d0%be%d0%bc%d1%83-%d0%ba%d0%bb%d0%b8%d0%b5%d0%bd%d1%82%d1%83-%d0%bd%d0%be%d0%b2%d0%b0%d1%8f-%d1%80%d0%b0%d1%81%d1%87%d0%b5%d1%81%d0%ba%d0%b0', '', '', '2020-12-02 13:05:59', '2020-12-02 13:05:59', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=60', 0, 'yaponskie_tehnologii', '', 0),
(61, 1, '2020-12-02 13:06:43', '2020-12-02 13:06:43', '<!-- wp:paragraph -->\n<p>Фиксированная цена на все стрижки — даже сложные и модельные. Все просто — вы платите только 350 рублей.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>*Стоимость услуги может варьироваться в разных регионах.</p>\n<!-- /wp:paragraph -->', 'Все стрижки по одной цене', '', 'publish', 'closed', 'closed', '', '%d0%b2%d1%81%d0%b5-%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b8-%d0%bf%d0%be-%d0%be%d0%b4%d0%bd%d0%be%d0%b9-%d1%86%d0%b5%d0%bd%d0%b5', '', '', '2020-12-02 13:06:43', '2020-12-02 13:06:43', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=61', 0, 'yaponskie_tehnologii', '', 0),
(63, 1, '2020-12-03 08:54:30', '0000-00-00 00:00:00', '', 'Москва', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-12-03 08:54:30', '2020-12-03 08:54:30', '', 0, 'http://striksi.loc/?post_type=gorod&#038;p=63', 0, 'gorod', '', 0),
(64, 1, '2020-12-03 09:00:17', '2020-12-03 09:00:17', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"ulitsa\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Улицы', '%d1%83%d0%bb%d0%b8%d1%86%d1%8b', 'publish', 'closed', 'closed', '', 'group_5fc8a8c7b21eb', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 0, 'http://striksi.loc/?post_type=acf-field-group&#038;p=64', 0, 'acf-field-group', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(68, 1, '2020-12-03 09:14:54', '2020-12-03 09:14:54', '[yamap center=\"55.7473,37.6247\" height=\"22rem\" controls=\"\" zoom=\"12\" type=\"yandex#map\" scrollzoom=\"0\" mobiledrag=\"0\"][yaplacemark  coord=\"55.7458,37.6480\" icon=\"islands#dotIcon\" color=\"#1e98ff\"][/yamap]', 'Кутузовский проспект, 5/3', '', 'publish', 'closed', 'closed', '', '%d0%ba%d1%83%d1%82%d1%83%d0%b7%d0%be%d0%b2%d1%81%d0%ba%d0%b8%d0%b9-%d0%bf%d1%80%d0%be%d1%81%d0%bf%d0%b5%d0%ba%d1%82-5-3', '', '', '2020-12-03 15:05:45', '2020-12-03 15:05:45', '', 0, 'http://striksi.loc/?post_type=ulitsa&#038;p=68', 0, 'ulitsa', '', 0),
(69, 1, '2020-12-03 09:15:34', '2020-12-03 09:15:34', '[yamap center=\"55.7473,37.6247\" height=\"22rem\" controls=\"\" zoom=\"12\" type=\"yandex#map\" scrollzoom=\"0\" mobiledrag=\"0\"][yaplacemark  coord=\"55.7434,37.6498\" icon=\"islands#dotIcon\" color=\"#1e98ff\"][/yamap]', 'ул. Каланчёвская, 12с3', '', 'publish', 'closed', 'closed', '', '%d1%83%d0%bb-%d0%ba%d0%b0%d0%bb%d0%b0%d0%bd%d1%87%d1%91%d0%b2%d1%81%d0%ba%d0%b0%d1%8f-12%d1%813', '', '', '2020-12-03 12:12:04', '2020-12-03 12:12:04', '', 0, 'http://striksi.loc/?post_type=ulitsa&#038;p=69', 0, 'ulitsa', '', 0),
(70, 1, '2020-12-03 09:15:59', '2020-12-03 09:15:59', '[yamap center=\"55.7473,37.6247\" height=\"22rem\" controls=\"\" zoom=\"12\" type=\"yandex#map\" scrollzoom=\"0\" mobiledrag=\"0\"][yaplacemark  coord=\"55.7363,37.6707\" icon=\"islands#dotIcon\" color=\"#1e98ff\"][/yamap]', 'Сибирский проезд, 5', '', 'publish', 'closed', 'closed', '', '%d1%81%d0%b8%d0%b1%d0%b8%d1%80%d1%81%d0%ba%d0%b8%d0%b9-%d0%bf%d1%80%d0%be%d0%b5%d0%b7%d0%b4-5', '', '', '2020-12-03 12:53:42', '2020-12-03 12:53:42', '', 0, 'http://striksi.loc/?post_type=ulitsa&#038;p=70', 0, 'ulitsa', '', 0),
(71, 1, '2020-12-03 09:27:09', '2020-12-03 09:27:09', '<div class=\"info__stitle stitle stitle_sm\">Наши главные отличия от других парикмахерских:</div>\r\n<div class=\"info__text textb\">\r\n\r\n— Мы используем в работе одноразовые полотенца и воротнички;\r\n\r\n— У нас всегда чисто, потому что в парикмахерские шкафы встроена система мощных пылесосов, которые всасывают мусор с пола. Все, даже самые мелкие и незаметные глазу, остриженные волоски моментально затягиваются в специальный электросовок внизу шкафа. Мы не используем обычные совки для уборки волос и не складируем волосы в рабочем пространстве парикмахерской;\r\n\r\n— После каждого клиента инструменты отправляются на 3-хступенчатую обработку;\r\n\r\n— Мастера используют для работы перчатки. Это дополнительная защита для вас и парикмахера, которая защищает от порезов и заноз;\r\n\r\n— Мастер при вас откроет новую расческу и будет использовать ее во время стрижки. После вы получите расческу в подарок.\r\n\r\n</div>', 'Японские технологии', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-12-03 09:27:09', '2020-12-03 09:27:09', '', 16, 'http://striksi.loc/2020/12/03/16-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2020-12-03 09:30:51', '2020-12-03 09:30:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Заголовок', 'title', 'publish', 'closed', 'closed', '', 'field_5fc8b00300cc5', '', '', '2020-12-03 09:30:51', '2020-12-03 09:30:51', '', 52, 'http://striksi.loc/?post_type=acf-field&p=72', 0, 'acf-field', '', 0),
(73, 1, '2020-12-03 09:30:51', '2020-12-03 09:30:51', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;}', 'Контент', 'content', 'publish', 'closed', 'closed', '', 'field_5fc8b03000cc6', '', '', '2020-12-03 09:30:51', '2020-12-03 09:30:51', '', 52, 'http://striksi.loc/?post_type=acf-field&p=73', 1, 'acf-field', '', 0),
(74, 1, '2020-12-03 09:31:37', '2020-12-03 09:31:37', '', 'Японские технологии', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-12-03 09:31:37', '2020-12-03 09:31:37', '', 16, 'http://striksi.loc/2020/12/03/16-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2020-12-03 09:33:38', '2020-12-03 09:33:38', '', 'Японские технологии', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-12-03 09:33:38', '2020-12-03 09:33:38', '', 16, 'http://striksi.loc/2020/12/03/16-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2020-12-03 09:34:40', '2020-12-03 09:34:40', '', 'Японские технологии', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2020-12-03 09:34:40', '2020-12-03 09:34:40', '', 16, 'http://striksi.loc/2020/12/03/16-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2020-12-03 10:06:20', '2020-12-03 10:06:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Время Работы', 'time_work', 'publish', 'closed', 'closed', '', 'field_5fc8b7fe51510', '', '', '2020-12-03 10:06:20', '2020-12-03 10:06:20', '', 64, 'http://striksi.loc/?post_type=acf-field&p=77', 0, 'acf-field', '', 0),
(78, 1, '2020-12-03 10:06:20', '2020-12-03 10:06:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Номер Телефона', 'number_telephone', 'publish', 'closed', 'closed', '', 'field_5fc8b81951511', '', '', '2020-12-03 10:06:20', '2020-12-03 10:06:20', '', 64, 'http://striksi.loc/?post_type=acf-field&p=78', 1, 'acf-field', '', 0),
(81, 1, '2020-12-03 10:08:19', '2020-12-03 10:08:19', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Price list', 'price_list', 'publish', 'closed', 'closed', '', 'field_5fc8b8c9a389e', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 64, 'http://striksi.loc/?post_type=acf-field&#038;p=81', 4, 'acf-field', '', 0),
(82, 1, '2020-12-03 10:08:19', '2020-12-03 10:08:19', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title price', 'title_price', 'publish', 'closed', 'closed', '', 'field_5fc8b8eda389f', '', '', '2020-12-03 10:08:19', '2020-12-03 10:08:19', '', 81, 'http://striksi.loc/?post_type=acf-field&p=82', 0, 'acf-field', '', 0),
(83, 1, '2020-12-03 10:08:19', '2020-12-03 10:08:19', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'list', 'list', 'publish', 'closed', 'closed', '', 'field_5fc8b8ffa38a0', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 81, 'http://striksi.loc/?post_type=acf-field&#038;p=83', 1, 'acf-field', '', 0),
(84, 1, '2020-12-03 10:39:29', '2020-12-03 10:39:29', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"18\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Салоны', '%d1%81%d0%b0%d0%bb%d0%be%d0%bd%d1%8b', 'publish', 'closed', 'closed', '', 'group_5fc8c00eb11c4', '', '', '2020-12-03 10:41:16', '2020-12-03 10:41:16', '', 0, 'http://striksi.loc/?post_type=acf-field-group&#038;p=84', 0, 'acf-field-group', '', 0),
(85, 1, '2020-12-03 10:39:29', '2020-12-03 10:39:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Салон Заголовок', 'salon_title', 'publish', 'closed', 'closed', '', 'field_5fc8c0245f98e', '', '', '2020-12-03 10:41:16', '2020-12-03 10:41:16', '', 84, 'http://striksi.loc/?post_type=acf-field&#038;p=85', 0, 'acf-field', '', 0),
(86, 1, '2020-12-03 10:40:11', '2020-12-03 10:40:11', '', 'Салоны', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2020-12-03 10:40:11', '2020-12-03 10:40:11', '', 18, 'http://striksi.loc/2020/12/03/18-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2020-12-03 11:09:02', '2020-12-03 11:09:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'link vk', 'link_vk', 'publish', 'closed', 'closed', '', 'field_5fc8c73893a25', '', '', '2020-12-03 11:13:21', '2020-12-03 11:13:21', '', 64, 'http://striksi.loc/?post_type=acf-field&#038;p=88', 2, 'acf-field', '', 0),
(89, 1, '2020-12-03 11:09:02', '2020-12-03 11:09:02', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'link insta', 'link_insta', 'publish', 'closed', 'closed', '', 'field_5fc8c74493a26', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 64, 'http://striksi.loc/?post_type=acf-field&#038;p=89', 3, 'acf-field', '', 0),
(90, 1, '2020-12-03 11:13:22', '2020-12-03 11:13:22', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'price name', 'price_name', 'publish', 'closed', 'closed', '', 'field_5fc8c83d0ee90', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 83, 'http://striksi.loc/?post_type=acf-field&p=90', 0, 'acf-field', '', 0),
(91, 1, '2020-12-03 11:13:22', '2020-12-03 11:13:22', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'price', 'price', 'publish', 'closed', 'closed', '', 'field_5fc8c8480ee91', '', '', '2020-12-03 11:13:22', '2020-12-03 11:13:22', '', 83, 'http://striksi.loc/?post_type=acf-field&p=91', 1, 'acf-field', '', 0),
(92, 1, '2020-12-03 11:39:10', '2020-12-03 11:39:10', 'Мы экономим не только ваши деньги, но и время. Сочетание японских технологий и эргономичности рабочего места мастера позволяют нам это сделать.', 'Стрижки от 15 минут', '', 'publish', 'closed', 'closed', '', '%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b8-%d0%be%d1%82-15-%d0%bc%d0%b8%d0%bd%d1%83%d1%82', '', '', '2020-12-03 11:39:10', '2020-12-03 11:39:10', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=92', 0, 'yaponskie_tehnologii', '', 0),
(93, 1, '2020-12-03 11:39:37', '2020-12-03 11:39:37', 'Во всех наших салонах инструменты проходят трехступенчатую очистку, включая финальную стерилизацию ультрафиолетом. Он абсолютно безопасен для вашего здоровья, но исключительно хорошо борется с вредоносными микробами.\r\n<h5>Вы можете быть уверены в чистоте наших инструментов.</h5>', 'Гарантия чистоты', '', 'publish', 'closed', 'closed', '', '%d0%b3%d0%b0%d1%80%d0%b0%d0%bd%d1%82%d0%b8%d1%8f-%d1%87%d0%b8%d1%81%d1%82%d0%be%d1%82%d1%8b', '', '', '2020-12-03 11:39:37', '2020-12-03 11:39:37', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=93', 0, 'yaponskie_tehnologii', '', 0),
(94, 1, '2020-12-03 11:40:04', '2020-12-03 11:40:04', 'Кроме мойки головы водой с шампунем мы используем воздушную мойку с помощью парикмахерского мощного пылесоса. Воздушная мойка распространена в Японии и Европе, такой мойкой убирать волосы в три раза быстрее водяной – это позволяет держать нам низкие цены при высоком качестве работы.', 'Воздушная мойка головы', '', 'publish', 'closed', 'closed', '', '%d0%b2%d0%be%d0%b7%d0%b4%d1%83%d1%88%d0%bd%d0%b0%d1%8f-%d0%bc%d0%be%d0%b9%d0%ba%d0%b0-%d0%b3%d0%be%d0%bb%d0%be%d0%b2%d1%8b', '', '', '2020-12-03 11:40:04', '2020-12-03 11:40:04', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=94', 0, 'yaponskie_tehnologii', '', 0),
(95, 1, '2020-12-03 11:40:26', '2020-12-03 11:40:26', 'Можно не записываться к нашим мастерам заранее. Технологии работы парикмахерской позволяют обслуживать клиентов в порядке очереди.\r\n<h5>Решили подстричься? Заходите, у нас вам всегда рады!</h5>', 'Без записи', '', 'publish', 'closed', 'closed', '', '%d0%b1%d0%b5%d0%b7-%d0%b7%d0%b0%d0%bf%d0%b8%d1%81%d0%b8', '', '', '2020-12-03 11:40:26', '2020-12-03 11:40:26', '', 0, 'http://striksi.loc/?post_type=yaponskie_tehnologii&#038;p=95', 0, 'yaponskie_tehnologii', '', 0),
(96, 1, '2020-12-03 12:50:37', '2020-12-03 12:50:37', '[yamap center=\"55.7473,37.6247\" height=\"22rem\" controls=\"\" zoom=\"12\" type=\"yandex#map\" scrollzoom=\"0\" mobiledrag=\"0\"][yaplacemark  coord=\"55.7363,37.6707\" icon=\"islands#dotIcon\" color=\"#1e98ff\"][/yamap]', 'Сибирский проезд, 5', '', 'inherit', 'closed', 'closed', '', '70-autosave-v1', '', '', '2020-12-03 12:50:37', '2020-12-03 12:50:37', '', 70, 'http://striksi.loc/2020/12/03/70-autosave-v1/', 0, 'revision', '', 0),
(97, 1, '2020-12-03 12:57:07', '2020-12-03 12:57:07', '', 'ул. Адоратского, 21а', '', 'publish', 'closed', 'closed', '', '%d1%83%d0%bb-%d0%b0%d0%b4%d0%be%d1%80%d0%b0%d1%82%d1%81%d0%ba%d0%be%d0%b3%d0%be-21%d0%b0', '', '', '2020-12-03 12:57:07', '2020-12-03 12:57:07', '', 0, 'http://striksi.loc/?post_type=ulitsa&#038;p=97', 0, 'ulitsa', '', 0),
(98, 1, '2020-12-03 12:58:30', '2020-12-03 12:58:30', '', 'Политика конфиденциальности', '', 'publish', 'closed', 'closed', '', 'politika', '', '', '2020-12-03 13:13:13', '2020-12-03 13:13:13', '', 0, 'http://striksi.loc/?page_id=98', 0, 'page', '', 0),
(99, 1, '2020-12-03 12:58:30', '2020-12-03 12:58:30', '', 'ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2020-12-03 12:58:30', '2020-12-03 12:58:30', '', 98, 'http://striksi.loc/2020/12/03/98-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2020-12-03 13:03:41', '2020-12-03 13:03:41', ' ', '', '', 'publish', 'closed', 'closed', '', '100', '', '', '2020-12-03 13:03:41', '2020-12-03 13:03:41', '', 0, 'http://striksi.loc/?p=100', 1, 'nav_menu_item', '', 0),
(101, 1, '2020-12-03 13:13:13', '2020-12-03 13:13:13', '', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2020-12-03 13:13:13', '2020-12-03 13:13:13', '', 98, 'http://striksi.loc/2020/12/03/98-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(3, 'Primary-menu', 'primary-menu', 0),
(4, 'Кутузовский проспект', '%d0%ba%d1%83%d1%82%d1%83%d0%b7%d0%be%d0%b2%d1%81%d0%ba%d0%b8%d0%b9-%d0%bf%d1%80%d0%be%d1%81%d0%bf%d0%b5%d0%ba%d1%82', 0),
(5, '5/3', '5-3', 0),
(6, 'ул. Каланчёвская', '%d1%83%d0%bb-%d0%ba%d0%b0%d0%bb%d0%b0%d0%bd%d1%87%d1%91%d0%b2%d1%81%d0%ba%d0%b0%d1%8f', 0),
(7, 'Москва', '%d0%bc%d0%be%d1%81%d0%ba%d0%b2%d0%b0', 0),
(8, 'Пермь', '%d0%bf%d0%b5%d1%80%d0%bc%d1%8c', 0),
(9, 'Челябинск', '%d1%87%d0%b5%d0%bb%d1%8f%d0%b1%d0%b8%d0%bd%d1%81%d0%ba', 0),
(10, 'Ростов', '%d1%80%d0%be%d1%81%d1%82%d0%be%d0%b2', 0),
(11, 'Мурманск', '%d0%bc%d1%83%d1%80%d0%bc%d0%b0%d0%bd%d1%81%d0%ba', 0),
(12, 'footer-menu', 'footer-menu', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(24, 3, 0),
(25, 3, 0),
(68, 7, 0),
(69, 7, 0),
(70, 7, 0),
(97, 8, 0),
(100, 12, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(3, 3, 'nav_menu', '', 0, 2),
(4, 4, 'ulitsa', '', 0, 0),
(5, 5, 'ulitsa', '', 0, 0),
(6, 6, 'ulitsa', '', 0, 0),
(7, 7, 'gorod', '', 0, 3),
(8, 8, 'gorod', '', 0, 1),
(9, 9, 'gorod', '', 0, 0),
(10, 10, 'gorod', '', 0, 0),
(11, 11, 'gorod', '', 0, 0),
(12, 12, 'nav_menu', '', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"838cc03179a3718da027b03c92312a41cb32e56181d1fa37f59b421a6746138c\";a:4:{s:10:\"expiration\";i:1608033350;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36\";s:5:\"login\";i:1606823750;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(21, 1, 'wp_user-settings', 'libraryContent=upload&editor=tinymce'),
(22, 1, 'wp_user-settings-time', '1606993335'),
(23, 1, 'nav_menu_recently_edited', '12');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BVdf3VeND6RBX9upDGUQrr6LTPa5b60', 'admin', 'zafar.abdullaev.00@bk.ru', 'http://striksi.loc', '2020-12-01 11:55:24', '', 0, 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Индексы таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT для таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=520;

--
-- AUTO_INCREMENT для таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT для таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
