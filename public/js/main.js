		
$('.citis-selectpicker').selectpicker();

$('.subscribe_email').on('keyup', function() {
   const label = document.querySelector('.btn-subscribe_placeholder')
   var input = $(this);
   if(input.val().length === 0) {
    $(label).addClass('empty');
} else {
    $(label).removeClass('empty');
}
});
$('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
});
$(document).ready(function() {
    $(".galery a").attr("data-fancybox","mygalery");
    $(".galery a").each(function(){
        $(this).attr("data-caption", $(this).find("img").attr("alt"));
        $(this).attr("title", $(this).find("img").attr("alt"));
    });

    $(".galery a").fancybox();
});
$('.owl-carousel-galery').owlCarousel({
    margin:40,
    nav:true,
    autoWidth:true,
    autoplayTimeout:2000,
    responsive:{
        0:{
            items:1,
            center:true,
            loop:true,
        },
        600:{
            items:1,
            center:true,
            loop:true,

        },
        1367:{
            items:6
        }
    }
})
$('.owl-carousel-travel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        992:{
            items:1
        },
        1000:{
            items:3
        }
    }
})

