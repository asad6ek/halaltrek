<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 */
?>

@extends('admin.app')
@section('title') Dashboard admin create @endsection
@section('content')
	@include('admin.admin.form',['user'=>$user,'method'=>'post', 'route'=>route('admin.admin.store')])
@endsection
