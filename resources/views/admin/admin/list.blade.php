@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<a href="{{route('admin.admin.create')}}" class="btn btn-success">Create</a>
				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>User</th>
							<th>Email</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($users as $key => $user)
							<tr>
								<td>{{  $users->firstItem() +$key }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>

								<td>
									<a href="{{ route('admin.admin.edit',$user->id) }}" class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									<form action="{{ route('admin.admin.delete',['admin'=>$user->id]) }}" method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
                    {!! $users->links() !!}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
