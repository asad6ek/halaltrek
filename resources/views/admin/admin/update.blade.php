<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard admin update @endsection
@section('content')
	@include('admin.admin.form',['user'=>$user,'method'=>'put','route'=>route('admin.admin.update',['admin'=>$user->id])])
@endsection
