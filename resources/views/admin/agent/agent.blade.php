@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<a href="{{route('admin.agent.create')}}" class="btn btn-success">Create</a>
				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>User</th>
							<th>Email</th>
							<th>Status</th>
							<th>Telephone</th>
							<th>Atol Number</th>
						</tr>
						</thead>
						<tbody>
						@foreach($agents as $key => $agent)
							<tr>
								<td>{{ $agents->firstItem() +$key}}</td>
								<td>{{ $agent->name }}</td>
								<td>{{ $agent->email }}</td>
								<td><span class="tag tag-success">
                      @if($agent->approved == 1)
											<span class="badge bg-blue">Approved</span>
										@else
											<span class="badge bg-pink">Pending</span>
										@endif
                      </span></td>
								<td>{{ $agent->telephone }}</td>
								<td>{{ $agent->atol_number }}</td>
								<td>
									<a href="{{ route('admin.agent.edit',$agent->id) }}" class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									<a href="{{ route('admin.agent.packages.index',$agent->id) }}" class="btn btn-info waves-effect">
										<i class="material-icons">Packages</i>
									</a>
									<form action="{{ route('admin.agent.delete', $agent->id) }}" method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
                    {{ $agents->links() }}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
