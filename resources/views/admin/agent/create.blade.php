<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 */
?>

@extends('admin.app')
@section('title') Dashboard agent create @endsection
@section('content')
	@include('admin.agent.form',['agent'=>$agent,'method'=>'post', 'route'=>route('admin.agent.store')])
@endsection
