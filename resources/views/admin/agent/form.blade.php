<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 * @var $agent \App\Models\Agent
 * @var $route string
 * @var $method string
 */
?>

<form action="{{$route}}" method="post" enctype="multipart/form-data">
	@include('partials.formErrors')
	@csrf
	@method($method)
	<div class="row">
		<div class="col-md-6">
			<div class="card card-primary">
				<div class="card-header">
					<h3 class="card-title">Required</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						        title="Collapse">
							<i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="form-group">
							<label for="inputLogo">Logo</label>
							<input type="file" id="inputLogo"  name="file">
					</div>
					<div class="form-group">
						<label for="inputName">Name</label>
						<input type="text" id="inputName" class="form-control" name="name"
						       value="{{old('name',$agent->name)}}">
					</div>
					<div class="form-group">
						<label for="inputAddress">Address</label>
						<input type="text" id="inputAddress" class="form-control" name="address"
						       value="{{old('address', $agent->address)}}">
					</div>
					<div class="form-group">
						<label for="inputAddress">Post code</label>
						<input type="text" id="inputAddress" class="form-control" name="postcode"
						       value="{{old('postcode',$agent->postcode)}}">
					</div>
					<div class="form-group">
						<label for="inputTelephone">Telephone</label>
						<input type="text" id="inputTelephone" class="form-control" name="telephone"
						       value="{{old('telephone',$agent->telephone)}}">
					</div>

					<div class="form-group">
						<label for="inputEmail">E-mail</label>
						<input type="email" id="inputEmail" class="form-control" name="email"
						       value="{{old('email',$agent->email)}}">
					</div>

					<div class="form-group">
						<label for="inputAtolNumber">Mobile</label>
						<input type="text" id="inputAtolNumber" class="form-control" name="mobile"
						       value="{{old('mobile',$agent->mobile)}}">
					</div>
					<div class="form-group">
						<label for="inputAtolNumber">Website</label>
						<input type="text" id="inputAtolNumber" class="form-control" name="website"
						       value="{{old('website',$agent->website)}}">
					</div>
					<div class="form-group">
						<label for="inputAtolNumber">Town</label>
						<input type="text" id="inputAtolNumber" class="form-control" name="town"
						       value="{{old('town',$agent->town)}}">
					</div>
					<div class="form-group">
						<label for="inputAtolNumber">Registered with Saudi Hajj Ministry</label>
						{{  Form::select(
							'registered_with_ministry',
							[
								1=>'Yes',
								0=>'No'
							],
							old('registered_with_ministry', $agent->registered_with_ministry),
							['placeholder' => 'Choose','class'=>'form-control']) }}
					</div>




				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
		<div class="col-md-6">
			<div class="card card-secondary">
				<div class="card-header">
					<h3 class="card-title">Optional</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						        title="Collapse">
							<i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="inputManagerName">Manager name</label>
						<input type="text" id="inputManagerName" class="form-control" name="manager_name"
						       value="{{old('manager_name',$agent->manager_name)}}">
					</div>
					<div class="form-group">

						<label for="inputAtolNumber">Atol Number</label>
						<input type="text" id="inputAtolNumber" class="form-control" name="atol_number"
						       value="{{old('atol_number',$agent->atol_number)}}">
					</div>
					<div class="form-group">
						<label for="inputInfo">Info</label>
						<textarea  id="inputInfo" class="form-control" name="info"
						>{{old('info',$agent->info)}}</textarea>
					</div>
					<div class="form-group">
						<label for="inputMinistryApproved">Ministry Approved</label>
						<input type="checkbox" id="inputMinistryApproved" name="ministry_approved" value="1"
						       @if(old('ministry_approved',$agent->ministry_approved) == 1) checked @endif
						>
					</div>
					<div class="form-group">
						<label for="inputApproved">Approved</label>
						<input type="checkbox" id="inputApproved" name="approved" value="1"
						       @if(old('approved',$agent->approved) == 1) checked @endif
						>
					</div>
				</div>
				<!-- /.card-body -->
			</div>

			<div class="card card-secondary">
				<div class="card-header">
					<h3 class="card-title">Password</h3>

					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
						        title="Collapse">
							<i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label for="password">Password</label>
						<input type="text" id="password" class="form-control" name="password">
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<input type="submit" value="{{ $agent->id?'Update':'Create' }}" class="btn btn-success">
			<a href="{{route('admin.agent.index')}}" class="btn btn-secondary">Cancel</a>
		</div>
	</div>
</form>


@push('scripts')


	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond.css')}}">
	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.css')}}">
	{{--<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-image-edit.min.css')}}">--}}
	<script src="{{asset('backend/plugins/filepond/filepond.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-type-validator.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-edit.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-resize.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filePond-image-transform.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-exif-orintation.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-crop.min.js')}}"></script>
	<script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        FilePond.setOptions({
            server: {
                process: './process',
                revert: './revert',
                restore: './restore/',
                load: './load/',
                fetch: './fetch/',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Select the file input and use
        // create() to turn it into a pond
        var pond = FilePond.create(
            document.getElementById('inputLogo'),
            {
                files: [
						@if($agent->getLogoPond())
                    {
                        source: "{{$agent->getLogoPond()}}",
                        options: {
                            type: 'local'
                        }
                    }
					@endif
                ],
                imagePreviewHeight: 100,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 200,
                imageResizeTargetHeight: 200,
                // stylePanelLayout: 'compact circle',
                styleLoadIndicatorPosition: 'center bottom',
                styleProgressIndicatorPosition: 'right bottom',
                styleButtonRemoveItemPosition: 'left bottom',
                styleButtonProcessItemPosition: 'right bottom',

                // Use Doka.js as image editor
                // imageEditEditor: Doka.create({
                //     utils: ['crop', 'filter', 'color']
                // })
            }
        );

        $('#fform').on('submit', function (e) {
            console.log(pond.status);
            if ([1, 4].indexOf(pond.status) === -1) {
                return false;
            }
            return true;
        });
	</script>
@endpush
