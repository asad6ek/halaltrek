<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard agent update @endsection
@section('content')
	@include('admin.agent.form',['agent'=>$agent,'method'=>'put','route'=>route('admin.agent.update',['agent'=>$agent->id])])
@endsection
