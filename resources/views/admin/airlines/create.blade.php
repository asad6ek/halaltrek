<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 */
?>

@extends('admin.app')
@section('title') Dashboard Air line create @endsection
@section('content')
	@include('admin::airlines.form',['model'=>$model,'method'=>'post', 'route'=>route('admin.airlines.store')])
@endsection
