<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 * @var $user \App\Models\Admin
 * @var $route string
 * @var $method string
 * @var $model \App\Models\Airline
 */
?>

<form action="{{$route}}" method="post" enctype="multipart/form-data" id="fform">
	@include('partials.formErrors')
	@csrf
	@method($method)
	<div class="row">
		<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-body">
					<div class="form-group image-pond-a">
						<label for="inputLogo">Logo</label>
						<input type="file" id="inputLogo" name="logo">
					</div>
					<div class="form-group">
						<label for="inputName">Name</label>
						<input type="text" id="inputName" class="form-control" name="name" value="{{$model->name}}">
					</div>
					<div class="form-group">
						<label for="inputInfo">Info</label>
						<textarea id="inputInfo" class="form-control" name="info" cols="30"
						          rows="10">{{$model->info}}</textarea>
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<input type="submit" value="Save" class="btn btn-success">
			<a href="{{route('admin.airlines.index')}}" class="btn btn-secondary">Cancel</a>
		</div>
	</div>
</form>

@push('scripts')
	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond.css')}}">
	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.css')}}">
	{{--<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-image-edit.min.css')}}">--}}
	<script src="{{asset('backend/plugins/filepond/filepond.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-type-validator.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-edit.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-resize.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filePond-image-transform.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-exif-orintation.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-crop.min.js')}}"></script>
	<script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        FilePond.setOptions({
            server: {
                process: './process',
                revert: './revert',
                restore: './restore/',
                load: './load/',
                fetch: './fetch/',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Select the file input and use
        // create() to turn it into a pond
        var pond = FilePond.create(
            document.getElementById('inputLogo'),
            {
                files: [
						@if($model->getLogoPond())
                    {
                        source: "{{$model->getLogoPond()}}",
                        options: {
                            type: 'local'
                        }
                    }
					@endif
                ],
                imagePreviewHeight: 100,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 50,
                imageResizeTargetHeight: 50,
                // stylePanelLayout: 'compact circle',
                styleLoadIndicatorPosition: 'center bottom',
                styleProgressIndicatorPosition: 'right bottom',
                styleButtonRemoveItemPosition: 'left bottom',
                styleButtonProcessItemPosition: 'right bottom',

                // Use Doka.js as image editor
                // imageEditEditor: Doka.create({
                //     utils: ['crop', 'filter', 'color']
                // })
            }
        );

        $('#fform').on('submit', function (e) {
            console.log(pond.status);
            if ([1, 4].indexOf(pond.status) === -1) {
                return false;
            }
            return true;
        });
	</script>
@endpush
