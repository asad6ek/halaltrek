@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<a href="{{route('admin.airlines.create')}}" class="btn btn-success">Create</a>
				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key=>$model)
							<tr>
								<td>{{  $models->firstItem() +$key }}</td>
								<td>{{ $model->name }}</td>

								<td>
									<a href="{{ route('admin.airlines.edit',$model->id) }}"
									   class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									<form action="{{ route('admin.airlines.delete',$model->id) }}"
									      method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $models->links() }}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
