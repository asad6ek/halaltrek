<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard City update @endsection
@section('content')
	@include('admin::airlines.form',['model'=>$model,'method'=>'put','route'=>route('admin.airlines.update',$model->id)])
@endsection
