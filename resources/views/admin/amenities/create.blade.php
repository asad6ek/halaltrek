@extends('admin.app')
@section('title') Dashboard admin create @endsection
@section('content')
	@include('admin::amenities.form',['model'=>$model,'method'=>'post', 'route'=>route('admin.amenities.store')])
@endsection
