<form action="{{$route}}" method="post" enctype="multipart/form-data" id="fform">
    @include('partials.formErrors')
    @csrf
    @method($method)
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-body">

                    {{--					<div class="form-group">--}}
                    {{--						<label for="inputName">Name</label>--}}
                    {{--						<input type="text" id="inputName" class="form-control" name="name"--}}
                    {{--						       value="{{old('name',$model->name)}}">--}}
                    {{--					</div>--}}

                    <div class="form-group">
                        <label for="inputInfo">Title</label>
                        <input id="inputInfo" class="form-control" name="title" value="{{$model->title}}">
                    </div>

                    <div class="row">

                        <div class="col-3">
                            <input type="radio" id="img_1" name="label" value="images/Layer_280859 (1).png"
                                   @if($model->label === 'images/Layer_280859 (1).png')checked @endif>
                            <label for="img_1"><img src="{{asset("images/Layer_280859 (1).png")}}" alt=""> </label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_2" name="label" value="images/Layer_280861.png"
                                   @if($model->label === 'images/Layer_280861.png')checked @endif>
                            <label for="img_2"> <img src="{{asset("images/Layer_280861.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_3" name="label" value="images/Layer_280863.png"
                                   @if($model->label === 'images/Layer_280863.png')checked @endif>
                            <label for="img_3"> <img src="{{asset("images/Layer_280863.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_4" name="label" value="images/Layer_280897.png"
                                   @if($model->label === 'images/Layer_280897.png')checked @endif>
                            <label for="img_4"> <img src="{{asset("images/Layer_280897.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_5" name="label" value="images/Layer_280898.png"
                                   @if($model->label === 'images/Layer_280898.png')checked @endif>
                            <label for="img_5"> <img src="{{asset("images/Layer_280898.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_6" name="label" value="images/Layer_280901.png"
                                   @if($model->label === 'images/Layer_280901.png')checked @endif>
                            <label for="img_6"> <img src="{{asset("images/Layer_280901.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_7" name="label" value="images/Layer_280906.png"
                                   @if($model->label === 'images/Layer_280906.png')checked @endif>
                            <label for="img_7"> <img src="{{asset("images/Layer_280906.png")}}" alt=""></label>
                        </div>
                        <div class="col-3">
                            <input type="radio" id="img_8" name="label" value="images/Layer_280914.png"
                                   @if($model->label === 'images/Layer_280914.png')checked @endif>
                            <label for="img_8"> <img src="{{asset("images/Layer_280914.png")}}" alt=""></label>
                        </div>

                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input type="submit" value="Save" class="btn btn-success">
            <a href="{{route('admin.amenities.index')}}" class="btn btn-secondary">Cancel</a>
        </div>
    </div>
</form>



@push('scripts')
    <link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.css')}}">
    <script src="{{asset('backend/plugins/filepond/filepond.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>

    <script src="{{asset('backend/plugins/filepond/filepond.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-type-validator.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-edit.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-resize.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filePond-image-transform.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-exif-orintation.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-crop.min.js')}}"></script>
    <script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        FilePond.setOptions({
            server: {
                process: './process',
                revert: './revert',
                restore: './restore/',
                load: './load/',
                fetch: './fetch/',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Select the file input and use
        // create() to turn it into a pond

        $('#fform').on('submit', function (e) {
            console.log(pond.status);
            if ([1, 4].indexOf(pond.status) === -1) {
                return false;
            }
            return true;
        });
    </script>
@endpush
