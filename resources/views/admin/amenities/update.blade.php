@extends('admin.app')
@section('title') Dashboard City update @endsection
@section('content')
	@include('admin::amenities.form',['model'=>$model,'method'=>'put','route'=>route('admin.amenities.update',['amenity'=>$model->id])])
@endsection
