@extends('templates.admin.app')


@section('body')
    @stack('img_css')
    @include('admin::sidebar')
    <div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">

        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @include('partials.AlertComponent')

            @yield('content')

        </div>
    </section>
</div>

@stack('img_script')
@endsection()
