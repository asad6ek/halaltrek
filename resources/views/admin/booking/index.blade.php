@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<!-- /.card-header -->

                    <div class="card-header d-flex p-0">
                        <h3 class="card-title p-3">Type:</h3>
                        <ul class="nav nav-pills ml-auto p-3">
                            <li class="nav-item"><a class="nav-link   active " href="#tab_1"
                                                    data-toggle="tab">New</a></li>
                            <li class="nav-item"><a class="nav-link  " href="#tab_2" data-toggle="tab">Complete</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">

                        <div class="tab-content">

                            <div class="tab-pane   active " id="tab_1">

                                <div class="container-fluid">
                                    <div class="package__list">
                                        <div class="card-body table-responsive p-0">
                                            <table class="table table-hover text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Booking type</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>address</th>
                                                    <th>Contact</th>
                                                    <th>Created at</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($new as $key => $model)

                                                    <tr>
                                                        <td>{{  $new->firstItem() +$key }}</td>
                                                        <td>{{ $model->getType() }}</td>
                                                        <td>{{ optional($model->mainContract)->name }}</td>
                                                        <td>{{ optional($model->mainContract)->email }}</td>
                                                        <td>{{ optional($model->mainContract)->address }}</td>
                                                        <td>{{ optional($model->mainContract)->contact }}</td>
                                                        <td>{{ Carbon\Carbon::parse($model->created_at)->format('jS M Y') }}</td>
                                                        <td>
                                                            <a href="{{ route('admin.booking.show',$model->id) }}"
                                                               class="btn btn-warning waves-effect">
                                                                <i class="material-icons">Info</i>
                                                            </a>

                                                            <form action="{{ route('admin.booking.delete',$model->id) }}"
                                                                  method="POST">
                                                                @csrf
                                                                @method('delete')
                                                                <input type="submit" value="Delete" class="btn btn-danger">
                                                            </form>
                                                            <form action="{{ route('admin.booking.update_status',$model->id) }}"
                                                                  method="POST">
                                                                @csrf
                                                                @method('POST')
                                                                <input type="submit" value="Complete" class="btn btn-success">
                                                            </form>
                                                        </td>
                                                    </tr>

                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{ $new->links() }}
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <!-- /.tab-pane -->
                            <div class="tab-pane  " id="tab_2">

                                <div class="container-fluid">
                                    <div class="package__list">
                                        <div class="card-body table-responsive p-0">
                                            <table class="table table-hover text-nowrap">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Booking type</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>address</th>
                                                    <th>Contact</th>
                                                    <th>Created at</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($complete as $key => $model)
                                                    <tr>
                                                        <td>{{  $complete->firstItem() + $key }}</td>
                                                        <td>{{ $model->getType() }}</td>
                                                        <td>{{ optional($model->mainContract)->name }}</td>
                                                        <td>{{ optional($model->mainContract)->email }}</td>
                                                        <td>{{ optional($model->mainContract)->address }}</td>
                                                        <td>{{ optional($model->mainContract)->contact }}</td>
                                                        <td>{{ Carbon\Carbon::parse($model->created_at)->format('jS M Y') }}</td>


                                                        <td>
                                                            <a href="{{ route('admin.booking.show',$model->id) }}"
                                                               class="btn btn-warning waves-effect">
                                                                <i class="material-icons">Info</i>
                                                            </a>

                                                            <form action="{{ route('admin.booking.delete',$model->id) }}"
                                                                  method="POST">
                                                                @csrf
                                                                @method('delete')
                                                                <input type="submit" value="Delete" class="btn btn-danger">
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{ $complete->links() }}
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- /.tab-pane -->


                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->




                <!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
