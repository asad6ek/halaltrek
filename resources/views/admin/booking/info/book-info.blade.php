<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:40
 */

$fields = [
    'booker_name',
    'room_size',
    'special_req',
];
?>


@include('components.detailed-view',['model'=>$model, 'fields'=>$fields])

