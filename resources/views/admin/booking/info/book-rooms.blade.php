<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-29
 * Time: 17:00
 */

$roomFields = [
    'size',
    'number',
];


?>

<table class="table ">
	<thead>
	<tr>
		<th>Room size</th>
		<th>Number of rooms</th>
	</tr>
	</thead>
	<tbody>
	@if(is_array($model))
		@foreach($model as $room)
			@if(!data_get($room,'number'))
				@continue
			@endif

			<tr>
				<td>
					{{data_get($room,'size')}}
				</td>
				<td>
					{{data_get($room,'number')}}
				</td>
			</tr>
		@endforeach
	@endif
	</tbody>
</table>
