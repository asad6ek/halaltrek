<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:45
 */
$fields = [
    'nights_madinah',
    'nights_makkah',
    'depart',
    'return',
    'comment',
    'Would you like private transfers?' => 'transfers|bool',
];
?>


@include('components.detailed-view',['fields'=>$fields, 'model'=>$model])
