<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-22
 * Time: 18:56
 */
?>
@if($model)
	<table class="table">
		<tr>
			<td>
				Do you have a specific flight you would like?
			</td>
			<td>
                <?= data_get($model, 'has_flight') ? 'Yes' : 'No' ?>
			</td>
		</tr>

		@if(!data_get($model, 'has_flight'))
			<tr>
				<td>
					Flight
				</td>
				<td>
                    <?= data_get(\App\Models\CustomQuote::flightTypes(), data_get($model, 'flight', '0')) ?>
				</td>
			</tr>
		@else
			<tr>
				<td>
					Details
				</td>
				<td>
                    <?= data_get($model, 'detail') ?>
				</td>
			</tr>
		@endif
	</table>
@endif
