<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-22
 * Time: 18:57
 */
?>

@if($model)
	<table class="table">
		<tr>
			<td>
				Do you have a specific flight you would like?
			</td>
			<td>
                <?= data_get($model, 'has_hotels') ? 'Yes' : 'No' ?>
			</td>
		</tr>

		@if(!data_get($model, 'has_hotels'))
			<tr>
				<td>
					Flight
				</td>
				<td>
                    <?= data_get(\App\Models\CustomQuote::hotelTypes(), data_get($model, 'type', '0')) ?>
				</td>
			</tr>
		@else
			<tr>
				<td>
					Hotel Makkah
				</td>
				<td>
                    <?= \App\Models\Hotel::getHotelName(data_get($model, 'hotel_makkah')) ?>
				</td>
			</tr>

			<tr>
				<td>
					Hotel Madinah
				</td>
				<td>
                    <?= \App\Models\Hotel::getHotelName(data_get($model, 'hotel_madinah')) ?>
				</td>
			</tr>
		@endif
	</table>
@endif
