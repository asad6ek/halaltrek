<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:42
 * @var $model \App\Models\CustomQuote
 */

$fields = [
    'name',
    'address',
    'Phone/Mobile'=>'contact',
    'group_contact',
    'email',

];
?>


@include('components.detailed-view',['fields'=>$fields, 'model'=>$model])

