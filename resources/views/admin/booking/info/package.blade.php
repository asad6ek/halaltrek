<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:45
 * @var $model \App\DDD\Packages\Models\Package
 */
$fields = [
    'ziyarat_type',
    'name',
    'date_from',
    'date_to',
    'num_days',
	'Hotel Makkah'=>function() use($model){
        $packageHotel =  $model->getHotelsFor(\App\Models\PackageHotel::TYPE_MAKKA);
        return $packageHotel? optional($packageHotel->hotel)->name:'';
	},
    'hotel_basis_makkah',
    'Hotel Madinah'=>function() use($model){
        $packageHotel =  $model->getHotelsFor(\App\Models\PackageHotel::TYPE_MADINA);
        return $packageHotel? optional($packageHotel->hotel)->name:'';
    },
    'hotel_basis_madinah',
    'first_visit',
    'ziyorah_included|bool',
    'transport_support|bool',
];

if ($model->isForUmrah()){
    $fields['Umrah Seminar']='options.seminar|bool';
    $fields['Umrah Goody bag']='options.goody_bag|bool';
}else{
    $fields['is_shifting']='is_shifting|bool';
    $fields['num_days_aziziya']='num_days_aziziya';
}

$flightsFields = [
    'airline' => 'airline.name',
    'from',
	'to',
    'from_date',
    'type',
    'description',
];

$hotelFields = [
    'hotel' => 'hotel.name',
    'type' => 'typeText',
];

$paymentFields = [
    'name',
    'price',
];
?>


<div>
	@include('components.detailed-view',['fields'=>$fields, 'model'=>$model])


	<h3>Flights</h3>
	@foreach($model->flights as $flight)
		@include('components.detailed-view',['fields'=>$flightsFields, 'model'=>$flight])
		<br>
	@endforeach



	<h3>Prices</h3>
	@foreach($model->payments as $payment)
		@include('components.detailed-view',['fields'=>$paymentFields, 'model'=>$payment])
		<br>
	@endforeach
</div>
