<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:43
 *
 * @var $models \App\Models\BookingGuest[]
 */
?>

<table class="table">
	<thead>
	<tr>
		<th>
			Id
		</th>

		<th>
			Name
		</th>

		<th>
			Birth date
		</th>

		<th>
			Passport expire
		</th>

		<th>
			Relation
		</th>

		<th>
			Last travel
		</th>
	</tr>
	</thead>
	<tbody>
	@foreach($models as $model)
		<tr>
			<td>
				{{   $model->id  }}
			</td>

			<td>
				{{ $model->name }}
			</td>

			<td>
				{{ $model->birth_date }}
			</td>

			<td>
				{{ $model->passport_expire }}
			</td>

			<td>
				{{ $model->relation }}
			</td>

			<td>
				{{ $model->last_travel }}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

