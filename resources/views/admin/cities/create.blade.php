<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 */
?>

@extends('admin.app')
@section('title') Dashboard admin create @endsection
@section('content')
	@include('admin::cities.form',[
	'model'=>$model,
	'method'=>'post',
	'countries'=>$countries,
	 'route'=>route('admin.cities.store')
	 ])
@endsection
