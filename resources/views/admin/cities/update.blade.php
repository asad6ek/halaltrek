<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard City update @endsection
@section('content')
	@include('admin::cities.form',[
	'model'=>$model,
	'method'=>'put',
	'countries'=>$countries,
	'route'=>route('admin.cities.update',['city'=>$model->id])
	])
@endsection
