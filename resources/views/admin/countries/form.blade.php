<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 * @var $user \App\Models\Admin
 * @var $route string
 * @var $method string
 */
?>

<form action="{{$route}}" method="post">
	@include('partials.formErrors')
	@csrf
	@method($method)
	<div class="row">
		<div class="col-md-12">
			<div class="card card-primary">
				<div class="card-body">
					<div class="form-group">
						<label for="inputName">Name</label>
						<input type="text" id="inputName" class="form-control" name="name" value="{{$model->name}}">
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<input type="submit" value="Save" class="btn btn-success">
			<a href="{{route('admin.countries.index')}}" class="btn btn-secondary">Cancel</a>
		</div>
	</div>
</form>
