@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<link href="https://pqina.nl/filepond/static/assets/filepond.css?1578584451627" rel="stylesheet">

	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-lg-4 col-6">
					<!-- small box -->
					<div class="small-box bg-info">
						<div class="inner">
							<h3>{{ $orderCount }}</h3>

							<p>Bookings</p>
						</div>
						<div class="icon">
							<i class="ion ion-bag"></i>
						</div>
						<a href="{{ route('admin.booking.index') }}" class="small-box-footer">More info <i
									class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-4 col-6">
					<!-- small box -->
					<div class="small-box bg-success">
						<div class="inner">
							<h3>{{ $agentCount }}</h3>

							<p>Agents</p>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="{{ route('admin.agent.index') }}" class="small-box-footer">More info <i
									class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-4 col-6">
					<!-- small box -->
					<div class="small-box bg-warning">
						<div class="inner">
							<h3>{{ $reviewCount }}</h3>

							<p>Reviews</p>
						</div>
						<div class="icon">
							<i class="ion ion-chatbox"></i>

						</div>
						<a href="{{ route('admin.review.index') }}" class="small-box-footer">More info <i
									class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>

				<!-- ./col -->
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-gradient-indigo">
                        <div class="inner">
                            <h3>{{ $makkahCount }}</h3>
{{--first_visit--}}
                            <p>Makkah tours</p>
                        </div>

                        <a href="{{ route('admin.packages.all') }}" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-gradient-blue">
                        <div class="inner">
                            <h3>{{ $madinahCount }}</h3>

                            <p>Madinah tours</p>
                        </div>

                        <a href="{{ route('admin.packages.all') }}" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            </div>
		</div>
        <!-- /.container-fluid -->
	</section>



@endsection





