<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 * @var $user \App\Models\Admin
 * @var $route string
 * @var $method string
 * @var $image \App\Models\HotelImage
 */
?>

<form action="{{$route}}" method="post" enctype="multipart/form-data" id="fform">
    @include('partials.formErrors')
    @csrf
    @method($method)
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="form-group">
                        <label for="hotel-images">Images</label>
                        <input type="file" class="filepond" name="images[]" multiple id="hotel-images"
                               data-allow-reorder="true" value="file">
                    </div>
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" id="inputName" class="form-control" name="name"
                               value="{{old('name',$model->name)}}">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Address</label>
                        <input type="text" id="inputAddress" class="form-control" name="address"
                               value="{{old('address',$model->address)}}">
                    </div>
                    <div class="form-group">
                        <label for="inputStar">Star</label>
                        <input type="text" id="inputStar" class="form-control" name="star"
                               value="{{old('star',$model->star)}}">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Excerpt</label>
                        <input type="text" id="inputAddress" class="form-control" name="excerpt"
                               value="{{old('excerpt',$model->excerpt)}}">
                    </div>

                    <div class="form-group">
                        <label for="inputAddress">Description</label>
                        <input type="text" id="inputAddress" class="form-control" name="description"
                               value="{{old('description',$model->description)}}">
                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Раздел</th>
                                    <th>Доступы</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($amenities as  $amenity)
                                    <tr>
                                        <td><img src="{{asset($amenity->label)}}" alt="">{{$amenity->title}}</td>
                                        <td>
                                            <div class="btn-group">

                                                <label class="btn btn-primary" id="label{{$amenity->id }}">

                                                    <input type="checkbox" name="attribute[{{$amenity->id}}]"
                                                           id="{{$amenity->id }}"
                                                           onchange="funk(this, {{ $amenity->id }} )"
                                                           value="{{$model->hotelAmenity($amenity->id)?$amenity->id:null}}"
                                                        {{$model->hotelAmenity($amenity->id)?'checked':null}}
                                                    >
                                                </label>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input type="submit" value="Save" class="btn btn-success">
            <a href="{{route('admin.hotels.index')}}" class="btn btn-secondary">Cancel</a>
        </div>
    </div>
</form>

@push('scripts')
    <link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.css')}}">
    <script src="{{asset('backend/plugins/filepond/filepond.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>

    <script src="{{asset('backend/plugins/filepond/filepond.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-type-validator.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-edit.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-resize.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filePond-image-transform.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-exif-orintation.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filepond/filepond-image-crop.min.js')}}"></script>
    <script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        FilePond.setOptions({
            server: {
                process: './process',
                revert: './revert',
                restore: './restore/',
                load: './load/',
                fetch: './fetch/',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Select the file input and use
        // create() to turn it into a pond
        var pond = FilePond.create(
            document.getElementById('hotel-images'),
            {
                files: [
                        @foreach($model->images as $image)
                    {
                        source: "{{$image->filePondPath()}}",
                        options: {
                            type: 'local'
                        }
                    }
                    {{ $loop->last?'':',' }}
                    @endforeach
                ],
                imagePreviewHeight: 170,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 300,
                imageResizeTargetHeight: 300,
                // stylePanelLayout: 'compact circle',
                styleLoadIndicatorPosition: 'center bottom',
                styleProgressIndicatorPosition: 'right bottom',
                styleButtonRemoveItemPosition: 'left bottom',
                styleButtonProcessItemPosition: 'right bottom',
            }
        );


        $('#fform').on('submit', function (e) {
            console.log(pond.status);
            if ([1, 4].indexOf(pond.status) === -1) {
                return false;
            }
            return true;
        });

    </script>

    <script type="text/javascript">

        function funk(checkbox, id) {
            let input = document.getElementById(id);
            if (checkbox.checked) {
                let label = document.getElementById('label' + id);
                input.value = id;
                console.log(input);
            } else {
                input.value = null;
            }

        }
    </script>
@endpush
