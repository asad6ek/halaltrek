<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard City update @endsection
@section('content')
	@include('admin::hotels.form',['model'=>$model,'method'=>'put','route'=>route('admin.hotels.update',['hotel'=>$model->id])])
@endsection
