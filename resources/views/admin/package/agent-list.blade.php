<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-17
 * Time: 11:35
 */
?>

@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					Please choose agent in order to create package
				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>User</th>
							<th>Email</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $agent)
							<tr data-agent="{{route('admin.agent.packages.create', $agent->id)}}" class="agentRedirect">
								<td>{{ $agent->name }}</td>
								<td>{{ $agent->email }}</td>
								<td>
									<a href="{{ route('admin.agent.packages.create', $agent->id) }}"
									   class="btn btn-success">Choose</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection


@push('scripts')
	<script>
		$('.agentRedirect').on('click', function () {
			window.location.href = $(this).attr('data-agent');
        });
	</script>
@endpush
