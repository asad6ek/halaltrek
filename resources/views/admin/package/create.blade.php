<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 22:29
 */
?>

@extends('admin.app')
@section('title') Dashboard package create @endsection
@section('content')
	@include('admin::package.form',[
	'models'=>$models,
	'method'=>'post',
	'route'=>route('admin.agent.packages.store', $agent_id),
	'hotelBasisList'=>$hotelBasisList,
	'visitList'=>$visitList,
	'cities'=>$cities,
	'hotels'=>$hotels,
	 'cancelLink'=>route('admin.agent.packages.index', $agent_id),
	])
@endsection
