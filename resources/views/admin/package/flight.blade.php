<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-17
 * Time: 10:02
 */
?>


<div class="flight row">
	<a href="#" class="remove-flight btn btn-danger">-</a>
	@isset($model['id'])
		<input type="hidden" name="flight[{{$key}}][id]"
		       value="{{  $model['id'] }}">
	@endisset
	<div class="col-md-4">
		<div class="form-group">
			<label for="airline-{{ $key }}">Airline</label>
			{{  Form::select(
					"flight[$key][airline]",
			$airlines,
		old("flight[$key][airline]", data_get($model,'airline_id')),
			['placeholder' => 'Pick ...','class'=>'form-control']
			) }}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="inputEmail">From</label>
			<input type="text" class="form-control" name="flight[{{$key}}][from]"
			       value="{{old("flight[$key][from]", data_get($model,'from'))}}">
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label for="inputEmail">To</label>
			<input type="text" class="form-control" name="flight[{{$key}}][to]"
			       value="{{ old("flight[$key][to]", data_get($model,'to'))}}">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label for="inputEmail">Date</label>
			<input type="text" class="form-control flight-{{$key}}-date date-input" id="flight-{{$key}}-from_date"
			       name="flight[{{ $key }}][from_date]"
			       value="{{old("flight[$key][from_date]", $model?optional(data_get($model,'from_date'))->format('d-m-Y'):'')}}">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label for="inputEmail">Return date</label>
			<input type="text" class="form-control flight-{{$key}}-date date-input" id="flight-{{$key}}-return_date"
			       name="flight[{{ $key }}][return_date]" data-value="{{data_get($model,'return_date')}}"
			       value="{{old("flight[$key][return_date]", $model?optional(data_get($model,'return_date'))->format('d-m-Y'):'')}}">
		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<label for="airline-{{ $key }}">Type</label>
			{{  Form::select(
					"flight[$key][type]",
			    \App\Models\PackageAirline::types(),
			old("flight[$key][type]", data_get($model,'type')),
			['placeholder' => 'Pick ...','class'=>'form-control flight-type']
			) }}
		</div>
	</div>

	<div class="col-md-12 flight-description">
		<div class="form-group">
			<label for="airline-{{ $key }}">Details</label>
			<textarea name="flight[{{$key}}][description]" id="" cols="30" class="form-control"
			          rows="3">{{ old("flight[$key][description]", data_get($model,'description')) }}</textarea>
		</div>
	</div>

</div>
