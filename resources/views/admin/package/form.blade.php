<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 22:28
 * @var $model \App\DDD\Packages\Models\Package
 */

use  \App\DDD\Packages\Models\Package;


$modelHaj = $models[Package::TYPE_HAJJ];
$modelUmrah = $models[Package::TYPE_UMRAH];


$type = $modelUmrah->id ? Package::TYPE_UMRAH : Package::TYPE_HAJJ;
$isModelHaj = old('ziyarat_type', $type) === Package::TYPE_HAJJ;
$isModelUmrah = old('ziyarat_type', $type) === Package::TYPE_UMRAH;
$isModelTours = old('ziyarat_type', $type) === Package::TYPE_TOURS;
?>

@include('partials.formErrors')
@csrf
@method($method)

<div class="card">
	<div class="card-header d-flex p-0">
		<h3 class="card-title p-3">Type:</h3>
		<ul class="nav nav-pills ml-auto p-3">
			<li class="nav-item"><a class="nav-link  @if($isModelHaj) active @endif" href="#tab_1"
			                        data-toggle="tab">Hajj</a></li>
			<li class="nav-item"><a class="nav-link  @if(!$isModelHaj) active @endif" href="#tab_2" data-toggle="tab">Umrah</a>
			</li>
            <li class="nav-item"><a class="nav-link  @if(!$isModelHaj and $isModelUmrah) active @endif" href="#tab_3" data-toggle="tab">Tours</a>
			</li>
		</ul>
	</div><!-- /.card-header -->
	<div class="card-body">
		<div class="tab-content">
			<div class="tab-pane  @if($isModelHaj) active @endif" id="tab_1">
				@php
					$model = $models[\App\DDD\Packages\Models\Package::TYPE_HAJJ];
				@endphp
				<form action="{{$route}}" method="post">
					@csrf
					@if($model->id)
						@method('put')
					@endif

					<h4>Hajj</h4>
					<input type="hidden" name="ziyarat_type" value="{{ \App\DDD\Packages\Models\Package::TYPE_HAJJ }}">
					<div class="form-group">
						<label for="inputName">Name</label>
						{{ Form::text('name', old('name', $model->name),['class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date from</label>
						{{ Form::text('date_from', old('date_from', optional($model->date_from)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date to</label>
						{{ Form::text('date_to', old('date_to',optional($model->date_to)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>

					<div class="form-group">
						<label for="inputEmail">Number of days</label>
						{{ Form::number('num_days', old('num_days', $model->num_days),['class'=>'form-control']) }}
					</div>

					<div class="form-group">
						<label for="inputEmail">Package class</label>
						@php
                            $p_class= \App\DDD\Packages\Models\Package::packageClasses();
                        @endphp
                        {{  Form::select('package_class',$p_class,old('package_class', $model->package_class),['placeholder' => 'Pick a class ...','class'=>'form-control']) }}

					</div>
                    <div class="form-group">
						<label for="inputEmail">City</label>
						@php
                            $cities=\App\Models\City::all()->toArray();
$package_city =[];
$idies = [];
foreach ($cities as $city)
    $package_city[$city["id"]] = $city["name"];

                        @endphp
                        {{  Form::select('city_id[]',$package_city,old('city_id', $model->getCitiesIds()),['placeholder' => 'Pick a city ...','class'=>'form-control', 'multiple'=>'multiple','name'=>'city_id[]']) }}

					</div>

                    <div class="form-group">
						<label for="inputEmail">Hotel Name in Makkah</label>
						{{  Form::select(
						'hotels[makka]',
						$hotels,
						old('hotels[makka]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MAKKA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Makkah</label>
						{{  Form::select(
						'hotel_basis_makkah',
						$hotelBasisList,
						old('hotel_basis_makkah', $model->hotel_basis_makkah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel Name in Madinah</label>
						{{  Form::select(
						'hotels[madina]',
						$hotels,
						old('hotels[madina]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MADINA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Madinah</label>
						{{  Form::select(
						'hotel_basis_madinah',
						$hotelBasisList,
						old('hotel_basis_madinah', $model->hotel_basis_madinah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Where do Hujjaj visit first?</label>
						{{  Form::select(
						'first_visit',
						$visitList,
						old('first_visit', $model->first_visit),
						['placeholder' => 'Pick...','class'=>'form-control']) }}
					</div>



					<div class="form-group">
						<label for="inputEmail">Price per person</label>
						{{  Form::text(
						'payment[0][price]',
						old('num_days_aziziya', $model->getPriceFor(1)),
						['class'=>'form-control']) }}

						<input type="hidden" value="1" name="payment[0][type]">
						<input type="hidden" value="usd" name="payment[0][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for double</label>
						{{  Form::text(
						'payment[1][price]',
						old('payment[1][price]', $model->getPriceFor(2)),
						['class'=>'form-control']) }}

						<input type="hidden" value="2" name="payment[1][type]">
						<input type="hidden" value="usd" name="payment[1][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for triple</label>
						{{  Form::text(
						'payment[2][price]',
						old('payment[2][price]', $model->getPriceFor(3)),
						['class'=>'form-control']) }}

						<input type="hidden" value="3" name="payment[2][type]">
						<input type="hidden" value="usd" name="payment[2][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for Quad</label>
						{{  Form::text(
						'payment[3][price]',
						old('payment[3][price]', $model->getPriceFor(4)),
						['class'=>'form-control']) }}

						<input type="hidden" value="4" name="payment[3][type]">
						<input type="hidden" value="usd" name="payment[3][currency]">
					</div>

                    <div class="col-md-12 flight-description">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" cols="30" class="form-control"
                                      rows="3">{{ old("description") }}</textarea>
                        </div>
                    </div>

					<div class="form-check">
						<input type="hidden" name="ziyorah_included" value="0">

						{{  Form::checkbox(
						'ziyorah_included',
						1,
						old('ziyorah_included', $model->ziyorah_included),
						['id'=>'label-ziyorah_included'] ) }}
						<label class="form-check-label" for="label-ziyorah_included">Ziyaarah included?</label>
					</div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[breakfast]',
                         1,
                         old('options[breakfast]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-breakfast1']) }}
                        <label class="form-check-label" for="label-options-breakfast1">Breakfast full and half?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[ground_transport]',
                         1,
                         old('options[ground_transport]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-ground_transport1']) }}
                        <label class="form-check-label" for="label-options-ground_transport1">Ground transport?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[goody_bag]',
                         1,
                         old('options[goody_bag]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-goodyBag1']) }}
                        <label class="form-check-label" for="label-options-goodyBag1">Goody bag?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[hadiy]',
                         1,
                         old('options[hadiy]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-hadiy1']) }}
                        <label class="form-check-label" for="label-options-hadiy1">Hadiy?</label>
                    </div>

                    <div class="form-check form-group">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[visit]',
                         1,
                         old('options[visit]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-visit1']) }}
                        <label class="form-check-label" for="label-options-visit1">Visa?</label>
                    </div>




                    <div class="form-check form-group">
						<input type="hidden" name="is_shifting" value="0">

						{{  Form::checkbox(
						'is_shifting',
						 1,
						 old('is_shifting', $model->is_shifting),
						 ['id'=>'label-is_shifting']) }}
						<label class="form-check-label" for="label-is_shifting">Shifting?</label>
					</div>

					<div id="num_days_aziziya-form" class="form-group @if(!old('is_shifting',$model->is_shifting)) hide @endif ">
						<label for="inputEmail">Number of days in Aziziya?</label>
						{{  Form::number(
						'num_days_aziziya',
						old('num_days_aziziya', $model->num_days_aziziya),
						['class'=>'form-control']) }}
					</div>

					<div class="flights flights1">
						@if(old('flight',$model->flights->count()))


							@foreach(old('flight',$model->flights) as $flight_key => $flight)



								@include('admin.package.flight',['key'=>$flight_key, 'model'=>$flight])


							@endforeach
						@else
							@include('admin.package.flight',['key'=>'0','model'=>[]])
						@endif
					</div>
					<div class="row p-3">
						<a href="#" class="add-flight1 btn btn-success">Add more flights</a>
					</div>

					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success">
							<a href="{{ $cancelLink }}" class="btn btn-secondary">Cancel</a>
						</div>
					</div>



                </form>
			</div>
			<!-- /.tab-pane -->

			<div class="tab-pane  @if(!$isModelHaj) active @endif" id="tab_2">
				@php
					$model = $models[\App\DDD\Packages\Models\Package::TYPE_UMRAH];
				@endphp
				<form action="{{$route}}" method="post">
					@csrf

                        @method($method)

					<h4>Umrah</h4>
					<input type="hidden" name="ziyarat_type" value="{{ \App\DDD\Packages\Models\Package::TYPE_UMRAH }}">
					<div class="form-group">
						<label for="inputName">Name</label>
						{{ Form::text('name', old('name', $model->name),['class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date from</label>
						{{ Form::text('date_from', old('date_from', optional($model->date_from)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date to</label>
						{{ Form::text('date_to', old('date_to', optional($model->date_to)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Number of days</label>
						{{ Form::text('num_days', old('num_days', $model->num_days),['class'=>'form-control']) }}
					</div>

                    <div class="form-group">
                        <label for="inputEmail">Package class</label>
                        @php
                            $p_class= \App\DDD\Packages\Models\Package::packageClasses();
                        @endphp
                        {{  Form::select('package_class',$p_class,old('package_class', $model->package_class),['placeholder' => 'Pick a class ...','class'=>'form-control']) }}

                    </div>
                    <div class="form-group">
                        <label for="inputEmail">City</label>
                        @php
                            $cities=\App\Models\City::all()->toArray();
$package_city =[];
$idies = [];
foreach ($cities as $city)
    $package_city[$city["id"]] = $city["name"];

                        @endphp
                        {{  Form::select('city_id[]',$package_city,old('city_id', $model->getCitiesIds()),['placeholder' => 'Pick a city ...','class'=>'form-control', 'multiple'=>'multiple','name'=>'city_id[]']) }}

                    </div>

					<div class="form-group">
						<label for="inputEmail">Hotel Name in Makka</label>
						{{  Form::select(
						'hotels[makka]',
						$hotels,
						old('hotels[makka]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MAKKA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Makka</label>
						{{  Form::select(
						'hotel_basis_makkah',
						$hotelBasisList,
						old('hotel_basis', $model->hotel_basis_makkah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel Name in Madina</label>
						{{  Form::select(
						'hotels[madina]',
						$hotels,
						old('hotels[madina]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MADINA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Madina</label>
						{{  Form::select(
						'hotel_basis_madinah',
						$hotelBasisList,
						old('hotel_basis', $model->hotel_basis_madinah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Where do Hujjaj visit first?</label>
						{{  Form::select(
						'first_visit',
						$visitList,
						old('first_visit', $model->first_visit),
						['placeholder' => 'Pick ...','class'=>'form-control']) }}
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person</label>
						{{  Form::text(
						'payment[0][price]',
						old('num_days_aziziya', $model->getPriceFor(1)),
						['class'=>'form-control']) }}

						<input type="hidden" value="1" name="payment[0][type]">
						<input type="hidden" value="usd" name="payment[0][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for double</label>
						{{  Form::text(
						'payment[1][price]',
						old('payment[1][price]', $model->getPriceFor(2)),
						['class'=>'form-control']) }}

						<input type="hidden" value="2" name="payment[1][type]">
						<input type="hidden" value="usd" name="payment[1][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for triple</label>
						{{  Form::text(
						'payment[2][price]',
						old('payment[2][price]', $model->getPriceFor(3)),
						['class'=>'form-control']) }}

						<input type="hidden" value="3" name="payment[2][type]">
						<input type="hidden" value="usd" name="payment[2][currency]">
					</div>

					<div class="form-group">
						<label for="inputEmail">Price per person for Quad</label>
						{{  Form::text(
						'payment[3][price]',
						old('payment[3][price]', $model->getPriceFor(4)),
						['class'=>'form-control']) }}

						<input type="hidden" value="4" name="payment[3][type]">
						<input type="hidden" value="usd" name="payment[3][currency]">
					</div>

                    <div class="col-md-12 flight-description">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description1" cols="30" class="form-control"
                                      rows="3">{{ old("description") }}</textarea>
                        </div>
                    </div>

					<div class="form-check">
						<input type="hidden" name="ziyorah_included" value="0">
						{{  Form::checkbox(
						'ziyorah_included',
						1,
						old('ziyorah_included', $model->ziyorah_included),
						['id'=>'label-ziyorah_included2'] ) }}
						<label class="form-check-label" for="label-ziyorah_included2">Ziyaarah included?</label>
					</div>


					<div class="form-check">
						<input type="hidden" name="transport_support" value="0">
						{{  Form::checkbox(
						'options[seminar]',
						 1,
						 old('options[seminar]', optional($model->option)->seminar),
						 ['id'=>'label-options-seminar2']) }}
						<label class="form-check-label" for="label-options-seminar2">Umrah seminar?</label>
					</div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[breakfast]',
                         1,
                         old('options[breakfast]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-breakfast2']) }}
                        <label class="form-check-label" for="label-options-breakfast2">Breakfast full and half?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[ground_transport]',
                         1,
                         old('options[ground_transport]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-ground_transport2']) }}
                        <label class="form-check-label" for="label-options-ground_transport2">Ground transport?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[goody_bag]',
                         1,
                         old('options[goody_bag]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-goodyBag2']) }}
                        <label class="form-check-label" for="label-options-goodyBag2">Goody bag?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[hadiy]',
                         1,
                         old('options[hadiy]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-hadiy2']) }}
                        <label class="form-check-label" for="label-options-hadiy2">Hadiy?</label>
                    </div>

                    <div class="form-check form-group">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[visit]',
                         1,
                         old('options[visit]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-visit2']) }}
                        <label class="form-check-label" for="label-options-visit2">Visa?</label>
                    </div>


                    <div class="flights flights2">
						@if(old('flight',$model->flights->count()))

							@foreach(old('flight',$model->flights) as $flight_key => $flight)
								@include('admin.package.flight',['key'=>$flight_key, 'model'=>$flight])
							@endforeach
						@else
							@include('admin.package.flight',['key'=>'0','model'=>[]])
						@endif
					</div>

					<div class="row p-3">
						<a href="#" class="add-flight2 btn btn-success">Add more flights</a>
					</div>

					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success">
							<a href="{{ $cancelLink }}" class="btn btn-secondary">Cancel</a>
						</div>
					</div>
				</form>

			</div>
            <!-- /.tab-pane -->
			<div class="tab-pane  @if(!$isModelHaj and !$isModelUmrah) active @endif" id="tab_3">
				@php
					$model = $models[\App\DDD\Packages\Models\Package::TYPE_TOURS];
				@endphp
				<form action="{{$route}}" method="post">
					@csrf

                        @method($method)

					<h4>Tours</h4>
					<input type="hidden" name="ziyarat_type" value="{{ \App\DDD\Packages\Models\Package::TYPE_TOURS }}">
					<div class="form-group">
						<label for="inputName">Name</label>
						{{ Form::text('name', old('name', $model->name),['class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date from</label>
						{{ Form::text('date_from', old('date_from', optional($model->date_from)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Date to</label>
						{{ Form::text('date_to', old('date_to', optional($model->date_to)->format('d-m-Y')),['class'=>'form-control date-input']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Number of days</label>
						{{ Form::text('num_days', old('num_days', $model->num_days),['class'=>'form-control']) }}
					</div>

                    <div class="form-group">
                        <label for="inputEmail">Package class</label>
                        @php
                            $p_class= \App\DDD\Packages\Models\Package::packageClasses();
                        @endphp
                        {{  Form::select('package_class',$p_class,old('package_class', $model->package_class),['placeholder' => 'Pick a class ...','class'=>'form-control']) }}

                    </div>
                    <div class="form-group">
                        <label for="inputEmail">City</label>
                        @php
                            $cities=\App\Models\City::all()->toArray();
$package_city =[];
$idies = [];
foreach ($cities as $city)
    $package_city[$city["id"]] = $city["name"];

                        @endphp
                        {{  Form::select('city_id[]',$package_city,old('city_id', $model->getCitiesIds()),['placeholder' => 'Pick a city ...','class'=>'form-control', 'multiple'=>'multiple','name'=>'city_id[]']) }}

                    </div>

                    <div class="form-group">
						<label for="inputEmail">Hotel Name in Makka</label>
						{{  Form::select(
						'hotels[makka]',
						$hotels,
						old('hotels[makka]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MAKKA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Makka</label>
						{{  Form::select(
						'hotel_basis_makkah',
						$hotelBasisList,
						old('hotel_basis', $model->hotel_basis_makkah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel Name in Madina</label>
						{{  Form::select(
						'hotels[madina]',
						$hotels,
						old('hotels[madina]', optional($model->getHotelsFor(\App\Models\PackageHotel::TYPE_MADINA))->hotel_id),
						['placeholder' => 'Pick a hotel ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Hotel basis for Madina</label>
						{{  Form::select(
						'hotel_basis_madinah',
						$hotelBasisList,
						old('hotel_basis', $model->hotel_basis_madinah),
						['placeholder' => 'Pick a hotel basis...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Where do Hujjaj visit first?</label>
						{{  Form::select(
						'first_visit',
						$visitList,
						old('first_visit', $model->first_visit),
						['placeholder' => 'Pick ...','class'=>'form-control']) }}
					</div>
					<div class="form-group">
						<label for="inputEmail">Price per person</label>
						{{  Form::text(
						'payment[0][price]',
						old('num_days_aziziya', $model->getPriceFor(1)),
						['class'=>'form-control']) }}

						<input type="hidden" value="1" name="payment[0][type]">
						<input type="hidden" value="usd" name="payment[0][currency]">
					</div>
					<div class="form-group">
						<label for="inputEmail">Price per person for double</label>
						{{  Form::text(
						'payment[1][price]',
						old('payment[1][price]', $model->getPriceFor(2)),
						['class'=>'form-control']) }}

						<input type="hidden" value="2" name="payment[1][type]">
						<input type="hidden" value="usd" name="payment[1][currency]">
					</div>
					<div class="form-group">
						<label for="inputEmail">Price per person for triple</label>
						{{  Form::text(
						'payment[2][price]',
						old('payment[2][price]', $model->getPriceFor(3)),
						['class'=>'form-control']) }}

						<input type="hidden" value="3" name="payment[2][type]">
						<input type="hidden" value="usd" name="payment[2][currency]">
					</div>
					<div class="form-group">
						<label for="inputEmail">Price per person for Quad</label>
						{{  Form::text(
						'payment[3][price]',
						old('payment[3][price]', $model->getPriceFor(4)),
						['class'=>'form-control']) }}

						<input type="hidden" value="4" name="payment[3][type]">
						<input type="hidden" value="usd" name="payment[3][currency]">
					</div>
                    <div class="col-md-12 flight-description">
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description1" cols="30" class="form-control"
                                      rows="3">{{ old("description") }}</textarea>
                        </div>
                    </div>
					<div class="form-check">
						<input type="hidden" name="ziyorah_included" value="0">
						{{  Form::checkbox(
						'ziyorah_included',
						1,
						old('ziyorah_included', $model->ziyorah_included),
						['id'=>'label-ziyorah_included3'] ) }}
						<label class="form-check-label" for="label-ziyorah_included3">Ziyaarah included?</label>
					</div>
					<div class="form-check">
						<input type="hidden" name="transport_support" value="0">
						{{  Form::checkbox(
						'options[seminar]',
						 1,
						 old('options[seminar]', optional($model->option)->seminar),
						 ['id'=>'label-options-seminar3']) }}
						<label class="form-check-label" for="label-options-seminar3">Umrah seminar?</label>
					</div>
					<div class="form-check">
						<input type="hidden" name="transport_support" value="0">
						{{  Form::checkbox(
						'options[goody_bag]',
						 1,
						 old('options[goody_bag]', optional($model->option)->goody_bag),
						 ['id'=>'label-options-goodyBag3']) }}
						<label class="form-check-label" for="label-options-goodyBag3">Goody bag?</label>
					</div>


                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[breakfast]',
                         1,
                         old('options[breakfast]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-breakfast3']) }}
                        <label class="form-check-label" for="label-options-breakfast3">Breakfast full and half?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[ground_transport]',
                         1,
                         old('options[ground_transport]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-ground_transport3']) }}
                        <label class="form-check-label" for="label-options-ground_transport3">Ground transport?</label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[hadiy]',
                         1,
                         old('options[hadiy]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-hadiy3']) }}
                        <label class="form-check-label" for="label-options-hadiy3">Hadiy?</label>
                    </div>

                    <div class="form-check form-group">
                        <input type="hidden" name="transport_support" value="0">
                        {{  Form::checkbox(
                        'options[visit]',
                         1,
                         old('options[visit]', optional($model->option)->goody_bag),
                         ['id'=>'label-options-visit3']) }}
                        <label class="form-check-label" for="label-options-visit3">Visa?</label>
                    </div>


                    <div class="flights flights2">
						@if(old('flight',$model->flights->count()))

							@foreach(old('flight',$model->flights) as $flight_key => $flight)
								@include('admin.package.flight',['key'=>$flight_key, 'model'=>$flight])
							@endforeach
						@else
							@include('admin.package.flight',['key'=>'0','model'=>[]])
						@endif
					</div>
					<div class="row p-3">
						<a href="#" class="add-flight2 btn btn-success">Add more flights</a>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success">
							<a href="{{ $cancelLink }}" class="btn btn-secondary">Cancel</a>
						</div>
					</div>
				</form>

			</div>
		</div>
		<!-- /.tab-content -->
	</div><!-- /.card-body -->
</div>



<div class="template" style="display: none">
	@include('admin.package.flight',['key'=>'{$i}','model'=>[]])
</div>

@push('scripts')
	<script src="{{ asset('backend/js/dynamicForm.js') }}"></script>
	<script>
        $('.date-input').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: parseInt(moment().format('YYYY'), 10),
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
	</script>

	<script>
        var dynamicFlights1 = new DynamicForm({
            count_block: "{{ count($models['hajj']->flights)?:1}}",
            parent_block: ".flights1",
            add_button: '.add-flight1',
            copy_block: '.template',
            del_button: '.remove-flight',
            block: '.flight',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 5,
            min: 1,
            onAdd: function (index) {
                $('.flight-' + index + '-date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'DD-MM-YYYY'
                    }
                });
            }
        });

        var dynamicFlights2 = new DynamicForm({
            count_block: "{{ count($models['umrah']->flights)?:1}}",
            parent_block: ".flights2",
            add_button: '.add-flight2',
            copy_block: '.template',
            del_button: '.remove-flight',
            block: '.flight',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 5,
            min: 1,
            onAdd: function (index) {
                $('.flight-' + index + '-date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                });
            }
        });
  var dynamicFlights3 = new DynamicForm({
            count_block: "{{ count($models['tours']->flights)?:1}}",
            parent_block: ".flights2",
            add_button: '.add-flight2',
            copy_block: '.template',
            del_button: '.remove-flight',
            block: '.flight',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 5,
            min: 1,
            onAdd: function (index) {
                $('.flight-' + index + '-date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                });
            }
        });

        dynamicFlights1.init();
        dynamicFlights2.init();
        dynamicFlights3.init();


        $(document).on('change', '.flight-type', function (e) {

				flightDescriptionToggle(this);
        });

        function flightDescriptionToggle(input) {
            if ($(input).val() === 'Indirect') {
                console.log('indirect');
                $(input).parents('.flight').find('.flight-description').show();
            } else {
                console.log('direct');
                $(input).parents('.flight').find('.flight-description').hide();

            }

        }


        $('.flight-type').each(function (index, input) {
            flightDescriptionToggle(input);
        });


        $('#label-is_shifting').click(function (e) {
	        $('#num_days_aziziya-form').toggle('hide');
        });

        $('[name="city_id[]"').select2()
	</script>

	<style>
		.hide{
			display: none;
		}
	</style>
@endpush


