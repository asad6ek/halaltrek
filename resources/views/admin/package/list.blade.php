<?php
/**
 * @var $model \App\DDD\Packages\Models\Package
 */
?>
@extends('admin.app')
@section('title') Packages @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					@isset($agent_id)
						<a href="{{ route('admin.agent.packages.create', $agent_id) }}"
						   class="btn btn-success">Create</a>
					@else
						<a href="{{ route('admin.packages.agents') }}"
						   class="btn btn-success">Create</a>
					@endisset
				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Ziyarat type</th>
							<th>Agent</th>
							<th>Date from</th>
							<th>Date to</th>
							<th>Number of days</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key=> $model)
							<tr>
								<td>{{  $models->firstItem() +$key }}</td>
								<td>{{ $model->name }}</td>
								<td>{{ $model->ziyarat_type }}</td>
								<td>
									<a href="{{route('admin.agent.edit',$model->agent_id)}}">
										{{ optional($model->agent)->name }}
									</a>
								</td>
                                <td>{{    Carbon\Carbon::parse($model->date_from)->format('jS M Y') }}</td>
                                <td>{{  Carbon\Carbon::parse( $model->date_to )->format('jS M Y') }}</td>
								<td>{{ $model->num_days }}</td>
								<td>
									<a href="{{ route('admin.agent.packages.edit',['agent'=>optional($model->agent)->id, 'package'=>$model->id]) }}"
									   class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									<form action="{{ route('admin.agent.packages.delete',['agent'=>optional($model->agent)->id, 'package'=>$model->id]) }}"
									      method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
