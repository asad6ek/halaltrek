<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 22:29
 */
?>

@extends('admin.app')
@section('title') Dashboard package update @endsection
@section('content')
	@include('admin::package.form',[
	'models'=>$models,
	'method'=>'put',
	'route'=>route('admin.agent.packages.update',['agent'=>$agent_id, 'package'=>$id]),
	'hotelBasisList'=>$hotelBasisList,
	'visitList'=>$visitList,
	'cities'=>$cities,
	 'cancelLink'=>route('admin.agent.packages.index', $agent_id),
	])
@endsection
