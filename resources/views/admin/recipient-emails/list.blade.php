@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Created at</th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key => $model)
							<tr>
								<td>{{  $models->firstItem() +$key }}</td>
								<td>{{ $model->email }}</td>
                                <td>{{ Carbon\Carbon::parse($model->created_at)->format('jS M Y') }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $models->links() }}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
