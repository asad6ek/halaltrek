@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Status</th>
							<th>Agent</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key => $model)
							<tr>
								<td>{{  $models->firstItem() +$key }}</td>
								<td>{{ $model->name }}</td>
								<td>{{ $model->email }}</td>
								<td>{{ $model->status() }}</td>
								<td>
									<a href="{{ route('admin.agent.edit', [
									'agent'=>optional($model->reviewable)->id,
									]) }}">{{ ($model->reviewable)->name }}</a>
								</td>

								<td>
									<a href="{{ route('admin.review.edit',$model->id) }}"
									   class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									@if(!$model->isActive())
									<form action="{{ route('admin.review.active',$model->id) }}"
									      method="POST">
										@csrf
										<input type="submit" value="Activate" class="btn btn-success">
									</form>
									@endif
									<form action="{{ route('admin.review.delete',$model->id) }}"
									      method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $models->links() }}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
