<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 */
?>

@extends('admin.app')
@section('title') Dashboard Review update @endsection
@section('content')
	@include('admin::reviews.form',['model'=>$model,'method'=>'put','route'=>route('admin.review.update',$model->id)])
@endsection
