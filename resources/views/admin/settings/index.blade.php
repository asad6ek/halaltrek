@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
    <!-- /.row -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('admin.settings.create')}}" class="btn btn-success">Create</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Images</th>
                            <th>Comment</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($settings as $key => $carusel)
                            <tr>
                                <td>{{  $settings->firstItem() +$key }}</td>
                                <td>
                                    <img id="showImage" src="{{asset('frontend/images/'.$carusel->img)}}" alt=""  width="200">
                                </td>

                                <td>{{ $carusel->comment }}</td>
                                <td>
                                    <form action="{{ route('admin.settings.delete',['setting'=>$carusel->id]) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.row -->
@endsection
