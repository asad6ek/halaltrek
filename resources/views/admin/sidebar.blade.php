@php
    $review = App\Models\Review::where('status_read', 0)->count();
    $booking_read = \App\Models\BookingRead::where('status_admin', 0)->count();
@endphp
<!-- Main Sidebar Container -->
@php

$review = App\Models\Review::where('status_read', 0)->count();

@endphp


<aside class="main-sidebar sidebar-dark-primary elevation-4">


	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="info">
				<a href="#" class="d-block">Alexander Pierce</a>
				<a class="nav-link text-light btn btn-danger" href="{{ route('admin.logout') }}"><i
							class="fa fa-sign-out fa-lg"></i> Logout</a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
					 with font-awesome or any other icon font library -->
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.dashboard') }}" class="nav-link {{ isActiveRoute('admin.dashboard', true) }}">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Dashboard
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.agent.index') }}" class="nav-link {{ isActiveRoute('admin.agent') }}">
						<i class="nav-icon fas  fa-user-tie"></i>
						<p>
							Agents
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.packages.all') }}" class="nav-link {{ isActiveRoute('packages.all', true) }}">
						<i class="nav-icon fas  fas fa-columns"></i>
						<p>
							All packages
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.admin.index') }}" class="nav-link {{isActiveRoute('admin.admin')}}">
						<i class="nav-icon fas fa-users"></i>
						<p>
							Admins
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.hotels.index') }}" class="nav-link {{isActiveRoute('admin.hotels')}}">
						<i class="nav-icon fas  fa-hospital"></i>
						<p>
							Hotels
						</p>
					</a>
				</li>
                <li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.amenities.index') }}" class="nav-link {{isActiveRoute('admin.amenities')}}">
						<i class="nav-icon fas  fa-box-open"></i>
						<p>
                            Amenities
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.cities.index') }}" class="nav-link {{isActiveRoute('admin.cities')}}">
						<i class="nav-icon fas   fa-bullseye"></i>
						<p>
							Cities
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.countries.index') }}" class="nav-link {{isActiveRoute('admin.countries')}}">
						<i class="nav-icon fas   fa-bullseye"></i>
						<p>
							Countries
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.airlines.index') }}" class="nav-link {{isActiveRoute('admin.airlines')}}">
						<i class="nav-icon fas  fa-plane"></i>
						<p>
							Airlines
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.review.index') }}" class="nav-link {{isActiveRoute('admin.review')}}">
						<i class="nav-icon fas  fa-comments"></i>
						<p>
							Reviews ({{$review}})
						</p>
					</a>
				</li>

				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.booking.index') }}" class="nav-link {{isActiveRoute('admin.booking')}}">
						<i class="nav-icon fas  fa-calendar-check"></i>
						<p>
							Bookings ({{$booking_read}})
						</p>
					</a>
				</li>
                <li class="nav-item has-treeview menu-open">
					<a href="{{ route('admin.recipient.index') }}" class="nav-link {{isActiveRoute('admin.recipient')}}">
						<i class="nav-icon fas  fa-calendar-check"></i>
						<p>
							Recipient emails
						</p>
					</a>
				</li>

                <li class="nav-item  has-treeview menu-open">
					<a href="{{ route('admin.settings.index') }}" class="nav-link {{isActiveRoute('admin.settings')}}">
						<i class="nav-icon fas  fa-edit"></i>
						<p>
							Settings
						</p>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
