@extends('templates.main.app')
@section('title') Dashboard @endsection
@section('content')
	<section class="sign-area">
		<div class="sign-area-box d-flex justify-content-center">
			<div class="sign-box-wth">
				<div class="sign-area-content flex-wrap d-flex justify-content-between">
					<div class="sign-area-title">
						<h3>
							Login
						</h3>
						<p>
							Login to dashboard
						</p>
					</div>

				</div>
				<div class="sign-form">
					<form action="{{ route('agent.login') }}" method="POST">
						@include('partials.formErrors')
						{{ csrf_field() }}

						<div class="input-box">
							<input class="form-control sign-form-control" type="email" name="email" id="youremail"
							       value="{{ session()->get('agent.email') }}">
							<label for="youremail" class="sign-input-label">Your Email<span aria-hidden="true" class="star">*</span></label>

						</div>
						<div class="input-box">
							<input class="form-control sign-form-control" type="password" name="password" id="yourpassword">
							<label for="yourpassword" class="sign-input-label">Your Password<span aria-hidden="true"
							                                             class="star">*</span></label>
						</div>
						<div class="d-flex justify-content-end align-items-center flex-wrap sign-btn-box">
							<p class="backing-link">
								<a href="{{route('agent.register1')}}">Register</a>
							</p>
							<button class="btn btn-success sign-first-btn">Login</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')

	<script>
        var material = new materialInput('.sign-form-control');

        material.init();
	</script>
@endpush
