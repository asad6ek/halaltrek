@extends('templates.main.app')
@section('title') Dashboard @endsection
@section('content')
	<section class="sign-area">
		<div class="sign-area-box d-flex justify-content-center">
			<div class="sign-box-wth">
				<div class="sign-area-content flex-wrap d-flex justify-content-between">
					<div class="sign-area-title">
						<h3>
							Agents Signup
						</h3>
						<p>
							Lorem ipsum dolor sit amet, dignissim in nunc aliquam
						</p>
					</div>
					<div class="title-step">
                            <span>
                                Step 1/2
                            </span>
					</div>
				</div>
				<div class="sign-form">
					<form action="{{ route('post.register1') }}" method="POST">
						@include('partials.formErrors')
						@csrf
						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="name" id="username"
							       value="{{ old('name', session()->get('agent.name'))  }}" required>
							<label for="username" class="sign-input-label">Full Name<span aria-hidden="true"
							                                                              class="star">*</span></label>

						</div>

						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="manager_name"
							       id="managername" required
							       value="{{ old('manager_name',  session()->get('agent.manager_name')) }}">
							<label for="managername" class="sign-input-label">Manager Name</label>

						</div>


						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="address" id="address-agent"
							       value="{{ old('address', session()->get('agent.address')) }}" required>
							<label for="address-agent" class="sign-input-label">Address</label>

						</div>

						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="town" id="town-agent"
							       value="{{ old('town', session()->get('agent.town')) }}" required>
							<label for="town-agent" class="sign-input-label">Town</label>

						</div>

						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="postcode" id="yourpostcode"
							       value="{{ old('postcode', session()->get('agent.postcode')) }}" required>
							<label for="yourpostcode" class="sign-input-label">Postcode</label>

						</div>
						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="telephone" id="telephone"
							       value="{{ old('telephone', session()->get('agent.telephone')) }}" required>
							<label for="telephone" class="sign-input-label">Telephone</label>
						</div>
						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="mobile" id="mobile"
							       value="{{ old('mobile', session()->get('agent.mobile')) }}" required>
							<label for="mobile" class="sign-input-label">Mobile</label>
						</div>

						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="website" id="website"
							       value="{{ old('website', session()->get('agent.website')) }}" required>
							<label for="website" class="sign-input-label">Website</label>
						</div>
						<div class="field bg-white">
							<label for="hotel-search">Registered with Saudi Hajj Ministry</label>
							<select class="js-example-placeholder-single js-states form-control"
							        name="registered_with_ministry">
								<option value="1">Yes</option>
								<option value="0">No</option>

							</select>
						</div>
						<div class="input-box">
							<input class="form-control sign-form-control" type="text" name="atol_number" id="atolnumber"
							       value="{{ old('atol_number', session()->get('agent.atol_number')) }}" required>
							<label for="atolnumber" class="sign-input-label">ATOL Number</label>
						</div>
						<div class="d-flex justify-content-end align-items-center flex-wrap sign-btn-box">
							<p class="backing-link">
								<a href="{{route('agent.login')}}">Login</a>
							</p>
							<button class="btn btn-success sign-first-btn">Next Step</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection
@push('scripts')

	<script>
        var input = new materialInput('.sign-form-control');
        input.init()

        $(".js-states").select2({
            placeholder: "Select a state",
            allowClear: true
        });
	</script>
@endpush
