@extends('templates.main.app')
@section('title') Dashboard @endsection
@section('content')
	<section class="sign-area">
		<div class="sign-area-box d-flex justify-content-center">
			<div class="sign-box-wth">
				<div class="sign-area-content flex-wrap d-flex justify-content-between">
					<div class="sign-area-title">
						<h3>
							Agents Signup
						</h3>
						<p>
							Lorem ipsum dolor sit amet, dignissim in nunc aliquam
						</p>
					</div>
					<div class="title-step">
                            <span>
                                Step 2/2
                            </span>
					</div>
				</div>
				<div class="sign-form">
					<form action="{{ route('post.register2') }}" method="POST">
						@include('partials.formErrors')
						{{ csrf_field() }}

						<div class="input-box">
							<input class="form-control sign-form-control" type="email" name="email" id="youremail"
							       value="{{ old('email', session()->get('agent.email')) }}">
							<label for="youremail" class="sign-input-label">Your Email</label>

						</div>
						<div class="input-box">

							<input class="form-control sign-form-control" type="password" name="password" id="yourpassword">
							<label for="yourpassword" class="sign-input-label">Your Password</label>
						</div>
						<div class="input-box">
							<input type="password" name="password_confirmation" id="password_confirmation" class="form-control sign-form-control" required >
							<label for="password_confirmation" class="sign-input-label">Confirm Password</label>
						</div>
						<div class="d-flex justify-content-between align-items-center flex-wrap sign-btn-box">
							<p class="backing-link"> <a href="{{ route('agent.register1') }}">Back To Previous</a></p>
							<button class="btn btn-success sign-second-btn">Next Step</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

@endsection

@push('scripts')

	<script>
        var material = new materialInput('.sign-form-control');

        material.init();
	</script>
@endpush
