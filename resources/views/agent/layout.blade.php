<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 17:00
 */
?>
@extends('templates.admin.app')
@section('body')
	@include('agent.sidebar')
	<div class="content-wrapper">
		<div class="content-header">
			<div class="container-fluid">

			</div>
		</div>
		<section class="content">
			<div class="container-fluid">
				@include('partials.AlertComponent')

				@yield('content')

			</div>
		</section>
	</div>
@endsection()

