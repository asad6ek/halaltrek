@extends('agent.layout')
@section('title') Dashboard @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Booking type</th>
							<th>Name</th>
							<th>Email</th>
							<th>address</th>
							<th>Contact</th>
							<th>Created at</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key => $model)
							<tr>
								<td>{{ $models->firstItem() +$key }}</td>
								<td>{{ $model->getType() }}</td>
								<td>{{ optional($model->mainContract)->name }}</td>
								<td>{{ optional($model->mainContract)->email }}</td>
								<td>{{ optional($model->mainContract)->address }}</td>
								<td>{{ optional($model->mainContract)->contact }}</td>
								<td>{{ $model->created_at }}</td>


								<td>
									<a href="{{ route('agent.orders.show',$model->id) }}"
									   class="btn btn-warning waves-effect">
										<i class="material-icons">Info</i>
									</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $models->links() }}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
