<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:56
 * @var $model \App\Models\Booking
 */


?>
@extends('agent.layout')
@section('title') Dashboard Booking View @endsection
@section('content')
	<div class="row">
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header">
					Booking
				</div>
				<div class="card-body">
					@include('admin.booking.info.book-info', ['model'=> $model])
				</div>
			</div>
		</div>

		<div class="col-lg-6">
			<div class="card">
				<div class="card-header">
					Main contract
				</div>
				<div class="card-body">
					@include('admin.booking.info.main-contract', ['model'=> $model->mainContract])
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					Visitors
				</div>
				<div class="card-body">
					@include('admin.booking.info.visitors', ['models'=> $model->guests])
				</div>
			</div>
		</div>

		@if($model->isPackage())
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						Package
					</div>
					<div class="card-body">
						@include('admin.booking.info.package', ['model'=> $model->order])
					</div>
				</div>
			</div>
		@else
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						Custom quote
					</div>
					<div class="card-body">
						@include('admin.booking.info.custom-quote', ['model'=> $model->order])
					</div>
				</div>
			</div>
		@endif
	</div>
@endsection
