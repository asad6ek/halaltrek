<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 22:29
 */
?>

@extends('agent::layout')
@section('title') Dashboard package create @endsection
@section('content')
	@include('admin::package.form',[
	'models'=>$models,
	'method'=>'post',
	'route'=>route('agent.packages.store'),
	'hotelBasisList'=>$hotelBasisList,
	'visitList'=>$visitList,
	'cities'=>$cities,
	'hotels'=>$hotels,
	'cancelLink'=>route('agent.packages.index'),
	])
@endsection
