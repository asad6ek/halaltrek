<?php
/**
 * @var $model \App\DDD\Packages\Models\Package
 */
?>
@extends('agent.layout')
@section('title') Packages @endsection
@section('content')
	<!-- /.row -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<a href="{{ route('agent.packages.create') }}" class="btn btn-success" >Create</a>

				</div>
				<!-- /.card-header -->
				<div class="card-body table-responsive p-0">
					<table class="table table-hover text-nowrap">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Date from</th>
							<th>Date to</th>
							<th>Number of days</th>
							<th>City</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						@foreach($models as $key => $model)
							<tr>
								<td>{{ $models->firstItem() +$key }}</td>
								<td>{{ $model->name }}</td>
								<td>{{ Carbon\Carbon::parse($model->date_from)->format('jS M Y') }}</td>
								<td>{{ Carbon\Carbon::parse($model->date_to)->format('jS M Y') }}</td>
								<td>{{ $model->num_days }}</td>
								<td>{{ optional($model->city)->name }}</td>
								<td>
									<a href="{{ route('agent.packages.edit',$model->id) }}"
									   class="btn btn-info waves-effect">
										<i class="material-icons">Edit</i>
									</a>
									<form action="{{ route('agent.packages.delete', $model->id) }}"
									      method="POST">
										@csrf
										@method('delete')
										<input type="submit" value="Delete" class="btn btn-danger">
									</form>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
                    {!! $models->links() !!}
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
	</div>
	<!-- /.row -->
@endsection
