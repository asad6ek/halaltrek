<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 22:29
 */
?>

@extends('agent::layout')
@section('title') Dashboard package update @endsection
@section('content')
	@include('admin::package.form',[
	'models'=>$models,
	'method'=>'post',
	'route'=>route('agent.packages.update',$id),
	'hotelBasisList'=>$hotelBasisList,
	'visitList'=>$visitList,
	'cities'=>$cities,
	'cancelLink'=>route('agent.packages.index'),
	])
@endsection
