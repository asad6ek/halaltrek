<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-02-17
 * Time: 16:55
 */
?>

@extends('agent.layout')
@section('title') Profile change @endsection
@section('content')
	@include('agent.profile.form',['agent'=>$agent,'method'=>'post', 'route'=>route('agent.profile.index')])
@endsection
