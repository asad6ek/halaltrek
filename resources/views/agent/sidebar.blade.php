@php
    $booking_read = \App\Models\BookingRead::where('status_agent', 0)->count();
@endphp


<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="info">
				<a href="#" class="d-block">{{ optional(Auth::guard('agent')->user())->name }}</a>
				<a class="nav-link text-light btn btn-danger" href="{{ route('agent.logout') }}"><i
							class="fa fa-sign-out fa-lg"></i> Logout</a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
					 with font-awesome or any other icon font library -->
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('agent.packages.index') }}" class="nav-link {{ isActiveRoute('agent.packages') }}">
						<i class="nav-icon fas fa-tachometer-alt"></i>
						<p>
							Packages
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('agent.orders.index') }}" class="nav-link {{ isActiveRoute('agent.orders') }}">
						<i class="nav-icon fas fa-calendar-check"></i>
						<p>
							Orders ({{$booking_read}})
						</p>
					</a>
				</li>
				<li class="nav-item has-treeview menu-open">
					<a href="{{ route('agent.profile.index') }}" class="nav-link {{isActiveRoute('agent.profile')}}">
						<i class="nav-icon fas fa-user-tie"></i>
						<p>
							Profile
						</p>
					</a>
				</li>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
