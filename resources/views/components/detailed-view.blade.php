<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-31
 * Time: 09:54
 */

if (!isset($fields)) {
    throw new Exception('No fields provided');
}
if (!isset($model)) {
    throw new Exception('Model is not provided');
}

$normalizer = new \App\Helpers\NormalizeDetailedViewFields($fields, $model);
$fields = $normalizer->execute();
?>


<table class="table">
	<thead>
	<tr>
		<th>Attribute</th>
		<th>Values</th>
	</tr>
	</thead>
	<tbody>
	@foreach ( $fields as $field)
		<tr>
			<td>
				{{ $field['label'] }}
			</td>
			<td>
				{{ $field['filters']
				? (new \App\Helpers\DetailedViewFilter($field['value'], $field['filters']))->execute()
				: $field['value']}}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
