<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-24
 * Time: 22:29
 * @var $review \App\Models\Review
 */
?>

<div>
	Please verify your email in order to activate your review. <br/>
	verify link: <a href="{{ route('review.verify', $token) }}">
		{{ route('review.verify', $token) }}
	</a>


</div>
