<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 12:14
 */
?>
@extends('templates.main.app')

@section('content')


    <!-- HEADER -->
    <header class="header quote-header">
        <div class="banner quote-banner lg">
            <img src="{{asset("/images/Layer_280850 (1).png")}}" alt="">
        </div>
        <div class="banner quote-banner md">
            <img src="{{asset("/images/Layer_40.png")}}" alt="">
        </div>
        <div class="container">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="/">
                        <img src="{{asset("/images/Layer_280849.png")}}" alt="">
                    </a>
                </div>
                <a href="{{route('booking.custom')}}" class="btn btn-quote {{isActiveRoute('booking.custom',true)}}">
                    Give me a quote
                </a>
            </div>

        </div>
    </header>
    <!-- END HEADER -->

    <!-- MAIN CONTENT -->
    <main>
        <section class="section request-area">
            <div class="container">
                <div class="d-flex request-area-box">
                    <div class="collapse-block">
                        <div class="request-area__content">
                            <h3 class="book-area__box-title">
                                Custom Request
                            </h3>
                            <p class="book-area__box-text">
                                Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus ut vestibulum metus, vel
                                laoreet arcu. Mauris aliquet ornare massa et euismod tincidunt massa Lorem ipsum
                                dolor sit amet, consectetur.
                            </p>
                        </div>
                        <div class="request-area__collapse">

                            <div class="wrapper center-block">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <form class="" id="contact-form"
                                          action="{{ route('booking.custom') }}" method="POST">
                                        @include('partials.formErrors')
                                        @csrf
                                        <div class="panel panel-default">
                                            <div class="panel-heading active" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseOne" aria-expanded="true"
                                                       aria-controls="collapseOne">
                                                        <span class="collapse__number">01</span>Your Details
                                                    </a>
                                                </h4>
                                            </div>

                                            <div id="collapseOne" class="panel-collapse collapse in show"
                                                 role="tabpanel"
                                                 aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    <div class="contact-form">


                                                        <div class="row contact-form_box">
                                                            <div class="col">
                                                                <label for="name">Full Name</label>
                                                                <input type="text" class="form-control"
                                                                       placeholder="Full name" name="contract[name]"
                                                                       id="name"
                                                                       value="{{ old('contract.name')  }}" required>
                                                            </div>

                                                            <div class="col">
                                                                <label for="contact-address">Adress</label>
                                                                <input class="form-control w-100" type="text"
                                                                       name="contract[address]" id="contact-address"
                                                                       placeholder="Enter Your adress"
                                                                       value="{{ old('contract.address')  }}" required>
                                                            </div>
                                                        </div>

                                                        <div class="row contact-form_box">
                                                            <div class="col">
                                                                <label for="contact-contact">Phone Number</label>
                                                                <input class="form-control w-100"
                                                                       type="text" placeholder="Enter Your Phone Number"
                                                                       name="contract[contact]" id="contact-contact"
                                                                       value="{{ old('contract.contact')  }}" required>
                                                            </div>

                                                            <div class="col">
                                                                <label for="contact-group">Group Contact</label>
                                                                <input class="form-control w-100" id="contact-group"
                                                                       type="text" name="contract[group_contact]"
                                                                       placeholder="Enter Your Group Contact"
                                                                       value="{{ old('contract.group_contact')  }}"
                                                                       required>
                                                            </div>
                                                        </div>

                                                        <div class="row contact-form_box">
                                                            <div class="col">
                                                                <label for="contact-email">Email</label>
                                                                <input class="form-control w-100" type="text"
                                                                       name="contract[email]" id="contact-email"
                                                                       placeholder="Enter Your Email Address"
                                                                       value="{{ old('contract.email')  }}" required>
                                                            </div>

                                                            <div class="col">
                                                                <label for="contact-password">Password</label>
                                                                <input class="form-control w-100"
                                                                       type="password" name="contract[password]"
                                                                       id="contact-password"
                                                                       placeholder="Enter Your adress"
                                                                       value="{{ old('contract.password')  }}" required>
                                                            </div>
                                                        </div>

                                                        <div class="row contact-form_box">
                                                            <div class="col">
                                                                <label for="request">Special Request</label>
                                                                <textarea class="form-control w-100"
                                                                          type="text" name="special_req"
                                                                          id="contact-special_req"
                                                                          placeholder="Special Request"
                                                                >{{ old('special_req') }}</textarea>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading active" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapseTwo" aria-expanded="true"
                                                       aria-controls="collapseTwo">
                                                        <span class="collapse__number">02</span>Details of trip
                                                    </a>
                                                </h4>
                                            </div>

                                            <div id="collapseTwo" class="panel-collapse collapse in show"
                                                 role="tabpanel"
                                                 aria-labelledby="headingTwo">
                                                <div class="panel-body">

                                                        @include('main.booking.custom-quote',[
                                                                  'ziyarahOptions'=>$ziyarahOptions,
                                                                  'firstVisitOptions'=>$firstVisitOptions
                                                                  ])

                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse"
                                                       data-parent="#accordion" href="#collapseThree"
                                                       aria-expanded="false"
                                                       aria-controls="collapseThree">
                                                        <span class="collapse__number">03</span>Accompanying travellers
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="headingThree">
                                                <div class="panel-body">
                                                    <fieldset>
                                                        <div class="container-visitors">
                                                            @if(old('visitor',[]))
                                                                @foreach(old('visitor',[]) as $key=> $visitor)
                                                                    @include('main.booking.visotor-form',['visitor'=>$visitor, 'key'=>$key])
                                                                @endforeach
                                                            @else
                                                                @include('main.booking.visotor-form',['key'=>0])

                                                            @endif
                                                        </div>
                                                        <div class="add_visitor btn-success btn">
                                                            Add visitor
                                                        </div>
                                                    </fieldset>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex justify-content-between align-items-center">
                                            <a href="{{route('main')}}" class="back-btn d-flex align-items-center"><img
                                                    src="{{asset("/images/Shape_513.png")}}" alt="">Back</a>
                                            <button class="btn btn-book">
                                                Book Now
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-block">
                        <img src="{{asset("/images/Layer_280877.png")}}" alt="">
                        <h5 class="side_bar_title">Need Help?</h5>
                        <ul class="side_bar">
                            <li class="side_bar_item">
								<span class="side_bar-icon">
									<img src="{{asset("/images/Shape_514.png")}}" alt="">
								</span>
                                <p class="side_bar_item-text">
                                    EMAIL:<a href="#">info@halaltrekgmail.com</a>
                                </p>
                            </li>
                            <li class="side_bar_item">
								<span class="side_bar-icon tel_icon">
									<img src="{{asset("/images/Layer_280878 (1).png")}}" alt="">
								</span>
                                <p class="side_bar_item-text tel_number">
                                    PHONE:<a href="tel:+90 (9876) 543 - 210">+90 (9876) 543 - 210</a>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- END MAIN CONTENT -->
@endsection


@push('scripts')
    <div class="template " style="display: none">
        @include('main.booking.visotor-form',['key'=>'{$i}'])
    </div>

    <script src="{{ asset('backend/js/dynamicForm.js') }}"></script>
    <script>
        $('.date-input').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: parseInt(moment().format('YYYY'), 10),
            locale: {
                format: 'DD-MM-YYYY',
            }
        });

        $('.birthday-input').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1921,
            maxYear: parseInt(moment().format('YYYY'), 10),
            autoUpdateInput: false,
            locale: {
                format: 'DD-MM-YYYY',
                cancelLabel: 'Clear'
            }
        });

        $('.birthday-input').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('blur');
        });

        $('.birthday-input').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    </script>

    <script>
        var dynamicFlights1 = new DynamicForm({
            count_block: "1",
            parent_block: ".container-visitors",
            add_button: '.add_visitor',
            copy_block: '.template',
            del_button: '.remove_visitor',
            block: '.visitor',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 999,
            min: 1,
            onAdd: function (index, element) {
                $('#visitor-' + index + '-passport_expire').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'DD-MM-YYYY'
                    }
                });

                $('#visitor-' + index + '-birth_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    autoUpdateInput: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        cancelLabel: 'Clear'
                    }
                });


                $('#visitor-' + index + '-birth_date').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('blur');
                });

                $('#visitor-' + index + '-birth_date').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

                $('#visitor-' + index + '-last_travel').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    autoUpdateInput: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        cancelLabel: 'Clear'
                    }
                });


                $('#visitor-' + index + '-last_travel').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('blur');
                });

                $('#visitor-' + index + '-last_travel').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('').trigger('blur');
                });


                var material = new materialInput({
                    input: '#visitor-' + index + ' .sign-form-control',
                    inputFilled: 'input-filled',
                    inputError: 'input-error',
                    inputDirty: 'input-dirty',
                    inputActive: 'input-active',
                    inputBlur: 'input-blur',
                });

                material.init();

            }
        });

        dynamicFlights1.init();


        var material = new materialInput({
            input: '.sign-form-control',
            inputFilled: 'input-filled',
            inputError: 'input-error',
            inputDirty: 'input-dirty',
            inputActive: 'input-active',
            inputBlur: 'input-blur',
        });

        material.init();


        // never been checkbox
        $(document).on('click', '.never-been-checkbox', function () {
            var input = $(this).parents('.visitor').find('.input-last-travel');


            if ($(this).is(':checked')) {
                $(input).removeAttr('disabled');
                $(input).parent().removeClass('hidden');
                console.log('checked');
            } else {
                $(input).val('');
                $(input).parent().addClass('hidden');

                console.log('unchecked');
            }

        });
    </script>
@endpush

