<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 12:14
 */
?>
@extends('templates.main.app')

@section('content')
    <header class="header quote-header">
        <div class="banner quote-banner lg">
            <img src="{{asset("/images/Layer_280850 (1).png")}}" alt="">
        </div>
        <div class="banner quote-banner md">
            <img src="{{asset("/images/Layer_40.png")}}" alt="">
        </div>
        <div class="container">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="/">
                        <img src="{{asset("/images/Layer_280849.png")}}" alt="">
                    </a>
                </div>
                <a href="{{route('booking.custom')}}"
                   class="btn btn-quote {{isActiveRoute('booking.custom',true)}}">
                    Give me a quote
                </a>
            </div>
        </div>
    </header>

    <main>
        <section class="section request-area">
            <div class="container">
                <div class="d-flex request-area-box">
                    <div class="collapse-block">
                        <div class="request-area__content">
                            <h3 class="book-area__box-title">
                                Book package
                            </h3>

                        </div>
                        <div class="request-area__collapse">

                            <div class="wrapper center-block">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <form action="{{ route('booking.package', $package->id) }}" method="POST">
                                        @include('partials.formErrors')
                                        @csrf
                                        @include('main.booking.booking-form',[
                                            'roomSizes'=>$roomSizes,
                                        ])
                                        <div class="d-flex justify-content-between align-items-center">
                                            <a href="{{route('main')}}" class="back-btn d-flex align-items-center"><img
                                                    src="{{asset("/images/Shape_513.png")}}" alt="">Back</a>
                                            <button class="btn btn-book">
                                                Book Now
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-block">
                        <img src="{{asset("/images/Layer_280877.png")}}" alt="">
                        <h5 class="side_bar_title">Need Help?</h5>
                        <ul class="side_bar">
                            <li class="side_bar_item">
								<span class="side_bar-icon">
									<img src="{{asset("/images/Shape_514.png")}}" alt="">
								</span>
                                <p class="side_bar_item-text">
                                    EMAIL:<a href="#">info@halaltrekgmail.com</a>
                                </p>
                            </li>
                            <li class="side_bar_item">
								<span class="side_bar-icon tel_icon">
									<img src="{{asset("/images/Layer_280878 (1).png")}}" alt="">
								</span>
                                <p class="side_bar_item-text tel_number">
                                    PHONE:<a href="tel:+90 (9876) 543 - 210">+90 (9876) 543 - 210</a>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection


