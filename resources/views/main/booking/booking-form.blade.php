<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 13:14
 */
?>
<fieldset class="scheduler-border">
    <legend>Booking</legend>
    <div class="contact-form">
        <div class="row contact-form_box">
            <div class="col">
                <label for="name">Your Name</label>
                <input type="text" class="form-control"
                       placeholder="Your name" name="booker_name"
                       id="name"
                       value="{{ old('booker_name')  }}" required>
            </div>

        </div>
        <div class="row contact-form_box">
            <div class="col">
                <label for="name">Special request</label>
                <textarea class="form-control w-100" type="text" name="special_req" id="contact-special_req"
                          placeholder="Special request">{{ old('special_req') }}</textarea>
            </div>
        </div>
    </div>

</fieldset>


<fieldset class="scheduler-border visitors">
    <legend>Rooms</legend>

    <div class="contact-form">
        <div class="row">
            <div class="col-12">
                @if(old('rooms',[]))
                    @foreach(old('rooms',[]) as $key=> $room)
                        @include('main.booking.room-size',['room'=>$room, 'key'=>$key])
                    @endforeach
                @else
                    @foreach($roomSizes as $key=> $roomSize)
                        @include('main.booking.room-size',[
                        'room'=>['size'=>$key],
                        'key'=>$loop->index,
                        ])
                    @endforeach
                @endif
                <br>
            </div>
        </div>
    </div>
</fieldset>


<fieldset class="scheduler-border visitors">
    <legend>Contact</legend>

    {{--

        <div class="input-box">
            <input class="form-control sign-form-control" type="text" name="contract[name]" id="contact-name"
                   value="{{ old('contract.name')  }}" required>
             <label class="sign-input-label" for="contact-name">Name</label>
        </div>

        <div class="input-box">
            <input class="form-control sign-form-control" type="text" name="contract[address]" id="contact-address"
                   value="{{ old('contract.address')  }}" required>
             <label class="sign-input-label" for="contact-address">Address</label>

        </div>

        <div class="input-box">
            <input class="form-control sign-form-control" type="text" name="contract[contact]" id="contact-contact"
                   value="{{ old('contract.contact')  }}" required>
             <label class="sign-input-label" for="contact-contact">Telephone/Mobile</label>
        </div>

        <div class="input-box">
            <input class="form-control sign-form-control" type="text" name="contract[group_contact]" id="contact-contact"
                   value="{{ old('contract.group_contact')  }}" required>
             <label class="sign-input-label" for="contact-contact">Group contact</label>
        </div>

        <div class="input-box">
            <input class="form-control sign-form-control" type="text" name="contract[email]" id="contact-email"
                   value="{{ old('contract.email')  }}" required>
             <label class="sign-input-label" for="contact-email">Email</label>

        </div>


        <div class="input-box">
            <input class="form-control sign-form-control" type="password" name="contract[password]" id="contact-password"
                   value="{{ old('contract.password')  }}" required>
             <label class="sign-input-label" for="contact-password">Password</label>
        </div>
    --}}

    <div class="container-visitors">
        <div class="contact-form">
            <div class="row contact-form_box">
                <div class="col">
                    <label for="name">Full Name</label>
                    <input type="text" class="form-control"
                           placeholder="Full name" name="contract[name]"
                           id="name"
                           value="{{ old('contract.name')  }}" required>
                </div>

                <div class="col">
                    <label for="contact-address">Adress</label>
                    <input class="form-control w-100" type="text"
                           name="contract[address]" id="contact-address"
                           placeholder="Enter Your adress"
                           value="{{ old('contract.address')  }}" required>
                </div>
            </div>

            <div class="row contact-form_box">
                <div class="col">
                    <label for="contact-contact">Phone Number</label>
                    <input class="form-control w-100"
                           type="text" placeholder="Enter Your Phone Number"
                           name="contract[contact]" id="contact-contact"
                           value="{{ old('contract.contact')  }}" required>
                </div>

                <div class="col">
                    <label for="contact-group">Group Contact</label>
                    <input class="form-control w-100" id="contact-group"
                           type="text" name="contract[group_contact]"
                           placeholder="Enter Your Group Contact"
                           value="{{ old('contract.group_contact')  }}"
                           required>
                </div>
            </div>

            <div class="row contact-form_box">
                <div class="col">
                    <label for="contact-email">Email</label>
                    <input class="form-control w-100" type="text"
                           name="contract[email]" id="contact-email"
                           placeholder="Enter Your Email Address"
                           value="{{ old('contract.email')  }}" required>
                </div>

                <div class="col">
                    <label for="contact-password">Password</label>
                    <input class="form-control w-100"
                           type="password" name="contract[password]"
                           id="contact-password"
                           placeholder="Enter Your adress"
                           value="{{ old('contract.password')  }}" required>
                </div>
            </div>

        </div>
    </div>

</fieldset>


<fieldset class="scheduler-border visitors">
    <legend>Accompanying travellers</legend>
    <div class="container-visitors">
        @if(old('visitor',[]))
            @foreach(old('visitor',[]) as $key=> $visitor)
                @include('main.booking.visotor-form',['visitor'=>$visitor, 'key'=>$key])
            @endforeach
        @else
            @include('main.booking.visotor-form',['key'=>0])

        @endif
    </div>
    <div class="add_visitor btn-success btn">
        Add visitor
    </div>
</fieldset>




@push('scripts')
    <div class="template " style="display: none">
        @include('main.booking.visotor-form',['key'=>'{$i}'])
    </div>

    <div class="template-room" style="display: none">
        @include('main.booking.room-size',['key'=>'{$i}'])
    </div>

    <script src="{{ asset('backend/js/dynamicForm.js') }}"></script>
    <script>
        $('.date-input').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: parseInt(moment().format('YYYY'), 10),
            locale: {
                format: 'DD-MM-YYYY'
            }
        });


        $('.birthday-input').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1921,
            maxYear: parseInt(moment().format('YYYY'), 10),
            autoUpdateInput: false,
            locale: {
                format: 'DD-MM-YYYY',
                cancelLabel: 'Clear'
            }
        });

        $('.birthday-input').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('.birthday-input').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });
    </script>

    <script>
        var dynamicFlights1 = new DynamicForm({
            count_block: "1",
            parent_block: ".container-visitors",
            add_button: '.add_visitor',
            copy_block: '.template',
            del_button: '.remove_visitor',
            block: '.visitor',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 999,
            min: 1,
            onAdd: function (index, element) {
                $('#visitor-' + index + '-passport_expire').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    locale: {
                        format: 'DD-MM-YYYY'
                    }
                });

                $('#visitor-' + index + '-birth_date').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    autoUpdateInput: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        cancelLabel: 'Clear'
                    }
                });


                $('#visitor-' + index + '-birth_date').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('blur');
                });

                $('#visitor-' + index + '-birth_date').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('').trigger('blur');
                });

                $('#visitor-' + index + '-last_travel').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 1921,
                    maxYear: parseInt(moment().format('YYYY'), 10),
                    autoUpdateInput: false,
                    locale: {
                        format: 'DD-MM-YYYY',
                        cancelLabel: 'Clear'
                    }
                });


                $('#visitor-' + index + '-last_travel').on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('DD-MM-YYYY')).trigger('blur');
                });

                $('#visitor-' + index + '-last_travel').on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('').trigger('blur');
                });


                var material = new materialInput({
                    input: '#visitor-' + index + ' .sign-form-control',
                    inputFilled: 'input-filled',
                    inputError: 'input-error',
                    inputDirty: 'input-dirty',
                    inputActive: 'input-active',
                    inputBlur: 'input-blur',
                });

                material.init();

            }
        });

        dynamicFlights1.init();


        var material = new materialInput({
            input: '.sign-form-control',
            inputFilled: 'input-filled',
            inputError: 'input-error',
            inputDirty: 'input-dirty',
            inputActive: 'input-active',
            inputBlur: 'input-blur',
        });

        material.init();


        // never been checkbox
        $(document).on('click', '.never-been-checkbox', function () {
            var input = $(this).parents('.visitor').find('.input-last-travel');


            if ($(this).is(':checked')) {
                $(input).removeAttr('disabled');
                $(input).parent().removeClass('hidden');
                console.log('checked');
            } else {
                $(input).val('');
                $(input).parent().addClass('hidden');

                console.log('unchecked');
            }

        });


        var dynamicRoom = new DynamicForm({
            count_block: "1",
            parent_block: ".container-rooms",
            add_button: '.add_room',
            copy_block: '.template-room',
            del_button: '.remove_room',
            block: '.room',
            replaceItem: '\\{\\$i\\}',
            removeCopyButton: false,
            delConfirm: true,
            max: 999,
            min: 1,
            onAdd: function (index) {

                var material = new materialInput({
                    input: '#room-' + index + ' .sign-form-control',
                    inputFilled: 'input-filled',
                    inputError: 'input-error',
                    inputDirty: 'input-dirty',
                    inputActive: 'input-active',
                    inputBlur: 'input-blur',
                });

                material.init();
            }
        });

        dynamicRoom.init();
    </script>
@endpush
