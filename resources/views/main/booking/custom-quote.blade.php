<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 21:37
 */

$flightTypes = \App\Models\CustomQuote::flightTypes();

$hotelTypes = \App\Models\CustomQuote::hotelTypes();
?>

<fieldset>


	<fieldset class="scheduler-border flights">
		<legend>Flights</legend>

        <div class="contact-form">

            <div class="row contact-form-review">
                <div class="col">
                    <label for="flight-checkbox" class="zafa-sokkichi">
                        <input type="hidden" value="0"
                               name="flight[has_flight]">
                        <input type="checkbox" id="flight-checkbox" class="" value="1"
                               name="flight[has_flight]" @if(old('flight.has_flight')) checked @endif>
                        Do you have a specific flight you would like?
                    </label>
                </div>
            </div>


            <div class="form-group flight-type-holder  @if(old('flight.has_flight',0)) hidden @endif">
                <div class="fordatepicker bg-white form-group">
                    <div class="field">
                        <label for="flight-type">Flight </label>
                        <select name="flight[type]" id="flight-type"
                                class="js-example-placeholder-single js-states form-control">
                            @foreach($flightTypes as $key => $size)
                                <option @if(old('flight.type') == $key )selected
                                        @endif value="{{ $key }}">{{ $size }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="input-box flight-detail-holder  @if(!old('flight.has_flight',0)) hidden @endif">
			<textarea class="form-control sign-form-control" type="text" name="flight[detail]" id="flight-detail"
                      placeholder="Please give details of Airline, Date and time">{{ old('flight.detail') }}</textarea>
            </div>
        </div>
    </fieldset>
<br>

    <fieldset class="scheduler-border hotels">
        <legend>Hotels</legend>
<div class="contact-form">
        <div class="contact-form-review">
            <label for="hotel-has_hotels" class="zafa-sokkichi">
                <input type="hidden" value="0"
                       name="hotel[has_hotels]">
                <input type="checkbox" id="hotel-has_hotels" class="" value="1"
                       name="hotel[has_hotels]" @if(old('hotel.has_hotels')) checked @endif>
                Do you have a preference of hotels?
            </label>
        </div>

        <div class="form-group choose_hotels @if(!old('hotel.has_hotels',0)) hidden @endif">
            <div class="field bg-white form-group">
                <label for="hotel-search">Hotel in Makkah</label>
                <select class="js-example-placeholder-single js-states form-control"
                        id="custom-hotel_makkah"
                        name="hotel[hotel_makkah]">
                    @foreach($hotelsInMakkah as $key => $size)
                        <option @if(old('hotel.hotel_makkah') == $key )selected
                                @endif value="{{ $key }}">{{ $size }}</option>
                    @endforeach

                </select>
            </div>

            <div class="field bg-white form-group">
                <label for="hotel-search">Hotel in Madinah</label>
                <select class="js-example-placeholder-single js-states form-control"
                        id="custom-hotel_madinah"
                        name="hotel[hotel_madinah]">
                    @foreach($hotelsInMadinah as $key => $size)
                        <option @if(old('hotel.hotel_madinah') == $key )selected
                                @endif value="{{ $key }}">{{ $size }}</option>
                    @endforeach

                </select>
            </div>

        </div>


        <div class="field bg-white hotel_type @if(old('hotel.has_hotels',0)) hidden @endif">
            <label for="hotel-search">Hotel type</label>
            <select class="js-example-placeholder-single js-states form-control"
                    name="hotel[type]" id="hotel-type">
                @foreach($hotelTypes as $key => $size)
                    <option @if(old('hotel.type') == $key )selected
                            @endif value="{{ $key }}">{{ $size }}</option>
                @endforeach

            </select>
        </div>

</div>
    </fieldset>
    <br>
    <fieldset class="scheduler-border Rooms">
        <legend>Rooms</legend>
        <div class="contact-form">
                @if(old('rooms',[]))
                    @foreach(old('rooms',[]) as $key=> $room)
                        @include('main.booking.room-size',['room'=>$room, 'key'=>$key])
                    @endforeach
                @else
                    @foreach($roomSizes as $key=> $roomSize)
                        @include('main.booking.room-size',[
                        'room'=>['size'=>$key],
                        'key'=>$loop->index,
                        ])
                    @endforeach
                @endif
        <br>
        </div>
    </fieldset>
    <br>

    <fieldset class="scheduler-border details">
        <legend>Other details</legend>
        <div class="contact-form">

            <div class="row contact-form_box">
                <div class="col">
                    <label for="custom-nights_makka">Nights in Makka</label>
                    <input class="form-control w-100"
                           type="text" name="custom[nights_makkah]"
                           id="custom-nights_makka" placeholder="Nights in Makka"
                           value="{{ old('custom.nights_makkah')  }}" required>
                </div>


                <div class="col">
                    <label for="custom-nights_madinah">Nights in Madinah</label>
                    <input class="form-control w-100" type="text" name="custom[nights_madinah]"
                           id="custom-nights_madinah" placeholder="Nights in Madinah"
                           value="{{ old('custom.nights_madinah') }}" required>
                </div>
            </div>

            <div class="row contact-form_box">
                <div class="col">
                    <label for="custom-depart">Departure date</label>
                    <input class="form-control w-100 date-input" type="text" name="custom[depart]"
                           id="custom-depart" placeholder="Departure date"
                           value="{{ old('custom.depart') }}" required>
                </div>

                <div class="col">
                    <label for="custom-return">Return date</label>
                    <input class="form-control w-100 date-input" type="text" name="custom[return]"
                           id="custom-return" placeholder="Return date"
                           value="{{ old('custom.return') }}" required>
                </div>

            </div>
            <div class="row contact-form_box">
                <div class="col">
                    <label for="custom-comment">Comment</label>
                    <textarea class="form-control w-100" type="text" name="custom[comment]" id="custom-comment"
                    >{{ old('custom.comment')  }}</textarea>

                </div>
            </div>


            <div class="row contact-form-review">
                <div class="col">
                    <label for="custom-transfers" class="zafa-sokkichi">
                        <input type="hidden" value="0"
                               name="custom[transfers]">
                        <input type="checkbox" id="custom-transfers" class="" value="1"
                               name="custom[transfers]" @if(old('custom.transfers')) checked @endif>
                        Would you like private transfers?
                    </label>
                </div>
            </div>
        </div>
    </fieldset>
</fieldset>

@push('scripts')
    <script>
        $(".js-states").select2({
            placeholder: "Please select",
            allowClear: true
        });

        // $("#first_visit").select2({
        //     placeholder: "Please select",
        //     allowClear: true
        // });

        // $("#custom-hotel_makkah").select2({
        //     placeholder: "Please select",
        //     allowClear: true
        // });
        //
        // $("#custom-hotel_madinah").select2({
        //     placeholder: "Please select",
        //     allowClear: true
        // });

        $('#flight-checkbox').click(function (e) {
            if ($(this).is(':checked')) {
                $('.flight-detail-holder').removeClass('hidden');
                $('.flight-type-holder').addClass('hidden');
            } else {
                $('.flight-detail-holder').addClass('hidden');
                $('.flight-type-holder').removeClass('hidden');
            }
        });


        $('#hotel-has_hotels').click(function (e) {
            if ($(this).is(':checked')) {
                $('.choose_hotels').removeClass('hidden');
                $('.hotel_type').addClass('hidden');
            } else {
                $('.hotel_type').removeClass('hidden');
                $('.choose_hotels').addClass('hidden');
            }
        });


    </script>
@endpush
