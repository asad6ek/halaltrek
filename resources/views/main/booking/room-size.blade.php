<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-29
 * Time: 15:59
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-19
 * Time: 18:02
 */

if (!isset($room)) {
    $room = [];
}
?>
<fieldset id="room-{{$key}}">
    <div class="row contact-form_box">
        <div class="col">
            <label for="room-{{$key}}-number">Number of rooms</label>
            <input class="form-control w-100" type="text" name="rooms[{{$key}}][number]"
                   id="room-{{$key}}-number"
                   value="{{ data_get($room,'number') }}" placeholder="Room size ({{data_get($room,'size')}})"
            >
            <input type="hidden" name="rooms[{{$key}}][size]"
                   value="{{ data_get($room,'size')  }}">
        </div>
    </div>
</fieldset>

