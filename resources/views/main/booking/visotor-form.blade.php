<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-19
 * Time: 18:02
 */

if (!isset($visitor)) {
    $visitor = [];
}
?>


<fieldset class="scheduler-border visitor dynamic-field" id="visitor-{{$key}}">
    @if($key !== 0)
        <div class="remove_visitor remove-button btn btn-danger">
            -
        </div>
    @endif
    <div class="contact-form">

        <div class="row contact-form_box">
            <div class="col">
                <label for="contact-name">Full name (as it appears on passport)</label>
                <input class="form-control" type="text" name="visitor[{{$key}}][name]" id="contact-name"
                       value="{{ data_get($visitor,'name')  }}" required>
            </div>
        </div>

        <div class="row contact-form_box">
            <div class="col">
                <label for="visitor-{{$key}}-passport_expire">Passport expire date</label>
                <input class="form-control date-input" type="text"
                       name="visitor[{{$key}}][passport_expire]"
                       id="visitor-{{$key}}-passport_expire"
                       value="{{ data_get($visitor,'passport_expire')  }}" required>


            </div>

            <div class="col">
                <label for="visitor-{{$key}}-birth_date">Birth date</label>
                <input class="form-control birthday-input" type="text"
                       name="visitor[{{$key}}][birth_date]"
                       id="visitor-{{$key}}-birth_date"
                       value="{{ data_get($visitor,'birth_date')  }}" required>


            </div>
        </div>

{{--        <div class="row contact-form_box">--}}
{{--            <div class="col">--}}
{{--                <label for="visitor-{{$key}}-relation">Relation</label>--}}
{{--                <input class="form-control" type="text" name="visitor[{{$key}}][relation]"--}}
{{--                       id="visitor-{{$key}}-relation"--}}
{{--                       value="{{ data_get($visitor,'relation')  }}" required>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group">--}}
{{--            <label for="never-been-{{$key}}-checkbox" class="zafa-sokkichi">--}}
{{--                <input type="checkbox" id="never-been-{{$key}}-checkbox" class="never-been-checkbox hide" value="1"--}}
{{--                       name="visitor[{{$key}}][last_travel_input]"--}}
{{--                       @if(data_get($visitor,'last_travel')) checked @endif>--}}
{{--                Have you been to Hajj/Umrah previously?--}}
{{--            </label>--}}
{{--        </div>--}}

{{--        <div class="row contact-form_box">--}}
{{--            <div class="col @if( !data_get($visitor,'last_travel_input')) hidden @endif">--}}
{{--                <label for="visitor-{{$key}}-last_travel">Date</label>--}}
{{--                <input class="form-control birthday-input input-last-travel" type="text"--}}
{{--                       name="visitor[{{$key}}][last_travel]"--}}
{{--                       id="visitor-{{$key}}-last_travel"--}}
{{--                       value="{{ data_get($visitor,'last_travel')  }}">--}}

{{--            </div>--}}
{{--        </div>--}}
    </div>
</fieldset>
