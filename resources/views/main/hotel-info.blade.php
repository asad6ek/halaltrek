<div class="col-lg-6 galery-block">


    @php
        $images = $hotel->images;
        $first_image = $images[0]??null;
        if ($first_image){
            unset($images[0]);
        }
    @endphp

    @if($images)
        <div class="galery d-flex">
            @if($first_image)
                <div class="first-thumbnail">
                    <a class="fancybox" rel="gallery1"
                       href="{{ asset($first_image->getFullPath()) }}" title="{{ $hotel->name }}">
                        <img src="{{ asset($first_image->getFullPath()) }}" alt=""/>
                    </a>
                </div>
            @endif
            <div class="d-flex flex-column">
                @foreach($images as $key => $image)

                    @if($key !==3 )
                        <a class="fancybox @if($key > 3) d-none @endif" rel="gallery1"
                           href="{{ asset($image->getFullPath()) }}"
                           title="{{ $hotel->name }}">
                            <img src="{{ asset($image->getFullPath()) }}" alt=""/>
                        </a>
                    @else
                        <div class="last-thumbnail">
                            <div class="show-more-galery">
                                <img src="images/Layer_280857.png" alt="">
                                <span>10 More Phone</span>
                            </div>
                            <a class="fancybox" rel="gallery1"
                               href="{{ asset($image->getFullPath()) }}"
                               title="{{ $hotel->name }}">
                                <div class="show-more-galery">
                                    <img src="images/Layer_280857.png" alt="">
                                    <span>10 More Phone</span>
                                </div>

                                <img src="{{ asset($image->getFullPath()) }}" alt=""/>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    @endif

    <div class="galery-content">
        <div class="d-flex justify-content-between align-items-center">
            <h5 class="galery-content__title-city d-flex justify-content-center align-items-center">
                <span><img src="{{asset("images/Layer_280877 (1).png")}}" alt=""></span>{{$mainName}}
            </h5>
            <div class="stars">

                <i class="fa fa-star @if($hotel->star <1) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($hotel->star <2) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($hotel->star <3) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($hotel->star <4) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($hotel->star <5) disable-star @endif" aria-hidden="true"></i>
            </div>
        </div>
        <div class="">
            <h3 class="galery-content_title">
                {{$hotel->name}}
            </h3>
            <p class="galery-content_text">
                {{$hotel->description}}
            </p>
        </div>
        <!--  -->
        <div class="aminities-list-box">
            <h5>Main amenities</h5>
            <ul class="aminities-list">
                @forelse($hotel->amenities as $amenity)
                    <li class="aminities-list_item"><span><img src="{{asset($amenity->label)}}"
                                                               alt=""></span> {{$amenity->title}}</li>
                @empty

                @endforelse


            </ul>
        </div>
    </div>
</div>
