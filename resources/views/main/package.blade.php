<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-21
 * Time: 17:36
 * @var $package \App\DDD\Packages\Models\Package
 */
?>
@extends('templates.main.app')

@section('content')
    <!-- HEADER -->
    <header class="header package-header">
        <div class="banner package-banner lg">
            <img src="{{asset("images/5-layers.jpg")}}" alt="">
        </div>
        <div class="banner package-banner md">
            <img src="{{asset("images/5-layers (1).png")}}" alt="">
        </div>
        <div class="container">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="{{route('main')}}">
                        <img src="{{asset("images/Layer_280849.png")}}" alt="">
                    </a>
                </div>
                <a href="{{route('booking.custom')}}" class="btn btn-quote {{isActiveRoute('booking.custom',true)}}">
                    Give me a quote
                </a>
            </div>
            <h1 class="banner-title d-flex flex-column text-center">
				<span>
					{{ ucfirst(strtolower($package->getType())) }} package
				</span>
                Journey of a lifetime
            </h1>
            <!-- <div class="banner-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">HAJJ</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">UMRAH</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">TOURS</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="tab-pane__content">
                            <p>- Cities</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please Choose your city">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content tab-pane__content-middle">
                            <p>- Duration</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose duration">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content">
                            <p>- Sort</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose class">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-search">
                            Search
                        </button>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="tab-pane__content">
                            <p>- Cities</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please Choose your city">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content tab-pane__content-middle">
                            <p>- Duration</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose duration">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content">
                            <p>- Sort</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose class">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-search">
                            Search
                        </button>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="tab-pane__content">
                            <p>- Cities</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please Choose your city">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content tab-pane__content-middle">
                            <p>- Duration</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose duration">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <div class="tab-pane__content">
                            <p>- Sort</p>
                            <div class="selectpicker__box">
                                <select class="citis-selectpicker" multiple title="Please choose class">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-search">
                            Search
                        </button>
                    </div>
                </div>
            </div> -->
        </div>
    </header>
    <!-- END HEADER -->

    <!-- MAIN CONTENT -->
    <main class="home-main">
        <div class="package_book-area">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="area__title">{{ $package->name }}</h3>
                    <a href="{{ route('booking.package', $package->id)}}" class="btn btn-book">Book Now</a>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="container">
                <div class="d-flex price-block justify-content-between">
                    <article class="price-box d-flex align-items-center">
                        <div class="price-box__thumbnail"><img src="{{asset("images/Layer_280884 (1).png")}}" alt="">
                        </div>
                        <div class="price-box__content">
                            <h5 class="price-box__content-title">Quad (4 Sharing)</h5>
                            <h3 class="price">{!! $package->priceShow() !!}</h3>
                            <span class="price-box__content-per">Per Person</span>
                        </div>
                    </article>

                    <article class="price-box d-flex align-items-center">
                        <div class="price-box__thumbnail"><img src="/images/Layer_280884 (1).png" alt=""></div>
                        <div class="price-box__content">
                            <h5 class="price-box__content-title">Quad (4 Sharing)</h5>
                            <h3 class="price">{!! $package->priceShow() !!}</h3>
                            <span class="price-box__content-per">Per Person</span>
                        </div>
                    </article>

                    <article class="price-box d-flex align-items-center">
                        <div class="price-box__thumbnail"><img src="/images/Layer_280884 (1).png" alt=""></div>
                        <div class="price-box__content">
                            <h5 class="price-box__content-title">Quad (4 Sharing)</h5>
                            <h3 class="price">{!! $package->priceShow() !!}</h3>
                            <span class="price-box__content-per">Per Person</span>
                        </div>
                    </article>
                </div>
                <p class="info_text">
                    {{ $package->description}}

                    @php

                        $option = $package->option;
                        $breaktast =        isset($option)? $option->breakfast :        null;
                        $seminar =          isset($option)? $option->seminar :          null;
                        $goody_bag =        isset($option)? $option->goody_bag :        null;
                        $ground_transport = isset($option)? $option->ground_transport : null;
                        $hadiy =            isset($option)? $option->hadiy :            null;
                        $visit =            isset($option)? $option->visit :            null;

                    @endphp
                </p>
                <div class="row cart-block">
                    <div class="col-lg-6 d-flex cart-block__box selecting flex-wrap">
                        <article
                            class="cart-block_item @if($package->is_shifting) active @endif align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280909.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Shifting and
                                unshifting
                            </h5>
                        </article>
                        <article
                            class="cart-block_item @if($breaktast) active @endif align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280910.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Breakfast full
                                and half
                            </h5>
                        </article>
{{--                        @dd($package)--}}
                        <article class="cart-block_item @if($package->flights) active @endif align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280913.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Direct and
                                Indirect
                            </h5>
                        </article>
                        <article class="cart-block_item active align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280912.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Day in makkah
                                days in madina
                            </h5>
                        </article>
                    </div>

                    <div class="col-lg-6 d-flex cart-block__box selecting flex-wrap">
                        <article class="cart-block_item active @if($visit) active @endif align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280897.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Visit
                            </h5>
                        </article>
                        <article class="cart-block_item  @if($ground_transport) active @endif  align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280898.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Ground transport
                            </h5>
                        </article>
                        <article class="cart-block_item  @if($hadiy) active @endif  align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280901.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Hadiy
                            </h5>
                        </article>
                        <article class="cart-block_item  @if($breaktast) active @endif  align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280900.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Pre-umrah seminar
                            </h5>
                        </article>
                        <article class="cart-block_item  @if($goody_bag) active @endif  align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280902.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Goody bag
                            </h5>
                        </article>
                        <article
                            class="cart-block_item @if($package->ziyorah_included) active @endif align-items-center d-flex">
                            <div class="cart-block_item-thumbnail">
                                <img src="{{asset("images/Layer_280899.png")}}" alt="">
                            </div>
                            <h5 class="cart-block_item-text">
                                Ziyarah in Madinah
                            </h5>
                        </article>
                    </div>
                </div>
                <div class="flight-info__block">
                    <h3>
                        FLIGHT INFO
                    </h3>
                    @foreach($package->flights  as $flight)
                        {{--                        @dd($flight->airline)--}}
                        <div class="container-fluid">
                            <div class="row justify-content-between flight-info__block-area">
                                <div class="col-lg-6 flight-info__block-box">
                                    <div class="packages-block__box-info border-box-info d-flex align-items-center">
                                        <div class="info-flying text-center">
                                            @if(optional($flight->airline)->getUrl())
                                                <img src="{{ asset($flight->airline->getUrl())}}" alt="">
                                            @else
                                                <img src="{{ asset('frontend/photo/image.png')}}" alt="">
                                            @endif

                                            <h5>{{$flight->airline->name}}</h5>
                                        </div>
                                        <div class="info-flying__box">
                                            <div class="info-flying__city d-flex align-items-center">
                                                <h5>{{ $flight->from }}</h5>
                                                <div class="flying__image-box">
												<span class="flying__image">
													<img src="{{asset("images/Layer_280855.png")}}" alt="">
												</span>
                                                    <span class="flying__image-dote"></span>
                                                </div>
                                                <h5>{{ $flight->to }}</h5>
                                            </div>
                                            <div class="info-flying__city d-flex">
                                                <p>London
                                                    Heathrow</p>
                                                <span class="flying__time">
												3h 55
											</span>
                                                <p>King
                                                    Abdulaziz
                                                    International</p>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="flight-info__date d-flex justify-content-center align-items-center">
                                        <p>
                                            {{ $flight->dateFrom() }}
                                        </p>
                                    </div>
                                </div>

                                <div class="col-lg-6 flight-info__block-box">
                                    <div class="packages-block__box-info border-box-info d-flex align-items-center">
                                        <div class="info-flying text-center">
                                            @if(optional($flight->airline)->getUrl())
                                                <img src="{{ asset($flight->airline->getUrl())}}" alt="">
                                            @else
                                                <img src="{{ asset('frontend/photo/image.png')}}" alt="">
                                            @endif

                                            <h5>{{$flight->airline->name}}</h5>
                                        </div>

                                        <div class="info-flying__box">
                                            <div class="info-flying__city d-flex align-items-center">
                                                <h5>   {{ $flight->to }}</h5>
                                                <div class="flying__image-box">
												<span class="flying__image">
													<img src="{{asset("images/Layer_280855.png")}}" alt="">
												</span>
                                                    <span class="flying__image-dote"></span>
                                                </div>
                                                <h5>{{ $flight->from }}</h5>
                                            </div>
                                            <div class="info-flying__city d-flex">
                                                <p>London
                                                    Heathrow</p>
                                                <span class="flying__time">
												3h 55
											</span>
                                                <p>King
                                                    Abdulaziz
                                                    International</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flight-info__date d-flex justify-content-center align-items-center">
                                        <p>
                                            {{$flight->dateTo()}}
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="container-fluid">
                    <div class="row justify-content-between galery-block-area">


                        @php
                            $hotelMakkah = optional($package->getHotelForMakka())->hotel;
                            $hotelMADINAH = optional($package->getHotelForMadina())->hotel;
                        @endphp
                        @include('main.hotel-info',['hotel'=>$hotelMakkah, 'mainName' => 'MAKKAH'])

                        @include('main.hotel-info',['hotel'=>$hotelMADINAH, 'mainName' => 'MADINA'])
                    </div>
                </div>
                @php
                    $agent = $package->agent;
                @endphp

                <div class="travel-info_block">
                    <div class="d-flex travel-info_block-box">

                        <div class="travel-info_block-reviews-box">
                            <div class="d-flex travel-info_block-reviews-box_head align-items-center">
                                <p class="review-ball">
                                    {{ $package->getReviewAvarage() }}
                                </p>
                                <div class="d-flex align-items-center review-hlp">
                                    <h6>Fabulous</h6>
                                    <span> {{ $package->getReviewCount() }} Reviews</span>
                                </div>
                            </div>
                            <div class="travel-info_block-reviews-box_body">
                                <img src="{{asset($agent->getLogo())}}" alt="">
                            </div>
                        </div>

                        <div class="travel-info_block-content">
                            <h3 class="travel-info_block-content-title">
                                {{ $agent->name }}
                            </h3>
                            @if($agent->info)
                                <p class="travel-info_block-content-text">
                                    {{ $agent->info }}
                                </p>
                            @endif
                            <div class="d-flex travel-info_block-content-bottom">
                                <div class="d-flex align-items-center travel-info_block-content-bottom-item">
                                    <span><img src="{{asset("images/Layer_280893.png")}}" alt=""></span>
                                    <h5 class="d-flex align-items-center">ATOL Protected</h5>
                                    <span><img src="{{asset("images/Layer_280892.png")}}" alt=""> </span>
                                </div>
                                <div class="d-flex align-items-center">
                                    <span><img src="{{asset("images/Layer_280918.png")}}" alt=""></span>
                                    <h5 class="d-flex align-items-center">Ministry of Hajj & Umrah Approved</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion packages-acardion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        View All Reviews <img src="{{asset("images/Shape_512.png")}}" alt="">
                                    </button>
                                </h2>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div
                                    class="card-body d-flex write-review-block justify-content-between align-items-center">
                                    <div class="write-reviews-counter d-flex align-items-center">
                                        <p>
                                            {{ $package->getReviewAvarage() }}
                                        </p>
                                        <div class="write-reviews-counter-text">
                                            <span> {{ $package->getReviewCount() }} Reviews</span>
                                            <span>  {{$agent->getReviewAvarage() * 10}}% of customer recommend this product</span>
                                        </div>
                                    </div>
                                    <div class="accordion write-review-accardion" id="accordionReview">
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <button class="btn btn-write-review" type="button"
                                                        data-toggle="collapse" data-target="#collapseReview"
                                                        aria-expanded="true" aria-controls="collapseReview">
                                                    Write Review
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body write-review-block" >
                                    @foreach($agent->activeReviews as $review )
                                        <div class="row">
                                            <div class="col">
                                                <p>
                                                    {{ $review->comment }}
                                                </p>
                                                <div class="d-flex">
                                                    <div class="user d-flex">
                                                        <div class="user-image">
                                                        </div>
                                                        <div class="user-info">
                                                            <p>{{ $review->name }}</p>
                                                            <span>{{ $review->timeToNow() }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="rew-marks">
                                                        <p>
                                                            <b>
                                                                {{ $review->rate }}
                                                            </b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                <div id="collapseReview" class="collapse" aria-labelledby="headingTwo"
                                     data-parent="#accordionReview">
                                    <div
                                        class="card-body write-review-block-body d-flex justify-content-between align-items-center">
                                        <form class="contact-form-review" id="contact-form-review" action="{{ route('package.review', $package->id) }}" method="post">
                                            @method('POST')
                                            @include('partials.formErrors')
                                            @csrf
                                            <input type="hidden" value="1" name="reviewSend">

                                            <div class="row contact-form_box">
                                                <div class="col">
                                                    <label for="name">Full Name</label>
                                                    <input type="text" value="{{ old('name') }}" class="form-control" placeholder="First name"
                                                           name="name" id="name">
                                                </div>


                                                <div class="col">
                                                    <label for="adress">Email</label>
                                                    <input type="email" class="form-control w-100" value="{{ old('email') }}"
                                                           placeholder="Enter Your Email" name="email" id="adress">
                                                </div>
                                            </div>


                                            <div class="row contact-form_box">
                                                <div class="col">
                                                    <select
                                                        class="form-control w-100 "
                                                        id="package" name="rate">
                                                        @for($i=10; $i>0; $i--)
                                                            <option @if(old('rate') == $i )selected
                                                                    @endif value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>

                                                    <div class="row contact-form_box">
                                                <div class="col">
                                                    <label for="request">Comment</label>
                                                    <textarea class="form-control w-100"
                                                                placeholder="Comment" name="comment" id="request"
                                                              cols="30" rows="10" required>{{ old('comment') }}</textarea>
                                                </div>
                                            </div>
                                            <button class="btn btn-book">
                                                Book Now
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- END MAIN CONTENT -->



@endsection
@push('scripts')
    <script>
        $("form input").on("blur input focus", function () {
            var $field = $(this).closest(".field");
            if (this.value) {
                $field.addClass("filled");
            } else {
                $field.removeClass("filled");
            }
        });


        $("form input").on("focus", function () {
            var $field = $(this).closest(".field");
            if (this) {
                $field.addClass("filled");
            } else {
                $field.removeClass("filled");
            }
        });

        $(function () {
            $('input[name="daterange"]').daterangepicker({
                opens: 'center'
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
        $(function () {
            $('input[name="daterange2"]').daterangepicker({
                opens: 'center'
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
        $("#package").select2({
            placeholder: "Select a state",
            allowClear: true
        });
        $("#package2").select2({
            placeholder: "Select a state",
            allowClear: true
        });
    </script>
    <script>


        function openImg(imgName) {

            $('#' + imgName).siblings().hide();
            $('.сontainer-img').show();
            var item = document.getElementById(imgName);
            if (item) {
                item.style.display = "block";
            }
        }

        $(document).ready(function () {
            $('#reviewcollapse').click(function () {
                $(this).toggleClass("active");
                if ($(this).hasClass("active")) {
                    $(this).text("Hide All Reviews");
                } else {
                    $(this).text("View All Reviews");
                }
            });

            // document ready
        });
        $('.owl-carousel').owlCarousel({
            loop: false,
            margin: 10,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                767: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
    </script>
    <script>
        var input = new materialInput('.sign-form-control');
        input.init()
    </script>
@endpush
