@extends('templates.main.app')
@section('title') Dashboard @endsection
@section('content')
	<section class="bg-dark mt-70">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-9 p-2">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="d-flex align-items-center price-bar-box flex-wrap">
									<h5 class="text-white">Search </h5>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pb-100">
		<div class="container">
			<div class="row justify-content-center">
				<div class="search-form">
					@include('partials.search-form',['type'=> \App\DDD\Packages\Models\Package::TYPE_HAJJ])

				</div>

				<div class="items flex-grow-1 w-100">
					@each('partials.package-item',$models, 'package' )
				</div>
				<div class="mt-5">
					{{ $models->appends($requestData)->links() }}

				</div>

			</div>
		</div>
	</section>


@endsection
