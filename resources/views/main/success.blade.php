<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-30
 * Time: 12:14
 */
?>
@extends('templates.main.app')

@section('content')
    <header class="header quote-header">
        <div class="banner quote-banner lg">
            <img src="{{asset("/images/Layer_280850 (1).png")}}" alt="">
        </div>
        <div class="banner quote-banner md">
            <img src="{{asset("/images/Layer_40.png")}}" alt="">
        </div>
        <div class="container">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="/">
                        <img src="{{asset("/images/Layer_280849.png")}}" alt="">
                    </a>
                </div>
                <a href="{{route('booking.custom')}}" class="btn btn-quote {{isActiveRoute('booking.custom',true)}}">
                    Give me a quote
                </a>
            </div>

        </div>
    </header>
    <main>
        <section class="section request-area">
            <div class="container">
                <div class="d-flex request-area-box">
                    <div class="row justify-content-center">
                        <h3>An agent will call you to confirm all details to confirm your booking</h3>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection


