<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 10/14/18
 * Time: 5:49 PM
 */

$alert_messages = ['success', 'error', 'info', 'danger'];
?>
@foreach($alert_messages as $alert_message)
    @if(session()->get($alert_message))
        <div class="alert alert-{{$alert_message}}" role="alert">{{ session()->pull($alert_message) }}</div>
    @endif
@endforeach

@if(session()->has('alert-type'))
    <div class="alert alert-{{ session()->get('alert-type') }}" role="alert">{{ session()->get('alert-message') }}</div>
@endif



@if(session()->has('modal-message') && session()->has('modal-type'))
    @php
        $modalType =session()->get('modal-type');
    @endphp
    <div class="modal fade" tabindex="-1" role="dialog" id="custom-modal-alert" style="z-index: 9999">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-{{$modalType}}">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ trans('main.'.session()->get('modal-type')) }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ session()->get('modal-message') }}</p>
                </div>
                <div class="modal-footer alert-{{$modalType}}">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        {{ trans('main.close') }}
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @push('scripts')
        <script>
            $('document').ready(function (e) {
                $('#custom-modal-alert').modal();
            });
        </script>
    @endpush
@endif