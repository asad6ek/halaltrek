@push('scripts')
	<link rel="stylesheet" href="{{ asset('backend/plugins/dropzonejs/dropzone.min.css') }}">
	<link rel="stylesheet" href="{{asset('backend/plugins/cropperjs/cropper.min.css')}}">

	<script src="{{ asset('backend/plugins/dropzonejs/dropzone.min.js') }}"></script>
	<script src="{{ asset('frontend/js/dropzone.extend.js') }}"></script>
	<script src="{{ asset('backend/plugins/cropperjs/cropper.min.js') }}"></script>
	<script src="{{ asset('backend/plugins/cropperjs/jquery-cropper.min.js') }}"></script>
@endpush
