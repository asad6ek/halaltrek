@if($packageHotel->hotel)

    <article class="col-6 post-box d-flex align-items-center">
        <div class="post-image">
            <span class="post-image__lenta">HAJJ</span>
            <img src="{{ asset($packageHotel->hotel->getFirstImgUrl())}}" alt="">
        </div>
        <div class="post-content">
            <h5 class="post-content__title">
                {{ $packageHotel->hotel->name }}
            </h5>
            <div class="stars">

                <i class="fa fa-star @if($packageHotel->hotel->star <1) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($packageHotel->hotel->star <2) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($packageHotel->hotel->star <3) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($packageHotel->hotel->star <4) disable-star @endif" aria-hidden="true"></i>
                <i class="fa fa-star @if($packageHotel->hotel->star <5) disable-star @endif" aria-hidden="true"></i>
            </div>
            <p class="post-content__text d-flex">
															<span>
																<img src="{{asset("images/Layer_280858.png")}}" alt="">
															</span>
                {{ $packageHotel->hotel->address }}
            </p>
        </div>
    </article>
@endif
