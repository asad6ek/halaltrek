<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-18
 * Time: 16:53
 * @var  $package \App\DDD\Packages\Models\Package
 * @var $flight \App\Models\PackageAirline
 */
?>

{{--
<div class="lenta">
    <p>{{ strtoupper($package->ziyarat_type) }}</p>
    <div class="lenta-bt">
        <img src="{{ asset('frontend/images/Rectangle 2.1.png')}}" alt="">
    </div>
</div>
<div class="title">
    <h3>

    </h3>
</div>
--}}

<div class="packages-area__block">
    <div class="packages-area__block-header d-flex justify-content-between align-items-center">
        <h5>
            {{ $package->name }}
        </h5>
        <p>

        </p>
    </div>
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-9 packages-block__box">
                @foreach($package->flights  as $flight)
                    <div class="d-flex packages-block__box-hlp">
                        <div class="info-flying d-flex">
                            @if(optional($flight->airline)->getUrl())
                                <p><img src="{{ asset($flight->airline->getUrl())}}" alt=""></p>
                            @else
                                <p><img src="{{ asset('frontend/images/image.png')}}" alt=""></p>
                            @endif
                        </div>
                        <div class="d-flex packages-block__box-chld">


                            <div class="packages-block__box-info border-box-info d-flex align-items-center">
                                <div class="info-flying__box">
                                    <div class="info-flying__date d-flex align-items-center">
                                        <img src="{{asset("images/Layer_280856.png")}}" alt="">
                                        <span
                                            class="date"> {{ Carbon\Carbon::parse($flight->dateFrom())->format('jS M Y')}}</span>
                                    </div>
                                    <div class="info-flying__city d-flex align-items-center">
                                        <h5>  {{  $flight->from }}</h5>
                                        <div class="flying__image-box">
																	<span class="flying__image">
																		<img src="{{asset("images/Layer_280855.png")}}"
                                                                             alt="">
																	</span>
                                            <span class="flying__image-dote"></span>
                                        </div>
                                        <h5> {{ $flight->to }}</h5>
                                    </div>
                                </div>
                            </div>

                            <div class="packages-block__box-info border-box-info d-flex align-items-center">
                                <div class="info-flying__box">
                                    <div class="info-flying__date d-flex align-items-center">
                                        <img src="{{asset("images/Layer_280856.png")}}" alt="">
                                        <span
                                            class="date">  {{  Carbon\Carbon::parse($flight->dateTo())->format('jS M Y') }}</span>
                                    </div>
                                    <div class="info-flying__city d-flex align-items-center">
                                        <h5>  {{  $flight->to }}</h5>
                                        <div class="flying__image-box">
																	<span class="flying__image">
																		<img src="{{asset("images/Layer_280855.png")}}"
                                                                             alt="">
																	</span>
                                            <span class="flying__image-dote"></span>
                                        </div>
                                        <h5> {{ $flight->from }}</h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach

                <div class="row">
                    @each('partials.package-hotel-item',$package->hotels, 'packageHotel')
                </div>
            </div>

            <div class="col-lg-3 read-box flex-column">
                <div class="read-box__header d-flex justify-content-between">
                    <div>
                        <img src="{{ asset(optional($package->agent)->getLogo())}}" alt="logo">

                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="reviews">
                            <h6>Fabulous</h6>
                            <span>{{ $package->getReviewCount() }} Reviews</span>
                        </div>
                        <p class="review-ball">
                            {{ $package->getReviewAvarage() }}
                        </p>
                    </div>
                </div>
                <div class="read-box__price-box">
                    <div class="read-box__price d-flex justify-content-center align-items-center">
                        <h3>  {!!   $package->priceShow() !!}</h3>
                    </div>
                    <div class="read-box__btn">
                        <a href="{{ route('package.info', $package->id) }}" class="btn btn-read-more w-100">Read More</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


{{--<div class="col-lg-3 tabs-blocks right-box">
    <div class="d-flex flex-column right-all-box">
        <div class="d-flex justify-content-between right-box-item">

            <div class="d-flex">
                <div class="right-box-content">
                    <p>
                        Fabulous
                    </p>
                    <span>
											 Reviews
													</span>
                </div>
                <div class="rew-marks">
                    <p>
                        <b>
                            {{ $package->getReviewAvarage() }}
                        </b>
                    </p>
                </div>
            </div>
        </div>
        @if($package->getMinPrice())
            <div class="d-flex justify-content-between bottom-box">
                <p class="price-text">
                    <b>
                        Price
                    </b>
                </p>
                <div class="d-flex align-items-center">
                    <div class="right-box-content right-box-bottom">

                        <p>
                            {!!   $package->priceShow() !!}
                        </p>
                        <span>
														--}}{{--2,532 Reviews--}}{{--
													</span>
                    </div>
                </div>
            </div>
        @endif
        <div class="rigt-box-btn">
            <a href="{{ route('package.info', $package->id) }}">Read More</a>
        </div>
    </div>
</div>--}}
