<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-13
 * Time: 13:54
 */
?>
@push('scripts')
	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond.css')}}">
	<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.css')}}">
	{{--<link rel="stylesheet" href="{{asset('backend/plugins/filepond/filepond-image-edit.min.css')}}">--}}
	<script src="{{asset('backend/plugins/filepond/filepond.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-plugin-image-preview.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-type-validator.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-edit.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-resize.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filePond-image-transform.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-exif-orintation.min.js')}}"></script>
	<script src="{{asset('backend/plugins/filepond/filepond-image-crop.min.js')}}"></script>
	<script>
        FilePond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        FilePond.setOptions({
            server: {
                process: './process',
                revert: './revert',
                restore: './restore/',
                load: './load/',
                fetch: './fetch/',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Select the file input and use
        // create() to turn it into a pond
        var pond = FilePond.create(
            document.getElementById('inputLogo'),
            {
                files: [
						@if($agent->getLogoPond())
                    {
                        source: "{{$agent->getLogoPond()}}",
                        options: {
                            type: 'local'
                        }
                    }
					@endif
                ],
                imagePreviewHeight: 100,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 200,
                imageResizeTargetHeight: 200,
                // stylePanelLayout: 'compact circle',
                styleLoadIndicatorPosition: 'center bottom',
                styleProgressIndicatorPosition: 'right bottom',
                styleButtonRemoveItemPosition: 'left bottom',
                styleButtonProcessItemPosition: 'right bottom',

                // Use Doka.js as image editor
                // imageEditEditor: Doka.create({
                //     utils: ['crop', 'filter', 'color']
                // })
            }
        );

        $('#fform').on('submit', function (e) {
            console.log(pond.status);
            if ([1, 4].indexOf(pond.status) === -1) {
                return false;
            }
            return true;
        });
	</script>
@endpush

