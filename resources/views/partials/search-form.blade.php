<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-04-07
 * Time: 22:03
 */
use App\DDD\Packages\Models\Package;

if (!isset($duration)) {
    $duration = 7;
}

$durationList = [
    ''=>'Please choose duration',
];

$test = array();
$cities = array();
$package = Package::all();
foreach ($package as $source) {
    array_push($test, $source['num_days']);
   if (!is_null($source['country_id']))
   $cities[$source['country_id'] ] = $source->city->name;
}
$test = array_unique($test);
sort($test);
foreach ($test as $i){
    $durationList[$i] = $i . ' days';
}



if (!isset($type)) {
    $type = '';
}

$p_class = ['Premium'=>'Premium','Economy'=>'Economy','Budget'=>'Budget'];

$price_class = ['0' => 'Low to High', '1' => 'Highest to Low'];
?>

<div class="container">
    <form action="" class="main-search" role="search" method="post">
        @csrf
        <input type="hidden" name="type" value="{{ $type }}">

        <div class="tab-pane__content-search-form">
        <div class="tab-pane__content">
            <p>- Cities</p>
            <div class="selectpicker__box">

                {!!  Form::select('country_id', $cities, old('country_id'),     ['placeholder' => 'Please Choose your city','class'=>'citis-selectpicker']);!!}

            </div>
        </div>
        <div class="tab-pane__content tab-pane__content-middle">
            <p>- Duration</p>
            <div class="selectpicker__box">
                {!!  Form::select('duration', $durationList, old('duration'), ['class'=>'citis-selectpicker']);!!}

            </div>
        </div>
        <div class="tab-pane__content">
            <p>- Sort</p>
            <div class="selectpicker__box">

                {!!   Form::select('sort',$price_class, old('sort'),
                    ['placeholder' => 'Please choose class','class'=>'citis-selectpicker']) !!}
            </div>
        </div>
        <button class="btn btn-search">
            Search
        </button>
    </div>
    </form>
</div>

{{--
<form action="" class="main-search" role="search" method="post">
	@csrf
	<div class="d-flex picker align-items-center">
		<input type="hidden" name="type" value="{{ $type }}">

		<div style="width:35%;">
			<div class="p-0 pb-1">
				<div class="fordatepicker">
					<div class="field">
						<label for="package"><i class="fa fa-archive" aria-hidden="true"></i>Package
							Type</label>
						{!!  Form::select('package', $packageTypes, old('package'),
                        ['class'=>'js-example-placeholder-single js-states form-control']
                        ); !!}
					</div>
				</div>
			</div>

			--}}{{--<div class="p-0">
				<div class="fordatepicker">
					<div class="field">
						<label for="duration"><i class="fa fa-archive" aria-hidden="true"></i>Duration</label>
						{!!  Form::select('duration', $durationList, old('duration'), ['class'=>'js-example-placeholder-single js-states form-control']);!!}
					</div>
				</div>
			</div>--}}{{--
		</div>

		--}}{{--<div style="width: 35%" class="pl-3">
			<div class="p-0">
				<div class="fordatepicker">
					<div class="field">
						<label for="package_class"><i class="fa fa-archive" aria-hidden="true"></i>Package class</label>
						{!!   Form::select('package_class',$p_class,old('package_class'),
                        ['placeholder' => 'Please choose class','class'=>'js-example-placeholder-single js-states form-control']) !!}
					</div>
				</div>
			</div>

			<div class="p-0">
				<div class="fordatepicker">
					<div class="field">
						<label for="price_class"><i class="fa fa-archive" aria-hidden="true"></i>Sort</label>
						{!!   Form::select('sort',$price_class, old('sort'),
                        ['placeholder' => 'Please choose class','class'=>'js-example-placeholder-single js-states form-control']) !!}
					</div>
				</div>
			</div>
		</div>--}}{{--

		<button type="submit" class="btn btn-success top-btn ">Search</button>
	</div>
</form>--}}
