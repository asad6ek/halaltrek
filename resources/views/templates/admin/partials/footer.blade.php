<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 16:47
 */
?>
<script src="{{ asset('backend/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
{{--<script src="{{ asset('backend/plugins/chart.js/Chart.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/plugins/sparklines/sparkline.js') }}"></script>--}}
{{--<script src="{{ asset('backend/plugins/jqvmap/jquery.vmap.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>--}}
{{--<script src="{{ asset('backend/plugins/jquery-knob/jquery.knob.min.js') }}"></script>--}}
<script src="{{ asset('backend/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('backend/plugins/daterangepicker/daterangepicker.js') }}"></script>
{{--<script src="{{ asset('backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/plugins/summernote/summernote-bs4.min.js') }}"></script>--}}
<script src="{{ asset('backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('backend/dist/js/adminlte.js') }}"></script>
{{--<script src="{{ asset('backend/dist/js/pages/dashboard.js') }}"></script>--}}
<script src="{{ asset('backend/dist/js/demo.js') }}"></script>


@stack('scripts')
</div>
</body>
</html>

