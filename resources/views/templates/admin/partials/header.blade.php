<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title') - {{ config('app.name') }}</title>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Ionicons -->
  @stack('styles')
  @stack('header')
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css') }}"/>
  <link rel="stylesheet" type="text/css"
        href="{{ asset('backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}"/>
  <link rel="stylesheet" type="text/css"
        href="{{ asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css?ver='.env('THEME_VER')) }}"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    {{--  <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/jqvmap/jqvmap.min.css') }}"/>--}}
  <link rel="stylesheet" type="text/css" href="{{ asset('backend/dist/css/adminlte.min.css?ver='.env('THEME_VER')) }}"/>
  <link rel="stylesheet" type="text/css"
        href="{{ asset('backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css?ver='.env('THEME_VER')) }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/daterangepicker/daterangepicker.css?ver='.env('THEME_VER')) }}"/>
{{--  <link rel="stylesheet" type="text/css" href="{{ asset('backend/plugins/summernote/summernote-bs4.css') }}"/>--}}
  <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/style.css?ver='.env('THEME_VER')) }}"/>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{ asset('/js/script.js?ver='.env('THEME_VER')) }}"></script>
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


  </nav>
  <!-- /.navbar -->
