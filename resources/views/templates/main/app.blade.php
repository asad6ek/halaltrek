<html lang="en">
<head>
    <title>Title</title>
    <meta charset="UTF-8" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    <link href="{{asset("/frontend/css/select2.min.css?ver=".env('theme_ver',1))}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset("/frontend/css/style.css?ver=".env('theme_ver',1))}}">

    <link rel="stylesheet" href="{{asset('/css/bootstrap-select.min.css?ver='.env('theme_ver',1))}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css?ver='.env('theme_ver',1))}}">
    <link rel="stylesheet" href="{{asset('/css/fonts.css?ver='.env('theme_ver',1))}}">
    <link rel="stylesheet" href="{{asset('/css/style.css?ver='.env('theme_ver',1))}}">



</head>
<body>
@stack('head_script')

	@yield('content')

@include('templates.main.footer')

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap.min.js?ver='.env('theme_ver',1))}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap-select.min.js?ver='.env('theme_ver',1))}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('/js/main.js?ver='.env('theme_ver',1))}}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{asset("/frontend/js/select2.min.js?ver=".env('theme_ver',1))}}"></script>
<script src="{{asset("/frontend/js/script.js?v=55?ver=".env('theme_ver',1))}}"></script>


@stack('scripts')
</body>
</html>
