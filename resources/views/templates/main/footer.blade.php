<!-- FOOTER -->
<footer class="primary-color footer">
    <div class="container">
        <div class="row footer-list__box">
            <div class="col-lg-4">
                <div class="logo footer-logo">
                    <a href="{{route('main')}}">
                        <img src="{{asset("images/Layer_2808778.png")}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-2">
                <nav class="footer-nav">
                    <h6 class="foot-navs__header">Quick Link</h6>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link {{ isActiveRoute('main',true) }}" href="{{ route('main') }}">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Faq</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Privacy</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            @auth('agent')
                                <a class="nav-link @if(Request::is('cabinet*')) text-primary @endif"
                                   href="{{ route('agent.dashboard') }}"><i class="fa fa-user-o" aria-hidden="true">
                                        Cabinet</i></a>
                            @else
                                <a class="nav-link"
                                   href="{{ route('agent.register1') }}">Agent sign up/Log in
                                </a>
                            @endauth
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-6">
                <h6 class="foot-navs__header foot-navs__header-subs">
                    Subscribe to our Newsletter
                </h6>
                <form action="">
                    <input type="text" name="email" id="email" class="subscribe_email"
                           placeholder="&#xf0e0; Enter Your Email Address">
                    <button class="btn btn-subscribe">Subscribe Now</button>
                </form>
            </div>
        </div>
        <div class="footer-end d-flex justify-content-between align-items-center">
            <div class="d-flex footer-end__text-box">
                <p class="footer-end__text">2020 <span>HalalTrek.</span> All Right Reserved</p>
                <p class="footer-end__text footer-end__text-br">Term and Conditions</p>
            </div>
            <div class="socials d-flex">
					<span class="socials-icon">
						<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					</span>
                <span class="socials-icon">
						<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</span>
                <span class="socials-icon">
						<a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</span>
                <span class="socials-icon">
						<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					</span>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->
