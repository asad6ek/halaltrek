<header>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg static-top pt-3 pb-3">
		<div class="full-menu"></div>
		<div class="container">
			<a class="navbar-brand" href="/">
				<span class="logo-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
				<span class="logo-text">Halal<b>trek</b></span>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item {{isActiveRoute('booking.custom',true)}}">
                        <button type="button" class="btn btn-success"> <span src="{{route('booking.custom')}}">Give me a quote</span></button>

                    </li>
                </ul>

			</div>
		</div>
	</nav>
</header>
@include('partials.AlertComponent')
