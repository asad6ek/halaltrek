
@extends('templates.main.app')
@section('title') Dashboard @endsection

    @push('head_script')

        <script src="js/jssor.slider-28.0.0.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            window.jssor_1_slider_init = function() {

                var jssor_1_SlideoTransitions = [
                    [{b:500,d:1000,x:0,e:{x:6}}],
                    [{b:-1,d:1,x:100,p:{x:{d:1,dO:9}}},{b:0,d:2000,x:0,e:{x:6},p:{x:{dl:0.1}}}],
                    [{b:-1,d:1,x:200,p:{x:{d:1,dO:9}}},{b:0,d:2000,x:0,e:{x:6},p:{x:{dl:0.1}}}],
                    [{b:-1,d:1,rX:20,rY:90},{b:0,d:4000,rX:0,e:{rX:1}}],
                    [{b:-1,d:1,rY:-20},{b:0,d:4000,rY:-90,e:{rY:7}}],
                    [{b:-1,d:1,sX:2,sY:2},{b:1000,d:3000,sX:1,sY:1,e:{sX:1,sY:1}}],
                    [{b:-1,d:1,sX:2,sY:2},{b:1000,d:5000,sX:1,sY:1,e:{sX:3,sY:3}}],
                    [{b:-1,d:1,tZ:300},{b:0,d:2000,o:1},{b:3500,d:3500,tZ:0,e:{tZ:1}}],
                    [{b:-1,d:1,x:20,p:{x:{o:33,r:0.5}}},{b:0,d:1000,x:0,o:0.5,e:{x:3,o:1},p:{x:{dl:0.05,o:33},o:{dl:0.02,o:68,rd:2}}},{b:1000,d:1000,o:1,e:{o:1},p:{o:{dl:0.05,o:68,rd:2}}}],
                    [{b:-1,d:1,da:[0,700]},{b:0,d:600,da:[700,700],e:{da:1}}],
                    [{b:600,d:1000,o:0.4}],
                    [{b:-1,d:1,da:[0,400]},{b:200,d:600,da:[400,400],e:{da:1}}],
                    [{b:800,d:1000,o:0.4}],
                    [{b:-1,d:1,sX:1.1,sY:1.1},{b:0,d:1600,o:1},{b:1600,d:5000,sX:0.9,sY:0.9,e:{sX:1,sY:1}}],
                    [{b:0,d:1000,o:1,p:{o:{o:4}}}],
                    [{b:1000,d:1000,o:1,p:{o:{o:4}}}]
                ];

                var jssor_1_options = {
                    $AutoPlay: 1,
                    $CaptionSliderOptions: {
                        $Class: $JssorCaptionSlideo$,
                        $Transitions: jssor_1_SlideoTransitions
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$,
                        $SpacingX: 16,
                        $SpacingY: 16
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*#region responsive code begin*/

                var MAX_WIDTH = 980;

                function ScaleSlider() {
                    var containerElement = jssor_1_slider.$Elmt.parentNode;
                    var containerWidth = containerElement.clientWidth;

                    if (containerWidth) {

                        var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                        jssor_1_slider.$ScaleWidth(expectedWidth);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }

                ScaleSlider();

                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*#endregion responsive code end*/
            };
        </script>

        <style>
            .jssorl-004-double-tail-spin img {
                animation-name: jssorl-004-double-tail-spin;
                animation-duration: 1.6s;
                animation-iteration-count: infinite;
                animation-timing-function: linear;
            }

            @keyframes jssorl-004-double-tail-spin {
                from { transform: rotate(0deg); }
                to { transform: rotate(360deg); }
            }

            /*jssor slider bullet skin 031 css*/
            .jssorb031 {position:absolute;}
            .jssorb031 .i {position:absolute;cursor:pointer;}
            .jssorb031 .i .b {fill:#000;fill-opacity:0.6;stroke:#fff;stroke-width:1600;stroke-miterlimit:10;stroke-opacity:0.8;}
            .jssorb031 .i:hover .b {fill:#fff;fill-opacity:1;stroke:#000;stroke-opacity:1;}
            .jssorb031 .iav .b {fill:#fff;stroke:#000;stroke-width:1600;fill-opacity:.6;}
            .jssorb031 .i.idn {opacity:.3;}

            /*jssor slider arrow skin 051 css*/
            .jssora051 {display:block;position:absolute;cursor:pointer;}
            .jssora051 .a {fill:none;stroke:#fff;stroke-width:360;stroke-miterlimit:10;}
            .jssora051:hover {opacity:.8;}
            .jssora051.jssora051dn {opacity:.5;}
            .jssora051.jssora051ds {opacity:.3;pointer-events:none;}
        </style>

    @endpush


@section('content')
    <!-- HEADER -->
    <header class="header home-header">
        <div class="banner">
            <img src="{{asset("images/5-layers.png")}}" alt="">
            <img src="{{asset("images/2-layers.jpg")}}" alt="">
        </div>
        <div class="container">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="/">
                        <img src="{{asset('images/Layer_280849.png')}}" alt="">
                    </a>
                </div>

                <a href="{{route('booking.custom')}}" class="btn btn-quote {{isActiveRoute('booking.custom',true)}}" >
                    Give me a quote
                </a>
            </div>
            <h1 class="banner-title d-flex flex-column text-center">
				<span>
					Exclusive ziyarah in
				</span>
                Madinah al Munawwara
            </h1>
        </div>
    </header>
    <!-- END HEADER -->

    <!-- MAIN CONTENT -->
    <main>
        <div class="banner-tab">
            <div class="container">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active d-flex align-items-center" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><img src="images/Layer_280851.svg" alt=""> HAJJ</a>
                        <a class="nav-item nav-link d-flex align-items-center" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><img src="images/Layer_280852.png" alt="">UMRAH</a>
                        <a class="nav-item nav-link d-flex align-items-center" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><img src="images/Layer_280853.png" alt="">TOURS</a>
                    </div>
                </nav>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <!-- TAB HAJJ -->

                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    @include('partials.search-form',['duration'=>10, 'type'=>'hajj'])

                    <section class="section book-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 book-area__box">
                                    <h3 class="book-area__box-title">
                                        Makkah Hajj Best Hajj and
                                        Umrah Tours and travels
                                        Company you should prefer
                                    </h3>
                                    <p class="book-area__box-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus
                                        ut vestibulum metus, vel laoreet arcu. Mauris aliquet ornare
                                        massa et euismod tincidunt massa,
                                    </p>
                                    <div class="book-area__box-btn">
                                        <a href="{{route('booking.custom')}}" class="btn btn-book">Book Now</a>
                                    </div>
                                </div>
                                <div class="col-lg-6 book-area__image">
                                    <img src="{{asset("images/Layer_2808774.png")}}" alt="">
                                </div>
                            </div>
                            <div class="owl-carousel owl-theme logo-list owl-carousel-galery">
                                <div class="logo-list__item"><img src="images/Layer_280878.png" alt=""></div>
                                <div class="logo-list__item"><img src="images/Layer_280879.png" alt=""></div>
                                <div class="logo-list__item"><img src="images/Layer_280880.png" alt=""></div>
                                <div class="logo-list__item logo-list__item-enable"><img src="images/Layer_280881.png" alt=""></div>
                                <div class="logo-list__item"><img src="images/Layer_280882.png" alt=""></div>
                                <div class="logo-list__item"><img src="images/Layer_280883.png" alt=""></div>
                            </div>
                        </div>
                    </section>
                    <section class="section travel-area">
                        <h2 class="area__title">Travel Features</h2>
                        <div class="container">
                            <div class="owl-carousel owl-theme owl-carousel-travel">
                                <div class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280884.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Trip Reviews
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="travel-area__box travel-area__box-middle">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280885.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Social Dining
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280886.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Time Converter
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="section works-area">
                        <div class="container">
                            <div class="works-area__block">
                                <article class="row align-items-center">
                                    <div class="col-lg-4 works-area__block-thumbnail">
                                        <img src="images/Layer_2808775.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>01</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/_copy.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center flex-row-reverse middle-row">
                                    <div class="col-lg-4 works-area__block-thumbnail">
                                        <img src="images/Layer_2808777.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content works-area__block-content-middle">
                                        <h5 class="works-area__block-title flex-row-reverse"><span>02</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/rotate-decoration.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center">
                                    <div class="col-lg-4 works-area__block-thumbnail">
                                        <img src="images/Layer_2808776.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>03</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                    <section class="section packages-area">
                        <h2 class="area__title packages-area__title">
                            Popular Tour Packages
                        </h2>
                        <div class="container">
                            @each('partials.package-item',$hajPackages, 'package' )


                            <div class=" d-flex justify-content-center view-more1 @if($hajPackages->hasMorePages()) hidden @endif" data-page="1"
                                 data-url="{{ $hajPackages->nextPageUrl() }}">
                                <p>
                                    <a class="btn btn-load-more" href="{{ $hajPackages->nextPageUrl() }}">
                                        Load More
                                    </a>
                                </p>
                            </div>
                        </div>
                    </section>

                    <div class="container-fluid">
                        <div class="package__list">

                        </div>


                    </div>

                </div>
                <!-- TAB HAJJ END -->

                <!-- TAB UMRAH -->
                <div class="tab-pane fade show" id="nav-profile" role="tabpanel" aria-labelledby="nav-home-tab">
                    @include('partials.search-form',['duration'=>7, 'type'=>'umrah'])
                    <section class="section book-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 book-area__box">
                                    <h3 class="book-area__box-title">
                                        Makkah Hajj Best Hajj and
                                        Umrah Tours and travels
                                        Company you should prefer
                                    </h3>
                                    <p class="book-area__box-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus
                                        ut vestibulum metus, vel laoreet arcu. Mauris aliquet ornare
                                        massa et euismod tincidunt massa,
                                    </p>
                                    <div class="book-area__box-btn">
                                        <a href="package.html" class="btn btn-book">Book Now</a>
                                    </div>
                                </div>
                                <div class="col-lg-6 book-area__image">
                                    <img src="images/Layer_2808774.png" alt="">
                                </div>
                            </div>
                            <ul class="logo-list">
                                <li class="logo-list__item-disable"><img src="images/Layer_280878.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280879.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280880.png" alt=""></li>
                                <li class="logo-list__item-enable"><img src="images/Layer_280881.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280882.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280883.png" alt=""></li>
                            </ul>
                        </div>
                    </section>
                    <section class="section travel-area">
                        <h2 class="area__title">Travel Features</h2>
                        <div class="container">
                            <div class="d-flex">
                                <article class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280884.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Trip Reviews
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="travel-area__box travel-area__box-middle">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280885.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Social Dining
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280886.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Time Converter
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                    <section class="section works-area">
                        <div class="container">
                            <div class="works-area__block">
                                <article class="row align-items-center">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808775.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>01</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/_copy.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center flex-row-reverse middle-row">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808777.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content works-area__block-content-middle">
                                        <h5 class="works-area__block-title flex-row-reverse"><span>02</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/rotate-decoration.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808776.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>03</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>

                    <section class="section packages-area">
                        <h2 class="area__title packages-area__title">
                            Popular Tour Packages
                        </h2>

                        <div class="container">

                                        @each('partials.package-item',$umrahPackages, 'package' )

                            <div class=" d-flex justify-content-center view-more1 @if($umrahPackages->hasMorePages()) hidden @endif" data-page="1"
                                 data-url="{{ $umrahPackages->nextPageUrl() }}">
                                <p>
                                    <a class="btn btn-load-more" href="{{ $umrahPackages->nextPageUrl() }}">
                                        Load More
                                    </a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- TAB UMRAH END -->

                <!-- TAB TOURS -->
                <div class="tab-pane fade show" id="nav-contact" role="tabpanel" aria-labelledby="nav-home-tab">
                    @include('partials.search-form',['duration'=>7, 'type'=>'tours'])

                    <section class="section book-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 book-area__box">
                                    <h3 class="book-area__box-title">
                                        Makkah Hajj Best Hajj and
                                        Umrah Tours and travels
                                        Company you should prefer
                                    </h3>
                                    <p class="book-area__box-text">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing Vivamus
                                        ut vestibulum metus, vel laoreet arcu. Mauris aliquet ornare
                                        massa et euismod tincidunt massa,
                                    </p>
                                    <div class="book-area__box-btn">
                                        <a href="package.html" class="btn btn-book">Book Now</a>
                                    </div>
                                </div>
                                <div class="col-lg-6 book-area__image">
                                    <img src="images/Layer_2808774.png" alt="">
                                </div>
                            </div>
                            <ul class="logo-list">
                                <li class="logo-list__item-disable"><img src="images/Layer_280878.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280879.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280880.png" alt=""></li>
                                <li class="logo-list__item-enable"><img src="images/Layer_280881.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280882.png" alt=""></li>
                                <li class="logo-list__item-disable"><img src="images/Layer_280883.png" alt=""></li>
                            </ul>
                        </div>
                    </section>
                    <section class="section travel-area">
                        <h2 class="area__title">Travel Features</h2>
                        <div class="container">
                            <div class="d-flex">
                                <article class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280884.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Trip Reviews
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="travel-area__box travel-area__box-middle">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280885.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Social Dining
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                                <article class="travel-area__box">
                                    <div class="d-flex flex-column justify-content-center align-items-center">
                                        <div class="travel-area__box-img">
                                            <img src="images/Layer_280886.png" alt="">
                                        </div>
                                        <div class="travel-area__box-content text-center">
                                            <h5 class="travel-area__box-title">
                                                Time Converter
                                            </h5>
                                            <p class="travel-area__box-text">
                                                Lorem ipsum dolor sit amecon
                                                secstetur adipisgcingg Vivamsuss
                                                ut vestibulum metus.
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                    <section class="section works-area">
                        <div class="container">
                            <div class="works-area__block">
                                <article class="row align-items-center">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808775.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>01</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/_copy.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center flex-row-reverse middle-row">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808777.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content works-area__block-content-middle">
                                        <h5 class="works-area__block-title flex-row-reverse"><span>02</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                    <span class="works-area__block-doted">
										<img src="images/rotate-decoration.png" alt="">
									</span>
                                </article>
                                <article class="row align-items-center">
                                    <div class="col-lg-4">
                                        <img src="images/Layer_2808776.png" alt="">
                                    </div>
                                    <div class="col-lg-8 works-area__block-content">
                                        <h5 class="works-area__block-title"><span>03</span> HalalTrek saves you money</h5>
                                        <p class="works-area__block-text">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut vestibulum metus vel lao reet arcu. Mauris aliquet ornare massa, et euismod purus ornare sodales. Duis sed tincidunt massa, a ornare urna. Interdum et malesuada fames ac ante ipsum.
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>


                    <section class="section packages-area">
                        <h2 class="area__title packages-area__title">
                            Popular Tour Packages
                        </h2>
                        <div class="container">
                            @each('partials.package-item',$toursPackages, 'package' )

                            <div class="d-flex justify-content-center view-more1 @if($toursPackages->hasMorePages()) hidden @endif" data-page="1"
                                 data-url="{{ $toursPackages->nextPageUrl() }}">
                                <p>
                                    <a href="{{ $toursPackages->nextPageUrl() }}"  class="btn btn-load-more">
                                        Load More
                                    </a>
                                </p>
                            </div>


                        </div>
                    </section>
                </div>
                <!-- TAB TOURS END -->
            </div>
        </div>
    </main>
    <!-- END MAIN CONTENT -->
@endsection


@push('scripts')

        <script>



        $('.subscribe-form').on('submit', function (e) {
            $('#subscribe-error').hide();
            $.post({
                url: $(this).attr('action'),
                data: $(this).serialize(),
            }).done(function (data) {
                $('#subscribe-success').html(data.message).show();
                $('#subscribe-error').hide();
                $('.subscribe-form').hide();

            }).fail(function (error) {
                $('#subscribe-error').html(error.responseJSON.message).show();
            });


            return false;
        });



        $(function () {
            $('input[name="daterange"]').daterangepicker({
                opens: 'center'
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
        $(function () {
            $('input[name="daterange2"]').daterangepicker({
                opens: 'center'
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
            });
        });
        $(".js-states").select2({
            // placeholder: "Select a state",
            allowClear: true
        });

        $('.view-more1').on('click', function (e) {
            e.preventDefault();
            const mainThis = this;
            var data = $(this).parents('.tab-pane').find('form').serializeArray();
            const page = ($(this).attr('data-page') || 1) * 1 + 1;
            data.push({name: 'page', value: page});
            console.log(data);

            $.ajax({
                method: 'get',
                url: '{{ route('search.ajax') }}',
                data,
                success: function (data) {
                    handleResponse(data, mainThis);
                },
                error: function () {
                    console.log('error');
                }
            })
        });

        $('.main-search').submit(function () {

            const search = this,
                viewMoreElement = $(this).parents('.tab-pane').find('.view-more1'),
                data = $(this).serialize();

            console.log(viewMoreElement);


            $.ajax({
                method: 'get',
                url: '{{ route('search.ajax') }}',
                data,
                success: function (data) {
                    handleResponse(data, viewMoreElement, true);
                },
                error: function () {
                    console.log('error');
                }
            });

            return false;
        });

        function handleResponse(data, viewMoreElement, clear = false) {
            console.log(data);
            console.log(viewMoreElement);
            if (data.success) {

                const packages_list_item = $(viewMoreElement)
                    .parents('.container-fluid')
                    .find('.package__list');

                if (clear){
                    $(packages_list_item).html(data.content);
                } else{
                    $(packages_list_item). append(data.content);
                }




                if (data['has-next']) {
                    $(viewMoreElement).attr('data-page', data.page);
                    $(viewMoreElement).removeClass('hidden');
                } else {
                    $(viewMoreElement).addClass('hidden');
                }

            }
        }
         jssor_1_slider_init()
    </script>
@endpush
