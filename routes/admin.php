<?php

Route::prefix('admin')->name('admin.')->group(function () {

    Route::get('login', 'Admin\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Admin\LoginController@login')->name('login.post');

    Route::group(['middleware' => ['checkUser:admin', 'auth:admin']], function () {

        Route::get('/', 'Admin\DashboardController@index')->name('dashboard');

        resourceWithUpload('agent', 'Admin\AgentsController');

        Route::prefix('agent')->name('agent.')->group(function () {

            Route::resource('/{agent}/packages', 'Admin\PackageController')->names([
                'index' => 'packages.index',
                'create' => 'packages.create',
                'store' => 'packages.store',
                'edit' => 'packages.edit',
                'update' => 'packages.update',
                'destroy' => 'packages.delete',
            ]);

        });

        Route::get('packages','Admin\PackageController@list')->name('packages.all');
        Route::get('packages/create','Admin\PackageController@agents')->name('packages.agents');

        Route::resource('admin', 'Admin\AdminController')->names([
            'index' => 'admin.index',
            'create' => 'admin.create',
            'store' => 'admin.store',
            'edit' => 'admin.edit',
            'update' => 'admin.update',
            'destroy' => 'admin.delete',
        ]);
        Route::resource('settings', 'Admin\SettingsController')->names([
            'index' => 'settings.index',
            'create' => 'settings.create',
            'store' => 'settings.store',
            'edit' => 'settings.edit',
            'update' => 'settings.update',
            'destroy' => 'settings.delete',
        ]);

      Route::resource('amenities', 'Admin\AmenityController')->names([
            'index' => 'amenities.index',
            'create' => 'amenities.create',
            'store' => 'amenities.store',
            'edit' => 'amenities.edit',
            'update' => 'amenities.update',
            'destroy' => 'amenities.delete',
        ]);

        resourceWithUpload('hotels', 'Admin\HotelController');
        resourceWithUpload('airlines', 'Admin\AirlineController');

//        Route::post('hotels/process', 'Admin\HotelController@processImage');
//        Route::delete('hotels/revert', 'Admin\HotelController@revertImage');
//        Route::post('hotels/{id}/process', 'Admin\HotelController@processImage');
//        Route::delete('hotels/{id}/revert', 'Admin\HotelController@revertImage');
//        Route::get('hotels/{id}/load/{path}', 'Admin\HotelController@loadImage');
//        Route::resource('hotels', 'Admin\HotelController')->names([
//            'index' => 'hotels.index',
//            'create' => 'hotels.create',
//            'store' => 'hotels.store',
//            'edit' => 'hotels.edit',
//            'update' => 'hotels.update',
//            'destroy' => 'hotels.delete',
//        ]);

        Route::get('image-upload', 'Admin\SettingsController@imageUpload')->name('image.upload');
        Route::post('image-upload1', 'Admin\SettingsController@imageUploadPost')->name('image.upload.post');

        Route::resource('cities', 'Admin\CityController')->names([
            'index' => 'cities.index',
            'create' => 'cities.create',
            'store' => 'cities.store',
            'edit' => 'cities.edit',
            'update' => 'cities.update',
            'destroy' => 'cities.delete',
        ]);

        Route::resource('countries', 'Admin\CountryController')->names([
            'index' => 'countries.index',
            'create' => 'countries.create',
            'store' => 'countries.store',
            'edit' => 'countries.edit',
            'update' => 'countries.update',
            'destroy' => 'countries.delete',
        ]);

        Route::post('review/{review}/active', 'Admin\ReviewsController@active')->name('review.active');
        Route::resource('review', 'Admin\ReviewsController')->names([
            'index' => 'review.index',
            'create' => 'review.create',
            'store' => 'review.store',
            'edit' => 'review.edit',
            'update' => 'review.update',
            'destroy' => 'review.delete',
        ]);

        resource('booking','Admin\BookingController');
        Route::post('booking/{booking}', 'Admin\BookingController@update_status')->name('booking.update_status');

        Route::get('logout', 'Admin\LoginController@logout')->name('logout');



        Route::get('recipients', 'Admin\RecipientEmailsController@index')->name('recipient.index');



    });
});
