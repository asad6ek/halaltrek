<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-03-07
 * Time: 17:06
 */


Route::name('agent.')
    ->prefix('agent')
    ->namespace('Agent')
    ->middleware('checkUser:agent')
    ->group(function () {
        Route::get('/', 'OrderController@index')->name('dashboard');

        Route::resource('packages', 'PackageController')->names([
            'index' => 'packages.index',
            'create' => 'packages.create',
            'store' => 'packages.store',
            'edit' => 'packages.edit',
            'update' => 'packages.update',
            'destroy' => 'packages.delete',
        ]);


        Route::name('profile.')
            ->prefix('profile')
            ->group(function () {
                Route::get('/', 'ProfileController@edit')->name('index');
                Route::post('/', 'ProfileController@update');
                apiUpload('','ProfileController');
            });
        Route::post('booking/{booking}', 'OrderController@update_status')->name('booking.update_status');

        resource('orders','OrderController');

        Route::get('logout', 'LoginController@logout')->name('logout');

    });
