<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name('api.')
    ->prefix('api')
    ->namespace('Api')
    ->group(function () {


        Route::get('agents', 'AgentsController@select' )->name('agent.select');


        Route::get('airline', 'AirlineController@select' )->name('airline.select');

        Route::get('hotels', 'HotelsController@select' )->name('hotel.select');


    });
