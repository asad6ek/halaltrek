<?php

require "admin.php";
require "agent.php";

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Main\MainController@index')->name('main');

Route::post('/recipient-email', 'Main\RecipientEmailController@store')->name('main.recipient_email');

Route::get('register1', 'Agent\RegisterController@showForm1')->name('agent.register1');
Route::post('register1', 'Agent\RegisterController@postForm1')->name('post.register1');
Route::get('register2', 'Agent\RegisterController@showForm2')->name('agent.register2');
Route::post('register2', 'Agent\RegisterController@postForm2')->name('post.register2');
Route::get('login', 'Agent\LoginController@showLoginForm')->name('agent.login');
Route::post('login', 'Agent\LoginController@login');

Route::get('/package/{package}', 'Main\PackageController@view')->name('package.info');
Route::post('/package/{package}/review', 'Main\ReviewsController@save')->name('package.review');
Route::get('/review/{token}', 'Main\ReviewsController@verify')->name('review.verify');
Route::get('/booking/success', 'Main\BookingController@success')->name('booking.success');
Route::get('/booking/custom', 'Main\BookingController@bookCustomView')->name('booking.custom');
Route::post('/booking/custom', 'Main\BookingController@bookCustom')->name('booking.custom');
Route::get('/booking/{package}', 'Main\BookingController@bookPackageView')->name('booking.package');
Route::post('/booking/{package}', 'Main\BookingController@bookPackage')->name('booking.package');

Route::get('search', 'Main\SearchController@index')->name('search.index');
Route::get('search/ajax', 'Main\SearchController@ajax')->name('search.ajax');
Route::get('test', function () {
    \Debugbar::disable();
    return view('test');
});

Route::post('test', function (\Illuminate\Http\Request $request) {
    echo "<pre>";
    print_r($_POST);
    print_r($_FILES);
    echo "<pre>";
    die;
});

Route::post('file', function () {
    echo "<pre>";
    print_r($_FILES);
    echo "<pre>";
    die;
});
